import os
import math
from django.db import models
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_protect
from django.db import connection
import re
import datetime
from .models import Metrics
from core.settings import BASE_DIR
import pandas as pd
class APIViewCsrf(APIView):
    @classmethod
    def as_view(cls, **initkwargs):
        """
        Store the original class on the view function.

        This allows us to discover information about the view when we do URL
        reverse lookups.  Used for breadcrumb generation.
        """
        if isinstance(getattr(cls, "queryset", None), models.query.QuerySet):
            def force_evaluation():
                raise RuntimeError(
                    "Do not evaluate the `.queryset` attribute directly, "
                    "as the result will be cached and reused between requests. "
                    "Use `.all()` or call `.get_queryset()` instead."
                )

            cls.queryset._fetch_all = force_evaluation

        view = super().as_view(**initkwargs)
        view.cls = cls
        view.initkwargs = initkwargs

        # Note: session based authentication is explicitly CSRF validated,
        # all other authentication is CSRF exempt.
        return view

    # @method_decorator(csrf_protect)
    # def dispatch(self, request, *args, **kwargs):
    #     # Try to dispatch to the right method; if a method doesn't exist,
    #     # defer to the error handler. Also defer to the error handler if the
    #     # request method isn't on the approved list.
    #     if request.method.lower() in self.http_method_names:
    #         handler = getattr(
    #             self, request.method.lower(), self.http_method_not_allowed
    #         )
    #     else:
    #         handler = self.http_method_not_allowed
    #     return handler(request, *args, **kwargs)


decorators = [never_cache, csrf_protect]


class APIViewCsrfNoCache(APIView):
    @classmethod
    def as_view(cls, **initkwargs):
        """
        Store the original class on the view function.

        This allows us to discover information about the view when we do URL
        reverse lookups.  Used for breadcrumb generation.
        """
        if isinstance(getattr(cls, "queryset", None), models.query.QuerySet):
            def force_evaluation():
                raise RuntimeError(
                    "Do not evaluate the `.queryset` attribute directly, "
                    "as the result will be cached and reused between requests. "
                    "Use `.all()` or call `.get_queryset()` instead."
                )

            cls.queryset._fetch_all = force_evaluation

        view = super().as_view(**initkwargs)
        view.cls = cls
        view.initkwargs = initkwargs

        # Note: session based authentication is explicitly CSRF validated,
        # all other authentication is CSRF exempt.
        return view

    @method_decorator(decorators)
    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(
                self, request.method.lower(), self.http_method_not_allowed
            )
        else:
            handler = self.http_method_not_allowed
        return handler(request, *args, **kwargs)



@method_decorator(csrf_protect, name="get")
class MatricsAPIView(APIViewCsrf):
    "view de metrics"
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    renderer_classes = [JSONRenderer]

    def get(self, request):
        "api de login post"
        # print(request.data)
        # Dates requirements
        today = datetime.datetime.today().date()
        thismonth = datetime.datetime.today().date().month

        # uniquevisitorsday
        # Query objects that have a date within the specified range
        visitorsdayquery = Metrics.objects.filter(
            date=today,
        )
        # Use distinct() to get distinct values of the 'distinct_field' within the filtered objects
        uniquevisitorsday_distinct_values = visitorsdayquery.values_list(
            "ip", flat=True
        ).distinct()
        # Count the number of distinct values
        uniquevisitorsday_count = uniquevisitorsday_distinct_values.count()

        # uniquevistorsmonth
        monthstart = datetime.datetime.today().date().replace(day=1)
        dictmonth = {
            1: 31,
            2: 28,
            3: 31,
            4: 30,
            5: 31,
            6: 30,
            7: 31,
            8: 31,
            9: 30,
            10: 31,
            11: 30,
            12: 31,
        }
        monthend = datetime.datetime.today().date().replace(day=dictmonth[thismonth])
        visitorsmonthquery = Metrics.objects.filter(date__range=(monthstart, monthend))
        uniquevisitorsmonth_distinct_values = visitorsmonthquery.values_list(
            "ip", flat=True
        ).distinct()
        uniquevisitorsmonth_count = uniquevisitorsmonth_distinct_values.count()

        # loginday
        tibaday = visitorsdayquery.filter(path__startswith="/tiba").count()
        tibamonth = visitorsmonthquery.filter(
            path__startswith="/tiba"
        ).count()
        nontibaday = visitorsdayquery.filter(path__icontains="nontiba").count()
        nontibamonth = visitorsmonthquery.filter(
            path__icontains="nontiba"
        ).count()
        idday = visitorsdayquery.filter(
            path__icontains="id"
        ).count()
        idmonth = visitorsmonthquery.filter(
            path__icontains="id"
        ).count()
        mapday = visitorsdayquery.filter(path__icontains="map").count()
        mapmonth = visitorsmonthquery.filter(
            path__icontains="map"
        ).count()

        return Response(
            {
                "uniquevisitorsday": uniquevisitorsday_count,
                "uniquevisitorsmonth": uniquevisitorsmonth_count,
                "tibaday": tibaday,
                "tibamonth": tibamonth,
                "nontibaday": nontibaday,
                "nontibamonth": nontibamonth,
                "idday": idday,
                "idmonth": idmonth,
                "mapday": mapday,
                "mapmonth": mapmonth,
            },
            status=200,
        )
class WaterAPIView(APIViewCsrf):
    "View de water autonome"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView

    def response400():
        return Response(data={"": ""}, status=400)

    dict400={0:response400}
    
    def station_la_plus_proche(x, y, stations):
        """
        Trouve la station météo la plus proche en utilisant les coordonnées x et y (latitude et longitude).
        """
        distance_min = float('inf')
        station_proche = None

        for station in stations:
            lat_station = float(station['Latitude'])
            lon_station = float(station['Longitude'])
            d=math.sqrt((lat_station - x) ** 2 + (lon_station - y) ** 2)
            if d < distance_min:
                distance_min = d
                station_proche = station

        return station_proche
    dictstation={0:station_la_plus_proche}
    def post(self, request):
        "api de calcul water autonome"
        # print(request.data)
        resultdata=[]
        svresult=[]
        lat = request.data.get("lat", None)
        print(lat)
        lon = request.data.get("lon", None)
        print(lon)
        consohebdo = request.data.get("consohebdo", None)
        consohebdoestival=request.data.get("consohebdoestival", None)
        moisestivaldebut=request.data.get("moisestivaldebut", None)
        moisestivalfin=request.data.get("moisestivalfin", None)
        anneedebut=request.data.get("anneedebut", None)
        anneefin=request.data.get("anneefin", None)
        try:
            _=int(anneedebut)
            anneedebut=_
        except Exception as err:
            anneedebut=2010
        try:
            _ = int(anneefin)
            anneefin = _
        except Exception as err:
            anneefin = 2020

        pattern = r"(?<=synop\.)\d{6}(?=\.csv)"
        files=os.listdir(os.path.join(BASE_DIR,'media/data/water'))
        files=[a for a in files if a[-3:]=='csv']
        filesort=[]
        for k in files:
            match = re.search(pattern, str(k))
            if match:
                year = match[0].strip()
                if int(year[:4])<=int(anneefin) and int(year[:4])>=int(anneedebut):
                    filesort.append(k)
        print(filesort)
        combined_df = pd.concat((pd.read_csv(f"{BASE_DIR}/media/data/water/{f}",sep=';') for f in filesort), ignore_index=True)
        #07510 bordeaux
        #07535 gourdon

        #stations météos "hard coded"
        stations=[{'ID': '07005', 'Nom': 'ABBEVILLE', 'Latitude': '50.136000', 'Longitude': '1.834000', 'Altitude': '69'}, {'ID': '07015', 'Nom': 'LILLE-LESQUIN', 'Latitude': '50.570000', 'Longitude': '3.097500', 'Altitude': '47'}, {'ID': '07020', 'Nom': 'PTE DE LA HAGUE', 'Latitude': '49.725167', 'Longitude': '-1.939833', 'Altitude': '6'}, {'ID': '07027', 'Nom': 'CAEN-CARPIQUET', 'Latitude': '49.180000', 'Longitude': '-0.456167', 'Altitude': '67'}, {'ID': '07037', 'Nom': 'ROUEN-BOOS', 'Latitude': '49.383000', 'Longitude': '1.181667', 'Altitude': '151'}, {'ID': '07072', 'Nom': 'REIMS-PRUNAY', 'Latitude': '49.209667', 'Longitude': '4.155333', 'Altitude': '95'}, {'ID': '07110', 'Nom': 'BREST-GUIPAVAS', 'Latitude': '48.444167', 'Longitude': '-4.412000', 'Altitude': '94'}, {'ID': '07117', 'Nom': "PLOUMANAC'H", 'Latitude': '48.825833', 'Longitude': '-3.473167', 'Altitude': '55'}, {'ID': '07130', 'Nom': 'RENNES-ST JACQUES', 'Latitude': '48.068833', 'Longitude': '-1.734000', 'Altitude': '36'}, {'ID': '07139', 'Nom': 'ALENCON', 'Latitude': '48.445500', 'Longitude': '0.110167', 'Altitude': '143'}, {'ID': '07149', 'Nom': 'ORLY', 'Latitude': '48.716833', 'Longitude': '2.384333', 'Altitude': '89'}, {'ID': '07168', 'Nom': 'TROYES-BARBEREY', 'Latitude': '48.324667', 'Longitude': '4.020000', 'Altitude': '112'}, {'ID': '07181', 'Nom': 'NANCY-OCHEY', 'Latitude': '48.581000', 'Longitude': '5.959833', 'Altitude': '336'}, {'ID': '07190', 'Nom': 'STRASBOURG-ENTZHEIM', 'Latitude': '48.549500', 'Longitude': '7.640333', 'Altitude': '150'}, {'ID': '07207', 'Nom': 'BELLE ILE-LE TALUT', 'Latitude': '47.294333', 'Longitude': '-3.218333', 'Altitude': '34'}, {'ID': '07222', 'Nom': 'NANTES-BOUGUENAIS', 'Latitude': '47.150000', 'Longitude': '-1.608833', 'Altitude': '26'}, {'ID': '07240', 'Nom': 'TOURS', 'Latitude': '47.444500', 'Longitude': '0.727333', 'Altitude': '108'}, {'ID': '07255', 'Nom': 'BOURGES', 'Latitude': '47.059167', 'Longitude': '2.359833', 'Altitude': '161'}, {'ID': '07280', 'Nom': 'DIJON-LONGVIC', 'Latitude': '47.267833', 'Longitude': '5.088333', 'Altitude': '219'}, {'ID': '07299', 'Nom': 'BALE-MULHOUSE', 'Latitude': '47.614333', 'Longitude': '7.510000', 'Altitude': '263'}, {'ID': '07314', 'Nom': 'PTE DE CHASSIRON', 'Latitude': '46.046833', 'Longitude': '-1.411500', 'Altitude': '11'}, {'ID': '07335', 'Nom': 'POITIERS-BIARD', 'Latitude': '46.593833', 'Longitude': '0.314333', 'Altitude': '123'}, {'ID': '07434', 'Nom': 'LIMOGES-BELLEGARDE', 'Latitude': '45.861167', 'Longitude': '1.175000', 'Altitude': '402'}, {'ID': '07460', 'Nom': 'CLERMONT-FD', 'Latitude': '45.786833', 'Longitude': '3.149333', 'Altitude': '331'}, {'ID': '07471', 'Nom': 'LE PUY-LOUDES', 'Latitude': '45.074500', 'Longitude': '3.764000', 'Altitude': '833'}, {'ID': '07481', 'Nom': 'LYON-ST EXUPERY', 'Latitude': '45.726500', 'Longitude': '5.077833', 'Altitude': '235'}, {'ID': '07510', 'Nom': 'BORDEAUX-MERIGNAC', 'Latitude': '44.830667', 'Longitude': '-0.691333', 'Altitude': '47'}, {'ID': '07535', 'Nom': 'GOURDON', 'Latitude': '44.745000', 'Longitude': '1.396667', 'Altitude': '260'}, {'ID': '07558', 'Nom': 'MILLAU', 'Latitude': '44.118500', 'Longitude': '3.019500', 'Altitude': '712'}, {'ID': '07577', 'Nom': 'MONTELIMAR', 'Latitude': '44.581167', 'Longitude': '4.733000', 'Altitude': '73'}, {'ID': '07591', 'Nom': 'EMBRUN', 'Latitude': '44.565667', 'Longitude': '6.502333', 'Altitude': '871'}, {'ID': '07607', 'Nom': 'MONT-DE-MARSAN', 'Latitude': '43.909833', 'Longitude': '-0.500167', 'Altitude': '59'}, {'ID': '07621', 'Nom': 'TARBES-OSSUN', 'Latitude': '43.188000', 'Longitude': '0.000000', 'Altitude': '360'}, {'ID': '07627', 'Nom': 'ST GIRONS', 'Latitude': '43.005333', 'Longitude': '1.106833', 'Altitude': '414'}, {'ID': '07630', 'Nom': 'TOULOUSE-BLAGNAC', 'Latitude': '43.621000', 'Longitude': '1.378833', 'Altitude': '151'}, {'ID': '07643', 'Nom': 'MONTPELLIER', 'Latitude': '43.577000', 'Longitude': '3.963167', 'Altitude': '2'}, {'ID': '07650', 'Nom': 'MARIGNANE', 'Latitude': '43.437667', 'Longitude': '5.216000', 'Altitude': '9'}, {'ID': '07661', 'Nom': 'CAP CEPET', 'Latitude': '43.079333', 'Longitude': '5.940833', 'Altitude': '115'}, {'ID': '07690', 'Nom': 'NICE', 'Latitude': '43.648833', 'Longitude': '7.209000', 'Altitude': '2'}, {'ID': '07747', 'Nom': 'PERPIGNAN', 'Latitude': '42.737167', 'Longitude': '2.872833', 'Altitude': '42'}, {'ID': '07761', 'Nom': 'AJACCIO', 'Latitude': '41.918000', 'Longitude': '8.792667', 'Altitude': '5'}, {'ID': '07790', 'Nom': 'BASTIA', 'Latitude': '42.540667', 'Longitude': '9.485167', 'Altitude': '10'}, {'ID': '61968', 'Nom': 'GLORIEUSES', 'Latitude': '-11.582667', 'Longitude': '47.289667', 'Altitude': '3'}, {'ID': '61970', 'Nom': 'JUAN DE NOVA', 'Latitude': '-17.054667', 'Longitude': '42.712000', 'Altitude': '9'}, {'ID': '61972', 'Nom': 'EUROPA', 'Latitude': '-22.344167', 'Longitude': '40.340667', 'Altitude': '6'}, {'ID': '61976', 'Nom': 'TROMELIN', 'Latitude': '-15.887667', 'Longitude': '54.520667', 'Altitude': '7'}, {'ID': '61980', 'Nom': 'GILLOT-AEROPORT', 'Latitude': '-20.892500', 'Longitude': '55.528667', 'Altitude': '8'}, {'ID': '61996', 'Nom': 'NOUVELLE AMSTERDAM', 'Latitude': '-37.795167', 'Longitude': '77.569167', 'Altitude': '27'}, {'ID': '61997', 'Nom': 'CROZET', 'Latitude': '-46.432500', 'Longitude': '51.856667', 'Altitude': '146'}, {'ID': '61998', 'Nom': 'KERGUELEN', 'Latitude': '-49.352333', 'Longitude': '70.243333', 'Altitude': '29'}, {'ID': '67005', 'Nom': 'PAMANDZI', 'Latitude': '-12.805500', 'Longitude': '45.282833', 'Altitude': '7'}, {'ID': '71805', 'Nom': 'ST-PIERRE', 'Latitude': '46.766333', 'Longitude': '-56.179167', 'Altitude': '21'}, {'ID': '78890', 'Nom': 'LA DESIRADE METEO', 'Latitude': '16.335000', 'Longitude': '-61.004000', 'Altitude': '27'}, {'ID': '78894', 'Nom': 'ST-BARTHELEMY METEO', 'Latitude': '17.901500', 'Longitude': '-62.852167', 'Altitude': '44'}, {'ID': '78897', 'Nom': 'LE RAIZET AERO', 'Latitude': '16.264000', 'Longitude': '-61.516333', 'Altitude': '11'}, {'ID': '78922', 'Nom': 'TRINITE-CARAVEL', 'Latitude': '14.774500', 'Longitude': '-60.875333', 'Altitude': '26'}, {'ID': '78925', 'Nom': 'LAMENTIN-AERO', 'Latitude': '14.595333', 'Longitude': '-60.995667', 'Altitude': '3'}, {'ID': '81401', 'Nom': 'SAINT LAURENT', 'Latitude': '5.485500', 'Longitude': '-54.031667', 'Altitude': '5'}, {'ID': '81405', 'Nom': 'CAYENNE-MATOURY', 'Latitude': '4.822333', 'Longitude': '-52.365333', 'Altitude': '4'}, {'ID': '81408', 'Nom': 'SAINT GEORGES', 'Latitude': '3.890667', 'Longitude': '-51.804667', 'Altitude': '6'}, {'ID': '81415', 'Nom': 'MARIPASOULA', 'Latitude': '3.640167', 'Longitude': '-54.028333', 'Altitude': '106'}, {'ID': '89642', 'Nom': "DUMONT D'URVILLE", 'Latitude': '-66.663167', 'Longitude': '140.001000', 'Altitude': '43'}]

        # Demander à l'utilisateur d'entrer la latitude et la longitude
        x_input = str(lat)
        y_input = str(lon)

        # Remplacer les virgules par des points
        try:
            x_input = float(x_input.replace(',', '.'))
            y_input = float(y_input.replace(',', '.'))
        except:
            self.dict400[0]()
        # Utilisez les valeurs entrées par l'utilisateur comme variables x et y pour trouver la station météo la plus proche
        station_proche = self.dictstation[0](x_input, y_input, stations)
        resultdata.append(f"\nLa station météo la plus proche est:{station_proche['Nom']}")
        #print("\nLa station météo la plus proche est:", station_proche['Nom'])

        # Demander à l'utilisateur d'entrer sa consommation d'eau hebdomadaire
        waterconsohebdo = consohebdo
        
        # Remplacer les virgules par des points
        try:
            waterconsohebdo = float(waterconsohebdo.replace(',', '.'))
        except:
            self.dict400[0]()

        # Calcul consommation journaliere moyenne
        waterconsojour = waterconsohebdo/7

        # Demander à l'utilisateur d'entrer le mois de debut de la periode estivale
        moisdebutete = moisestivaldebut
        try:
            _=int(moisdebutete)
            moisdebutete=_
        except Exception as err:
            moisdebutete=5
            print(f"\nerreur de type ou valeur utilisateur vide, poursuite avec utilisation de moisdebutete={moisdebutete}")

        # Demander à l'utilisateur d'entrer le mois de fin de la periode estivale
        moisfinete = moisestivalfin
        try:
            _=int(moisfinete)
            moisfinete=_
        except Exception as err:
            moisfinete=9
            print(f"\nerreur de type ou valeur utilisateur vide, poursuite avec utilisation de moisfinete={moisfinete}")

        # Demander à l'utilisateur d'entrer la consommation d'eau supplémentaire en periode estivale
        waterconsohebdoete = consohebdoestival
        try:
            _=float(waterconsohebdoete.replace(',', '.')) # Remplacer les virgules par des points
            waterconsohebdoete=_
        except Exception as err:
            waterconsohebdoete=0
            print(f"\nerreur de type ou valeur utilisateur vide, poursuite avec utilisation de waterconsohebdoete={waterconsohebdoete}L")

        # Calcul consommation journaliere moyenne
        waterconsojourete = waterconsohebdoete/7

        result=combined_df[combined_df['numer_sta']==int(station_proche['ID'])]

        # Convertir la colonne 'date_column' dans un format datetime et la mettre en index trié
        result['datetime'] = pd.to_datetime(result['date'], format='%Y%m%d%H%M%S')
        result.set_index('datetime', inplace=True)
        result = result.sort_index()

        # remplacer les données manquantes par 0
        result['rr3']=result['rr3'].replace('mq','0')
        result['rr3']=result['rr3'].astype('float')

        # Ne garder que la colonne des précipitations des 3 dernieres heures
        result=result['rr3']

        # Calculer les sommes de précipitations par jour
        resultday=result.resample('D').sum()
        resultdaymonthindex=resultday.copy()
        resultdaymonthindex.index=resultdaymonthindex.index.month
        resultdata.append(f"Moyenne par jour (mm): {resultday.mean()}")
        resultdata.append(f"Minimum par jour (mm): {resultday.min()}")
        resultdata.append(f"Maximum par jour  (mm): {resultday.max()}")

        #print("\nMoyenne par jour (mm):\n", resultday.mean())
        #print("Minimum par jour (mm):\n", resultday.min())
        #print("Maximum par jour  (mm):\n", resultday.max())


        # Calculer les sommes de précipitations par semaine
        resultweek=result.resample('W').sum()

        # Calculer les sommes de précipitations par mois
        resultmonth=result.resample('M').sum()

        # Calculer les sommes de précipitations par trimestre
        resulttrim=result.resample('Q').sum()
        resulttrim=resulttrim.rename_axis('trimestre')
        resultdata.append(f"Précipitations trimestrielles (mm): {pd.DataFrame(resulttrim).to_html(escape=False)}")

        #print(resulttrim)

        # Calculer les sommes de précipitations par an
        resultyear=result.resample('Y').sum()
        resultdata.append(f"Précipitations annuelles moyennes (mm): {resultyear.mean()}")
        #print("\nPrécipitations annuelles moyennes (mm):\n",resultyear.mean())

        # Calculer le nombre de jours consécutifs maximum sans pluie
        max_streak = 0
        current_streak = 0
        for value in resultday:
            if value == 0:
                current_streak += 1
                max_streak = max(max_streak, current_streak)
            else:
                current_streak = 0  # Reset the streak if the value is not zero
        resultdata.append(f"\nNombre de jours consecutifs maximum sans pluie: {max_streak}")
        #print(f"\nNombre de jours consecutifs maximum sans pluie: {max_streak}")

        # Moyenne par trimestre pour chaque trimestre
        moyenne_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).mean()

        # Minimum par trimestre pour chaque trimestre
        min_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).min()

        # Maximum par trimestre pour chaque trimestre
        max_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).max()

        # Imprimer les résultats
        #print("\nMoyenne par trimestre pour chaque trimestre (mm):\n", moyenne_trimestrielle_par_trimestre)
        #print("\nMinimum par trimestre pour chaque trimestre (mm):\n", min_trimestrielle_par_trimestre)
        #print("\nMaximum par trimestre pour chaque trimestre (mm):\n", max_trimestrielle_par_trimestre)
        resultdata.append(f"\nMoyenne par trimestre pour chaque trimestre (mm): {pd.DataFrame(moyenne_trimestrielle_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"\nMinimum par trimestre pour chaque trimestre (mm): {pd.DataFrame(min_trimestrielle_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"\nMaximum par trimestre pour chaque trimestre (mm): {pd.DataFrame(max_trimestrielle_par_trimestre).to_html(escape=False)}")


        #Prise en comptes changement climatiques (hypothèses conservatrices multimodeles drias precipitations):
        # Definir les impacts sur les volumes de précipitations par saison
        adjustments = {0: -15, 1: -10, 2: -50, 3: -15}  # Adjust line 1 by -15%, line 2 by -10%, line 3 by -50%, line 4 by -15%

        # Appliquer les impacts sur les précipitations moyennes par trimestres:
        cc_moyenne_trimestrielle_par_trimestre=moyenne_trimestrielle_par_trimestre.copy()
        for line, adjustment in adjustments.items():
            cc_moyenne_trimestrielle_par_trimestre.iloc[line] = cc_moyenne_trimestrielle_par_trimestre.iloc[line]+cc_moyenne_trimestrielle_par_trimestre.iloc[line]*adjustment/100

        #print("\nMoyenne par trimestre pour chaque trimestre avec prise en compte du changement climatique (mm):\n", cc_moyenne_trimestrielle_par_trimestre)
        resultdata.append(f"\nMoyenne par trimestre pour chaque trimestre avec prise en compte du changement climatique (mm): {pd.DataFrame(cc_moyenne_trimestrielle_par_trimestre).to_html(escape=False)}")

        # Minimum par jour pour chaque trimestre
        min_par_jour_par_trimestre = resultday.groupby(resultday.index.quarter).min()
        min_par_jour_par_trimestre=min_par_jour_par_trimestre.rename_axis('trimestre')

        # Maximum par jour pour chaque trimestre
        max_par_jour_par_trimestre = resultday.groupby(resultday.index.quarter).max()
        max_par_jour_par_trimestre=max_par_jour_par_trimestre.rename_axis('trimestre')

        # Moyenne par jour pour chaque trimestre
        moyenne_par_jour_par_trimestre = resultday.groupby(resultday.index.quarter).mean()
        moyenne_par_jour_par_trimestre=moyenne_par_jour_par_trimestre.rename_axis('trimestre')

        # Imprimer les résultats
        #print("\nMinimum par jour pour chaque trimestre (mm):\n", min_par_jour_par_trimestre)
        #print("\nMaximum par jour pour chaque trimestre (mm):\n", max_par_jour_par_trimestre)
        #print("\nMoyenne par jour pour chaque trimestre (mm):\n", moyenne_par_jour_par_trimestre)
        resultdata.append(f"\nMoyenne par jour pour chaque trimestre (mm): {pd.DataFrame(moyenne_par_jour_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"\nMinimum par jour pour chaque trimestre (mm): {pd.DataFrame(min_par_jour_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"\nMaximum par jour pour chaque trimestre (mm): {pd.DataFrame(max_par_jour_par_trimestre).to_html(escape=False)}")


        #Calcul seuil mini surface de recuperation:
        surf0=(1/2)*math.ceil((13*(waterconsohebdo+waterconsohebdoete))/min(moyenne_trimestrielle_par_trimestre))
        #print(f"""Seuil surface recuperation avec hypothèse entrée et données fournies par l'utilisateur (m2)
        #hypothese:(conso trimestre / precipitations moyenne min trimestre)
        #{int(math.ceil(surf0))} m2""")
        resultdata.append(f"""<b>Seuil surface</b> recuperation avec hypothèse entrée et données fournies par l'utilisateur (m2)
        hypothese:(conso trimestre / precipitations moyenne min trimestre)
        <b>{int(math.ceil(surf0))} m2</b>""")

        #Calcul seuil mini reservoir:
        contraintejourmax=(resultday.max())*surf0
        contraintejourszero=max_streak*waterconsojour
        volume0=math.ceil(max(2.5*contraintejourmax,contraintejourszero))
        #print(f"""\nSeuil volume avec hypothèse entrée et données fournies par l'utilisateur (L)
        #hypothèse: max((2.5*précipitations journaliere maxi*Seuil surface recuperation),(44j consécutifs max sans pluie*conso journaliere))
        #{int(math.ceil(volume0))} L""")
        resultdata.append(f"""\n<b>Seuil volume</b> avec hypothèse entrée et données fournies par l'utilisateur (L)
        hypothèse: max((2.5*précipitations journaliere maxi*Seuil surface recuperation),(44j consécutifs max sans pluie*conso journaliere))
        <b>{int(math.ceil(volume0))} L</b>""")

        # surf0_input = input("\n\nSi vous souhaitez corriger la valeur initiale de surface (m2) pour les itérations, entrer votre valeur, sinon appuyer sur entree")
        # try:
        #     _=float(surf0_input)
        #     surf0=_
        # except Exception as err:
        #     print(f"\nerreur de type ou valeur utilisateur vide, poursuite avec utilisation de surf0={surf0}m2")
        #
        #
        # volume0_input = input("\n\nSi vous souhaitez corriger la valeur initiale de volume (L) pour les itérations, entrer votre valeur, sinon appuyer sur entree")
        # try:
        #     _=float(volume0_input)
        #     volume0=_
        # except Exception as err:
        #     print(f"\nerreur de type ou valeur utilisateur vide, poursuite avec utilisation de volume0={volume0}L")

        # Itérations algorithmiques stockage&consommation

        #hypothèse récupérateur 2/3 plein à t0
        water=(2/3)*volume0
        resultsurfvolume=(volume0,surf0)
        #boucle iteration
        listsurf0=[surf0*(1+i*0.33) for i in range(0,999)]
        listvolume0=[volume0*(1+i*0.5) for i in range(0,999)]
        listeday=list(resultday)
        listemonth=list(resultdaymonthindex.index)
        listresult=[]
        #fonction check surface volume
        
        def iter(data, v0,s0):
            "fonction check surface volume"
            water=(2/3)*v0
            for k in range(0,len(data)):
                recupday=data[k]*s0
                #print(f'recupday:{recupday}')
                if listemonth[k] in range(moisdebutete, moisfinete + 1):
                    consoday = waterconsojour + waterconsojourete
                else:
                    consoday = waterconsojour
                water=water+recupday-consoday
            #print(f'water:{water}')
                if water>v0:
                    #print("récupérateur plein")
                    water=v0 #hypothese gestion du trop plein ok
                if water<0:
                    #print("récupérateur vide")
                    #time.sleep(1)
                    return (0,0)
                #print("les surfaces et volumes permettent de subvenir à la consommation d'eau sur le dataset")
            return (v0,s0)
        dictiter={0:iter}
        for i in range(0,6): #boucle iteration surface
            for k in range(0,len(listeday)):
                recupday=listeday[k]*listsurf0[i]
                #print(f'recupday:{recupday}')
                if listemonth[k] in range(moisdebutete,moisfinete+1):
                    consoday=waterconsojour+waterconsojourete
                else:
                    consoday = waterconsojour
                water=water+recupday-consoday
                #print(f'water:{water}')
                if water>volume0:
                    #print("récupérateur plein")
                    water=volume0 #hypothese gestion du trop plein ok
                    continue
                if water<0:
                    #print("récupérateur vide, iteration avec hypothèse volume de récupération plus grand")
                    #time.sleep(1)
                    for j in range (1,i+40):
                        resultsurfvolume=dictiter[0](listeday,listvolume0[j],listsurf0[i])
                        if resultsurfvolume!=(0,0):
                            listresult.append(resultsurfvolume)
                            break
                        else:
                            continue
                    break

        for k in listresult:
            #print(f"""avec les données fournies par l'utilisateur, et
        #un volume de {int(k[0])}L et
        #une surface de {int(k[1])}m2,
        #on satisfait aux besoins utilisateurs ({waterconsohebdo}L/semaine constant)
        #et {waterconsohebdoete}L/semaine supplémentaire en periode estivale (du mois {moisdebutete} au mois {moisfinete})
        #entrées en hypothèse\n""")
            svresult.append(f"""avec les données fournies par l'utilisateur, et<br>
        un volume de <b>{int(k[0])}L</b> et <br>
        une surface de <b>{int(k[1])}m2</b>,<br>
        on satisfait aux besoins utilisateurs ({waterconsohebdo}L/semaine constant) <br>
        et {waterconsohebdoete}L/semaine supplémentaire en periode estivale (du mois {moisdebutete} au mois {moisfinete})<br>
        entrées en hypothèse\n<br><br>""")
        #resultdata=[stationmeteolaplusproche,moyennejour,minimumjour,maximumjour,trimestrielle,annuellemoyenne,jmaxsanspluie,moyennetrim,minitrim,maxtrim,
        #moyennetrimcc,moyjourtrim,minjourtrim,maxjourtrim,surf0,volume0,listresult]
        return Response(data=[resultdata,svresult], status=200)


class SunAPIView(APIViewCsrf):
    "View de water autonome"
    http_method_names = [
        "post"
    ]  # ['get', 'post', 'put', 'patch', 'delete', 'head', 'options', 'trace']
    authentication_classes = [SessionAuthentication]
    permission_classes = [AllowAny]
    parser_classes = [JSONParser]
    renderer_classes = [JSONRenderer]

    # serializer_class = dans GenericAPIView

    def response400():
        return Response(data={"": ""}, status=400)

    dict400={0:response400}
    def iter(data, consoelecday,d, v0, p0, joursnoelec):
        elec = v0
        current_streak = 0
        listjnoelec = []
        for i in range(len(data)):
            recupday = data.iloc[i] * p0
            # print(f'recupday:{recupday}')
            consoday = consoelecday
            elec = elec + recupday - consoday
            # print(f'water:{water}')
            if elec > v0:
                # print("batterie pleine")
                current_streak = 0
                elec = v0  # hypothese gestion du trop plein ok
                continue
            elif elec < d*v0 and current_streak < joursnoelec:
                if current_streak == 0:
                    listjnoelec.append([data.index[i].strftime('%d-%m-%Y'), 1])
                else:
                    listjnoelec[-1][-1] += 1
                elec = 0
                current_streak += 1
                continue
            elif elec < d*v0 and current_streak >= joursnoelec:
                # print("batterie vide")
                # time.sleep(1)
                return (0, 0)
            else:
                current_streak = 0
        nb_episode_no_elec = len(listjnoelec)
        if len(listjnoelec) != 0:
            list_duree_episode_no_elec = [length[1] for length in listjnoelec]
            duree_moy_episode_no_elec = sum(list_duree_episode_no_elec) / len(list_duree_episode_no_elec)
        else:
            duree_moy_episode_no_elec = 0
        # print("les puissances et batteries permettent de subvenir à la consommation d'electricité sur le dataset")
        return (v0, p0, joursnoelec, nb_episode_no_elec, duree_moy_episode_no_elec, listjnoelec)
    dictiter={0:iter}
    def post(self, request):
        "api de calcul water autonome"
        # print(request.data)
        resultdata = []
        sbpowercut = []
        sb0powercut=[]
        optisbpowercut=[]
        optisb0powercut=[]
        lat = request.data.get("lat", None)
        print(lat)
        lon = request.data.get("lon", None)
        print(lon)
        consojour = request.data.get("consojour", None)
        typebatterie = request.data.get("typebatterie", None)
        njnoelec = request.data.get("njnoelec", None)

        anneedebut = request.data.get("anneedebut", None)
        anneefin = request.data.get("anneefin", None)
        d=request.data.get("d",None)
        angle=request.data.get("angle",None)
        aspect=request.data.get("aspect",None)
        try:
            _ = int(anneedebut)
            anneedebut = _
        except Exception as err:
            anneedebut = 2005 
        try:
            _ = int(anneefin)
            anneefin = _
        except Exception as err:
            anneefin = 2020
        try:
            _ = float(d)
            d = _/100
        except Exception as err:
            d = 0
        try:
            _ = int(angle)
            angle = _
        except Exception as err:
            angle = 45
        dictaspect={'est':-90,'ouest':90,'nord':180,'sud':0}
        try:
            _ = dictaspect[aspect]
            aspect = _
        except Exception as err:
            aspect = 0             


        # Demander à l'utilisateur d'entrer la latitude et la longitude
        x_input = str(lat)
        y_input = str(lon)

        # Remplacer les virgules par des points
        try:
            x_input = float(x_input.replace(',', '.'))
            y_input = float(y_input.replace(',', '.'))
        except:
            self.dict400[0]()

        try:
            os.system(
                f'wget --max-redirect=10 -O output.csv "https://re.jrc.ec.europa.eu/api/v5_2/seriescalc?lat={x_input}&lon={y_input}&loss=14&angle={angle}&aspect={aspect}&startyear=2005&endyear=2020&pvcalculation=1&peakpower=1&pvtechchoice=crystSi&browser=0&outputformat=csv"')
        except:
            print("les données n'ont pas pu être téléchargée, exiting")
            self.dict400[0]()

        df = pd.read_csv('output.csv', skiprows=10, skipfooter=11, sep=',', engine='python')
        df['time'] = df['time'].astype(str)
        df['time'] = pd.to_datetime(df['time'], format="%Y%m%d:%H%M")
        df = df.set_index('time')
        daily_data = df.resample('D').sum()

        max_streak = 0
        current_streak = 0
        current_sum = 0
        target = 1000
        streaks = []
        for value in daily_data['P']:
            if current_sum <= target:
                current_sum += value
                current_streak += 1
                max_streak = max(max_streak, current_streak)
            else:
                streaks.append(current_streak)
                current_sum = 0
                current_streak = 0

        resultdata.append(f"""nb de jours consécutifs maximum pour qu'1kWc produise 1kWh: {str(max_streak)} j<br>
        nombre d'occurences: {streaks.count(max_streak)}<br>
        nb de jours consécutifs moyen pour qu'1kWc produise 1kWh: {str(sum(streaks) / len(streaks))} j<br>""")

        max_streak = 0
        current_streak = 0
        current_sum = 0
        target = 2000
        streaks = []
        for value in daily_data['P']:
            if current_sum <= target:
                current_sum += value
                current_streak += 1
                max_streak = max(max_streak, current_streak)
            else:
                streaks.append(current_streak)
                current_sum = 0
                current_streak = 0

        resultdata.append(f"""<br>nb de jours consécutifs maximum pour qu'1kWc produise 2kWh: {str(max_streak)} j<br>
        nombre d'occurences: {streaks.count(max_streak)}<br>
        nb de jours consécutifs moyen pour qu'1kWc produise 2kWh: {str(sum(streaks) / len(streaks))} j<br>""")

        max_streak = 0
        current_streak = 0
        current_sum = 0
        target = 3000
        streaks = []
        for value in daily_data['P']:
            if current_sum <= target:
                current_sum += value
                current_streak += 1
                max_streak = max(max_streak, current_streak)
            else:
                streaks.append(current_streak)
                current_sum = 0
                current_streak = 0

        resultdata.append(f"""<br>nb de jours consécutifs maximum pour qu'1kWc produise 3kWh: {str(max_streak)} j<br>
        nombre d'occurences: {str(streaks.count(max_streak))}<br>
        nb de jours consécutifs moyen pour qu'1kWc produise 3kWh: {str(sum(streaks) / len(streaks))}j<br><br>""")

        daily_data['P'] = daily_data['P'] / 1000
        # Production d'un kWh à partir des data jrc:
        resultday = daily_data['P'].resample('D').sum()
        resultday.index = pd.to_datetime(resultday.index, format="%Y%m%d")
        resultdata.append(f"""<br>Production moyenne par jour (kWh):<br> {resultday.mean()}<br>
        Production minimum par jour (kWh):<br> {resultday.min()}<br>
        Production maximum par jour  (kWh):<br> {resultday.max()}<br><br>""")

        # Calculer les sommes de production par semaine
        resultweek = daily_data['P'].resample('W').sum()

        # Calculer les sommes de production par mois
        resultmonth = daily_data['P'].resample('M').sum()

        # Calculer les sommes de production par trimestre
        resulttrim = resultday.resample('Q').sum()
        resulttrim = resulttrim.rename_axis('trimestre')
        resultdata.append(f"<br>Production par trimestre (kWh):<br> {pd.DataFrame(resulttrim).to_html(escape=False)}")

        # Calculer les sommes de production par an
        resultyear = daily_data['P'].resample('Y').sum()
        resultdata.append(f"<br>Production annuelle moyennes d'1kWc (kWh):<br> {resultyear.mean()}")

        # Calculer le nombre de jours consécutifs maximum sans production
        max_streak = 0
        current_streak = 0
        for value in resultday:
            if value == 0:
                current_streak += 1
                max_streak = max(max_streak, current_streak)
            else:
                current_streak = 0  # Reset the streak if the value is not zero
        resultdata.append(f"<br>Nombre de jours consecutifs maximum à production 0: <br>{max_streak}j")

        # Moyenne par trimestre pour chaque trimestre
        moyenne_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).mean()

        # Minimum par trimestre pour chaque trimestre
        min_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).min()

        # Maximum par trimestre pour chaque trimestre
        max_trimestrielle_par_trimestre = resulttrim.groupby(resulttrim.index.quarter).max()

        # Imprimer les résultats
        resultdata.append(f"<br>Moyenne par trimestre pour chaque trimestre (kWh):<br> {pd.DataFrame(moyenne_trimestrielle_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"<br>Minimum par trimestre pour chaque trimestre (kWh):<br> {pd.DataFrame(min_trimestrielle_par_trimestre).to_html(escape=False)}")
        resultdata.append(f"<br>Maximum par trimestre pour chaque trimestre (kWh):<br> {pd.DataFrame(max_trimestrielle_par_trimestre).to_html(escape=False)}")

        # Remplacer les virgules par des points
        inputelecconsoday = float(consojour)


        # Dimensionnement batterie initiale pour avoir 24h d'autonomie:
        if typebatterie == "plomb":
            batterie0 = int(math.ceil(inputelecconsoday / 0.5))
            typebatterie = "plomb"
        else:
            batterie0 = int(math.ceil(inputelecconsoday / 0.8))
            typebatterie = "lithium"
        resultdata.append(
            f"""Seuil batterie initiale avec hypothèse entrée et données fournies par l'utilisateur (batterie {typebatterie}))
          hypothese:(conso elec journaliere / 0.8 au lithium et conso elec journaliere / 0.5 au plomb)
          {batterie0} kWh""")

        # combien de jours sans elec il ou elle peut supporter
        try:
            jnoelec = int(njnoelec)
        except Exception as err:
            jnoelec = 0
        resultdata.append(f"""Nombre de jours maximum consécutifs sans electricité (ou avec un groupe electrogene)  pris en hypothèse 
          {jnoelec} j""")

        # Dimensionnement puissance crete des modules (kWc) initiale pour produire suffisamment en hiver (hypothese 2kWh par kWc par jour):
        puissance0 = int(math.ceil(inputelecconsoday / 2))

        resultdata.append(f"""<br>Seuil puissance initiale avec hypothèse entrée et données fournies par l'utilisateur
          hypothèse: conso elec journaliere/2 (2kWh produit par kWc en hiver)
          {puissance0} kWc""")

        # Itérations algorithmiques stockage&consommation


        # hypothèse récupérateur plein à t0
        elec = batterie0
        resultpuissancevolume = (batterie0, puissance0, jnoelec, 0)

        # boucle iteration
        listpuissance0 = [puissance0 * (1 + i * 0.33) for i in range(0, 999)]
        listbatterie0 = [batterie0 * (1 + i * 0.5) for i in range(0, 999)]
        listresult_0blackout = []
        listresult_blackout = []

        # increment avec hypothèse jnoelec=0 (k in range 0,1)
        for i in range(0, 14):  # boucle iteration surface
            for j in range(0, i + 40):
                for k in range(0, 1):
                    resultpuissancevolume = self.dictiter[0](resultday, inputelecconsoday, d, listbatterie0[j], listpuissance0[i],
                                                  k)
                    if resultpuissancevolume != (0, 0):
                        listresult_0blackout.append(resultpuissancevolume)
                        break
                    else:
                        continue
                if resultpuissancevolume != (0, 0):
                    break
                else:
                    continue

        # increment avec hypothèse jnoelec=20 (k in range 0,21)
        for i in range(0, 14):  # boucle iteration surface
            for j in range(0, i + 40):
                for k in range(0, 21):
                    resultpuissancevolume = self.dictiter[0](resultday, inputelecconsoday, d, listbatterie0[j], listpuissance0[i],
                                                  k)
                    if resultpuissancevolume != (0, 0):
                        listresult_blackout.append(resultpuissancevolume)
                        break
                    else:
                        continue
                if resultpuissancevolume != (0, 0):
                    break
                else:
                    continue



        # Affichage des résultats des boucles d'itérations
        for k in listresult_0blackout:
            sb0powercut.append(f"""<br>avec les données fournies par l'utilisateur, et<br> 
              une batterie de <b>{k[0]}kWh</b> <br>
              et une puissance crete de <b>{k[1]}kWc</b>,<br>
              et une hypothèse de {k[2]}j sans elec possible<br>
              et {k[3]} episodes sans elec (ou avec un groupe electrogene)<br>
              entre 2005 et 2020:<br>
              Liste des episodes sans elec (nb de jours):<br>
              {k[5]}<br>
              pour une duree moyenne d'épisode sans elec de {k[4]}j<br> 
              on satisfait aux besoins utilisateurs ({inputelecconsoday}kWh/jour)entrées en hypothèse<br>""")

        for k in listresult_blackout:
            sbpowercut.append(f"""<br>avec les données fournies par l'utilisateur, et<br> 
              une batterie de <b>{k[0]}kWh</b> <br>
              et une puissance crete de <b>{k[1]}kWc</b>,<br>
              et une hypothèse de {k[2]}j sans elec possible<br>
              et {k[3]} episodes sans elec (ou avec un groupe electrogene):<br>
              entre 2005 et 2020:<br>
              Liste des episodes sans elec (nb de jours):<br>
              {k[5]}<br>
              pour une duree moyenne d'épisode sans elec de {k[4]}j<br> 
              on satisfait aux besoins utilisateurs ({inputelecconsoday}kWh/jour)entrées en hypothèse<br>""")

        print('affichage des résultats optimisés')
        # Optimisation résultats:

        # premier résultat en incrémentant la puissance crete dont la capacité de batterie est minimale
        batterie_0blackout = [k[0] for k in listresult_0blackout]
        minbatterie_0blackout = min(batterie_0blackout)
        for k in listresult_0blackout:
            if k[0] == minbatterie_0blackout:
                optisb0powercut.append(f"""<br>avec les données fournies par l'utilisateur, et <br>
                  une batterie de <b>{k[0]}kWh</b> <br>
                  et une puissance crete de <b>{k[1]}kWc</b>,<br>
                  et une hypothèse de {k[2]}j sans elec possible<br>
                  et {k[3]} episodes sans elec (ou avec un groupe electrogene):<br>
                  entre 2005 et 2020:<br>
                  Liste des episodes sans elec (nb de jours):<br>
                  {k[5]}<br>
                  pour une duree moyenne d'épisode sans elec de {k[4]}j<br> 
                  on satisfait aux besoins utilisateurs ({inputelecconsoday}kWh/jour)entrées en hypothèse<br>""")
                break
        # premier résultat en incrémentant la puissance crete fournissant un nb de jours de blackout inferieur ou égal
        # à celui entrée par l'utilisateur
        for k in listresult_blackout:
            if k[2] <= jnoelec:
                optisbpowercut.append(f"""<br>avec les données fournies par l'utilisateur, et<br> 
                  une batterie de <b>{k[0]}kWh</b> <br>
                  et une puissance crete de <b>{k[1]}kWc</b>,<br>
                  et une hypothèse de {k[2]}j sans elec possible<br>
                  et {k[3]} episodes sans elec (ou avec un groupe electrogene):<br>
                  entre 2005 et 2020:<br>
                  Liste des episodes sans elec (nb de jours):<br>
                  {k[5]}<br>
                  pour une duree moyenne d'épisode sans elec de {k[4]}j<br> 
                  on satisfait aux besoins utilisateurs ({inputelecconsoday}kWh/jour)entrées en hypothèse<br>""")
                break
        return Response(data=[resultdata, sb0powercut,sbpowercut,optisb0powercut,optisbpowercut], status=200)

