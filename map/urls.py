from django.urls import path, re_path
from . import views

urlpatterns = [
    path("wgEXY2oF5AK/", views.MatricsAPIView.as_view(), name="metrics"),
    path("waterapi/", views.WaterAPIView.as_view(), name="waterapi"),
    path("sunapi/", views.SunAPIView.as_view(), name="sunapi"),
]
