#!/bin/bash -l


import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os
from selenium.common.exceptions import StaleElementReferenceException


def send(from_, name, subject, content):
    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart("alternative")
    msg["Subject"] = f"mail perso"
    msg["From"] = f"{from_}"
    msg["To"] = "aurel.pere@ikmail.com"

    # Create the body of the message (a plain-text and an HTML version).
    text = (
        f"https://www.lidl.fr/p/parkside-tronconneuse-a-essence-pbks-53-b3/p100365984"
    )
    # with open ('mailing.html','r',encoding='utf-8') as fileo:
    # html=fileo.read()
    # html=html.replace('X€ / mois',f'{price}€ / mois')
    # html = html.replace('X€ / mois', f'{price}€ / mois')
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, "plain", "utf-8")
    # part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    # msg.attach(part2)
    # Send the message via local SMTP server.
    mail = smtplib.SMTP("mail.infomaniak.com", 587)

    mail.ehlo()

    mail.starttls()

    mail.login("contact@harcelement.app", os.getenv("EMAIL_API_KEY"))
    mail.sendmail(from_, msg["To"], msg.as_string())
    mail.quit()


class Command:
    help = "mail perso"
    options = Options()
    options.headless = False
    driver = webdriver.Firefox(options=options)
    search = True
    result = []

    def email(self):
        html_message = "https://www.lidl.fr/p/parkside-tronconneuse-a-essence-pbks-53-b3/p100365984"
        send(
            f"contact@harcelement.app",
            f"mail_perso",
            f"https://www.lidl.fr/p/parkside-tronconneuse-a-essence-pbks-53-b3/p100365984",
            "https://www.lidl.fr/p/parkside-tronconneuse-a-essence-pbks-53-b3/p100365984",
        )
        self.search = False

    def present(self):
        self.result.append(1)

    def nopresent(self):
        pass

    dict_ = {1: present, 0: nopresent}
    dictmail = {1: email, 0: nopresent}

    def handle(self, *args, **options):
        while self.search:
            try:
                self.result = []
                self.driver.get(
                    "https://www.lidl.fr/p/parkside-tronconneuse-a-essence-pbks-53-b3/p100365984"
                )
                time.sleep(5)
                input = self.driver.find_elements(
                    by=By.XPATH, value=f'//*[@class="hint__body"]'
                )
                for a in input:
                    self.dict_[bool("Cet article sera bientôt disponible." in a.text)](
                        self
                    )
                self.dictmail[bool(len(self.result) == 0)](self)
                time.sleep(60)
            except StaleElementReferenceException as err:
                print(err)


if __name__ == "__main__":
    Command().handle()
