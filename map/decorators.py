import functools
import logging
from datetime import datetime


def log_ip(view_func):
    """
    log incoming ip for view_func view function
    """

    @functools.wraps(view_func)
    def wrapper(request, *args, **kwargs):
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        # print(x_forwarded_for)
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = request.META.get("REMOTE_ADDR")
        # instead of __name__ "django" but not working in local runserver or "django.server" for runserver
        # __name__ will take the name of the app the decorator is used in?
        # see https://docs.djangoproject.com/en/4.2/howto/logging/#naming-loggers-hierarchy
        # and https://docs.djangoproject.com/en/4.2/ref/logging/#django-logging-extensions
        logging.getLogger("django").info(
            f'[{datetime.now().strftime("%d/%b/%Y %H:%M:%S")}] - IP: ' + ip
        )
        return view_func(request, *args, **kwargs)

    return wrapper
