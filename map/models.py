from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from django.core.management.utils import get_random_secret_key
import datetime


class Metrics(models.Model):
    date = models.DateField(
        default=datetime.date.today, null=True, verbose_name="Date request"
    )
    path = models.CharField(
        max_length=255,
        default="",
        blank=True,
        validators=[RegexValidator(regex=r"[A-Za-z0-9\/]+")],
    )
    ip = models.CharField(
        max_length=255,
        default="",
        blank=True,
        validators=[RegexValidator(regex=r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")],
    )

    def set_date(self, date):
        self.date = date
        self.save()

    def set_path(self, path):
        self.path = path
        self.save()

    def set_ip(self, ip):
        self.ip = ip
        self.save()

    def __str__(self):
        return f"ip:{self.ip},path:{self.path},date:{self.date}"
