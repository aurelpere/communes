\set dep d :depart
\set millesime :millesime

--initialisation des variables
\set schema0 ff_ :dep _ :millesime 
\set table0 :dep _ :millesime _pb0010_local
\set table1 :dep _locaux
\set table2 :dep _TauxBureauxEtCco
\set table3 :dep _CntEtSurfLocaux
\set table4 :dep _IdLocauxActivite
\set table5 :dep _DecompteLocauxActivite
\set table6 :dep _IdLocauxActiviteParcelles
\set table7 :dep _CntEtSurfBatiActiviteVac
\set table8 :dep _NbbatActiviteVacant

-- requete de creation de table de locaux d'activité vacants
SELECT 'creation table locaux vacants' as comment;
DROP TABLE if exists public.:table1;
CREATE TABLE public.:table1 AS
(
SELECT idpar, idlocal, idbat, idcom, idcomtxt, ccodep, dteloc, cconlc, actvac2a, actvac5a, fburx, cconac, dnvoiri, dvoilib, stotp, typproptxt, idprocpte, dvltrt, dbniv, npiece_ff, nbgarpark, jannath, dniv, dnbniv,
CASE WHEN (dteloc = '4' AND cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB', 'U', 'US', 'UN', 'UE', 'UG')) THEN 1 ELSE 0 END AS activite
FROM :schema0.:table0
);


-- requete de creation de table de locaux d'habitation vacants
SELECT 'creation table habitat vacant' as comment;
DROP TABLE if exists public.:tablehab;
CREATE TABLE public.:tablehab AS
(
SELECT idpar, idlocal, idbat, idcom, idcomtxt, ccodep, dteloc, cconlc, vachab2a, vachab5a, fburx, cconac, dnvoiri, dvoilib, stoth, typproptxt, idprocpte, dvltrt, dbniv, npiece_ff, nbgarpark, jannath, dniv, dnbniv,
CASE WHEN (dteloc IN ('1', '2') THEN 1 ELSE 0 END AS habitat
FROM :schema0.:table0
);










-- requetes de caractérisation des locaux d'activités vacant

SELECT 'creation table TauxBureauxEtCco' as comment;
--Taux bureaux, nace et ape :
DROP TABLE if exists public.:table2;
CREATE TABLE public.:table2 AS
(
SELECT 
ccodep,
COUNT (DISTINCT idlocal) AS cntidlocal,
SUM(fburx) AS cntbureauxnotnull,
COUNT(CASE WHEN cconac IS NOT NULL THEN 1 ELSE NULL END) AS cntcconacnotnull
FROM public.:table1
WHERE activite=1
GROUP BY ccodep
);
--72324 idlocal (vald33)

SELECT 'creation table Decompte et surface locaux' as comment;
--Decompte et Surface locaux et vacance
DROP TABLE if exists public.:table3;
CREATE TABLE public.:table3 AS
(
SELECT 
idbat, 
idcom,
idcomtxt,
COUNT (DISTINCT idlocal) AS cntidlocal,
COUNT(CASE WHEN actvac2a='t' THEN 1 ELSE NULL END) AS cntvac2a,
COUNT(CASE WHEN actvac5a='t' THEN 1 ELSE NULL END) AS cntvac5a,
COUNT(CASE WHEN cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') THEN 1 ELSE NULL END) AS cntcommerce,
COUNT(CASE WHEN (cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') AND actvac2a='t') THEN 1 ELSE NULL END) AS cntcommercevac2a,
COUNT(CASE WHEN (cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') AND actvac5a='t') THEN 1 ELSE NULL END) AS cntcommercevac5a,
COUNT(CASE WHEN cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') THEN 1 ELSE NULL END) AS cntindus,
COUNT(CASE WHEN (cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') AND actvac2a='t') THEN 1 ELSE NULL END) AS cntindusvac2a,
COUNT(CASE WHEN (cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') AND actvac5a='t') THEN 1 ELSE NULL END) AS cntindusvac5a,
SUM (stotp) AS SurfLocaux,
SUM (CASE WHEN actvac2a='t' THEN stotp ELSE NULL END) AS Surfvac2a,
SUM (CASE WHEN actvac5a='t' THEN stotp ELSE NULL END) AS Surfvac5a,
SUM (CASE WHEN cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') THEN stotp ELSE NULL END) AS Surfcommerce,
SUM (CASE WHEN (cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') AND actvac2a='t') THEN stotp ELSE NULL END) AS Surfcommercevac2a,
SUM (CASE WHEN (cconlc IN ('CA', 'CD', 'CM', 'CH', 'ME', 'DC', 'SM', 'AT', 'AU', 'CB') AND actvac5a='t') THEN stotp ELSE NULL END) AS Surfcommercevac5a,
SUM (CASE WHEN cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') THEN stotp ELSE NULL END) AS Surfindus,
SUM (CASE WHEN (cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') AND actvac2a='t') THEN stotp ELSE NULL END) AS Surfindusvac2a,
SUM (CASE WHEN (cconlc  IN ('U', 'US', 'UN', 'UE', 'UG') AND actvac5a='t') THEN stotp ELSE NULL END) AS Surfindusvac5a
FROM public.:table1
Group BY idbat,idcom,idcomtxt
);


--594517 idbat (vald33)
-- 50490 idbat pour activité=1 (vald33)

SELECT 'creation table Identifiant locaux activite' as comment;
--Identification bâti activité et bâti mixte locaux d’activités
DROP TABLE if exists public.:table4;
CREATE TABLE public.:table4 AS
(
SELECT 
idbat, 
idcom,
idcomtxt,
SUM (CASE WHEN ((cntcommerce+cntindus)*1.0/cntidlocal<1 and (cntcommerce+cntindus)*1.0/cntidlocal>0.01) THEN 1 ELSE 0 END) AS BatimentMixte,
SUM (CASE WHEN ((cntcommerce+cntindus)/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActivite,
SUM (CASE WHEN (cntcommerce/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActiviteCommerce,
SUM (CASE WHEN (cntindus/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActiviteIndus
FROM public.:table3
Group BY idbat,idcom,idcomtxt
);
--594517 idbat (vald33)

SELECT 'creation table Decompte locaux activite' as comment;
--Decompte Locaux Activite Dept33
DROP TABLE if exists public.:table5;
CREATE TABLE public.:table5 AS
(
SELECT 
SUM (BatimentMixte) AS SumBatimentMixte,
SUM (BatimentActivite) AS SumBatimentActivite,
SUM (BatimentActiviteCommerce) AS SumBatimentActivitecommerce,
SUM (BatimentActiviteIndus) AS SumBatimentActiviteIndus
FROM public.:table4
);
/* (vald33)
14884 BatimentMixte
28767 BatimentActivitecommerce
6583 BatimentActiviteIndusVac2a
*/
SELECT 'creation table Identifiant locaux activite et parcelle' as comment;
--Identification bâti activité et bâti mixte locaux d’activités et parcelles
DROP TABLE if exists public.:table6;
CREATE TABLE public.:table6 AS
(
SELECT 
T2.idbat, 
T2.idcom,
T2.idcomtxt,
T1.idpar,
T1.idprocpte,
T1.typprop,
SUM (CASE WHEN ((cntcommerce+cntindus)*1.0/cntidlocal<1 and (cntcommerce+cntindus)*1.0/cntidlocal>0.01) THEN 1 ELSE 0 END) AS BatimentMixte,
SUM (CASE WHEN ((cntcommerce+cntindus)/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActivite,
SUM (CASE WHEN (cntcommerce/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActiviteCommerce,
SUM (CASE WHEN (cntindus/cntidlocal=1) THEN 1 ELSE 0 END) AS BatimentActiviteIndus
FROM public.:table1 AS T1, public.:table3 AS T2
WHERE T1.idbat=T2.idbat
Group BY T2.idbat,T2.idcom,T2.idcomtxt, T1.idpar, T1.idprocpte
);
/*
(vald33)
844772
634247 idprocpte distinct
594517 idbat distinct
522540 idpar distinct
*/

SELECT 'creation table Decompte et surface bati activite vacant' as comment;
--Identification bâti activité vacant
DROP TABLE if exists public.:table7;
CREATE TABLE public.:table7 AS
(
SELECT 
T2.idbat, 
T2.idcom,
T2.idcomtxt,
SUM (cntidlocal) AS SumCntIdLocal,
SUM (cntvac2a) AS SumCntvac2a,
SUM (cntvac5a) AS SumCntvac5a,
SUM (cntcommerce) AS Sumcntcommerce,
SUM (cntcommercevac2a) AS Sumcntcommercevac2a,
SUM (cntcommercevac5a) AS Sumcntcommercevac5a,
SUM (cntindus) AS Sumcntindus,
SUM (cntindusvac2a) AS Sumcntindusvac2a,
SUM (cntindusvac5a) AS Sumcntindusvac5a,
SUM (SurfLocaux) AS SumSurfLocaux,
SUM (Surfvac2a) AS SumSurfvac2a,
SUM (Surfvac5a) AS SumSurfvac5a,
SUM (Surfcommerce) AS SumSurfcommerce,
SUM (Surfcommercevac2a) AS SumSurfcommercevac2a,
SUM (Surfcommercevac5a) AS SumSurfcommercevac5a,
SUM (Surfindus) AS SumSurfindus,
SUM (Surfindusvac2a) AS SumSurfindusvac2a,
SUM (Surfindusvac5a) AS SumSurfindusvac5a,
SUM (BatimentActivite) AS SumBatimentActivite,
SUM (BatimentActiviteCommerce) AS SumBatimentActiviteCommerce,
SUM (BatimentActiviteIndus) AS SumBatimentActiviteIndus,
SUM (CASE WHEN (BatimentActivite=1 AND ((cntcommercevac2a+cntindusvac2a)*1.0/cntidlocal)>0.8) THEN 1 ELSE 0 END) AS BatimentActiviteVac2a,
SUM (CASE WHEN (BatimentActiviteCommerce=1 AND ((cntcommercevac2a)*1.0/cntidlocal)>0.8)  THEN 1 ELSE 0 END) AS BatimentActiviteCommerceVac2a,
SUM (CASE WHEN (BatimentActiviteIndus=1 AND ((cntindusvac2a)*1.0/cntidlocal)>0.8)  THEN 1 ELSE 0 END) AS BatimentActiviteIndusVac2a
FROM public.:table3 AS T1, public.:table4 AS T2
WHERE T1.idbat=T2.idbat AND BatimentActivite=1
Group BY T2.idbat, T2.idcomtxt, T2.idcom
);
--35553 idbat (vald33)

--Table nombre idbatvacant par commune
SELECT 'creation table Decompte batiment activite vacant par commune' as comment;
DROP TABLE if exists public.:table8;
CREATE TABLE public.:table8 AS
(
SELECT 
idcom,
idcomtxt,
SUM (BatimentActiviteVac2a) AS NbBatActVac2a,
SUM (BatimentActiviteCommerceVac2a) AS NbBatActCommerceVac2a,
SUM (BatimentActiviteIndusVac2a) AS NbBatActIndusVac2a,
SUM (SumBatimentActivite) AS NbBatimentActivite
FROM  public.:table7
GROUP BY idcom,idcomtxt
);
