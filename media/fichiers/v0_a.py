# -*- coding:Utf-8 -*-
# The code for changing pages was derived from: http://stackoverflow.com/questions/7546050/switch-between-two-frames-in-tkinter
# License: http://creativecommons.org/licenses/by-sa/3.0/	

import sys
if sys.version[0] =='2':
    import Tkinter as tk # module Tkinter pour Python2
else:
    import tkinter as tk# module Tkinter pour Python3
import time
import os 
import os.path 
import datetime
LARGE_FONT= ("Verdana", 12)
###Metier
# -*- coding:Utf-8 -*-

###fonctions chaines

#pour texte brut sans le dernier caractère
class Planning(object):   
    def __init__(self,*args,**kwargs):   
        self.name='cnnr'
    
    def decomposing(self,chaine,car):
        "renvoie la valeur de la chaine comprise entre les delimiteurs car [[ "
        listedecomposee=[]
        for i in range(len(chaine)):
            if i==0:
                subchaine=""
                k=0
                while chaine[i+k]!=car:
                    subchaine+=chaine[i+k]
                    k+=1
                listedecomposee.append(subchaine)
            if chaine[i]==car:
                subchaine=""
                k=1
                while (chaine[i+k]!=car and i+k<len(chaine)-1):##ok mais ne prend pas la derniere )
                    subchaine+=chaine[i+k]
                    k+=1
                listedecomposee.append(subchaine)
            if i==len(chaine)-1:
                return listedecomposee

#pour csv
    def decomposingpropre(self,chaine,car):
        "renvoie les chaines d'une ligne csv délimitées par car dans une liste"
        listemots=[]
        mots=''
        o=" " #initialisation d'une variable o pour la boucle for pour checker le caractère précédent
        index=0
        for m in chaine:
            if m!=car:#éventuellement utiliser fonction caractère alphanumérique
                mots=mots+m
            if m==car and o!=car:
                listemots.append(mots)#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                mots=''
            if m==car and o==car:
                listemots.append('')#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                mots=''
            if index==len(chaine)-1:
            #print('index')
                listemots.append(mots)#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                mots=''
            o=m #on affecte à o la valeur de m à la fin de la boucle for pour qu'à m+1, o vale m
            index=index+1#on indexe à +1 pour le caractère suivant
        return listemots


#pour liste avec tous les caractères et index
    def matri(self,phrase,car):
        "renvoie les mots d'une ligne csv délimités par car dans une liste de liste [mots,emplacement]"
        listemots=[]
        mots=''
        o=" " #initialisation d'une variable o pour la boucle for pour checker le caractère précédent
        index=0
        for m in phrase:
            if m!=car:#éventuellement utiliser fonction caractère alphanumérique
                mots=mots+m
            if m==car and o!=car:
                listemots.append([mots,index-len(mots)])#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                mots=''
            if index==len(phrase)-1:
                #print('index')
                listemots.append([mots,index-len(mots)+1])#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                mots=''
            o=m #on affecte à o la valeur de m à la fin de la boucle for pour qu'à m+1, o vale m
            index=index+1#on indexe à +1 pour le caractère suivant
        return listemots

#pour texte brut sans le dernier caractère
    def subchain(self,chaine,car1,car2):
        "renvoie la valeur de la chaine comprise entre les caracteres car1car2#car2 peut etre une liste ou un tuple de car"
        subchaine=""
        for i in range(len(chaine)):
            if chaine[i]==car1:
                k=1
                while chaine[i+k] not in car2:
                #print(chaine[i+k])
                    subchaine+=chaine[i+k]
                #print(subchaine)
                    k+=1
        return subchaine

#trouve le caractère dans la chaine à partir de l'index debut et renvoie l'index
    def find(self,chaine,caractère,debut):
        i=debut
        while i<len(chaine):
            if chaine[i]==caractère:
                index=i
                return index
                break
            else:
                index=-1
                i=i+1    
        return index     

    def slicationg(self,chaine,car):#
        "renvoie la valeur de la chaine comprise à gauche de car"
        subchaine=""
        fi=self.find(chaine,car,0)
        for i in range(fi):
            subchaine+=chaine[i]
        return subchaine

    def slicationd(self,chaine,car):#
        "renvoie la valeur de la chaine comprise à droite de car"
        subchaine=""
        fi=self.find(chaine,car,0)
        for i in range(fi+1,len(chaine)):
            subchaine+=chaine[i]
        return subchaine

    def estUnCarPoints(self,arg): 
        CarPoints={'-','1','2','3','4','5','6','7','8','9','0','.'}
        for i in CarPoints:
            if str(arg)==str(i):
                return 1
                break
        return 0

    def chiffreEstUn(self,arg): 
        Chiffres={'-','1','2','3','4','5','6','7','8','9','0'}
        for i in Chiffres:
            if str(arg)==str(i):
                return 1
                break
        return 0

    def deccoord(self,chaine):
        "renvoie les points floatwkt"#[[x1,y1],[x2,y2]...]
        listepoints=[]
        doubletpoints=[]
        points=''
        doublet=0
        o=1 #initialisation d'une variable o pour la boucle for pour checker le caractère dapres
        for m in range(len(chaine)):
            if self.estUnCarPoints(chaine[m]):#éventuellement utiliser fonction caractère alphanumérique
                points+=chaine[m]
            if self.estUnCarPoints(chaine[o])==0 and self.estUnCarPoints(chaine[m])==1:
            #print(points)
                doubletpoints.append(float(points))#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                doublet+=1
                points=''
                if doublet==2:
                    listepoints.append(doubletpoints)
                    doubletpoints=[]
                    doublet=0
            if o!=len(chaine)-1:
                o+=+1
            #print(o)#on affecte à o la valeur de m+1 à la fin de la boucle 
        #index+=1#on indexe à +1 pour le caractère suivant
        return listepoints


    def coord(self,chaine):
        "renvoie les points intwkt"#[[x1,y1],[x2,y2]...]
        listepoints=[]
        doubletpoints=[]
        points=''
        doublet=0
        inter=1
        o=1 #initialisation d'une variable o pour la boucle for pour checker le caractère dapres
        for m in range(len(chaine)):
            if self.chiffreEstUn(chaine[m]):#éventuellement utiliser fonction caractère alphanumérique
                points+=chaine[m]
            if self.chiffreEstUn(chaine[o])==0 and self.chiffreEstUn(chaine[m])==1:
            #print(points)
                if inter==1:
                    doubletpoints.append(int(points))#ajoute à la listemots une liste du mot et l'index de son premier caractère dans la chaine 
                    doublet+=1
                    points=''
                    inter=0
                    if doublet==2:
                        listepoints.append(doubletpoints)
                        doubletpoints=[]
                        doublet=0
                else:
                    points=''
                    inter=1
            if o!=len(chaine)-1:
                o+=+1
            #print(o)#on affecte à o la valeur de m+1 à la fin de la boucle 
        #index+=1#on indexe à +1 pour le caractère suivant
        return listepoints


### fonction fichier

    def import_(self,csv,sep):
    #chaque element de readlines est decomposé en une liste des éléments du csv
        fi=open(csv,'r')
        fil=fi.readlines()
        result=[0]*(len(fil))###-1 pour scripting psql, sinon len(fil)
        for i in range(len(fil)):###-1 pour scripting psql, sinon len(fil)
            fil[i]=fil[i].strip('\n')
            listline=self.decomposingpropre(fil[i],sep)
            result[i]=listline
        fi.close()
        return result

    def export_(self,table,csv,sep):
    #liste de liste dans table est transformé en csv separé par sep
        fi=open(csv,'w')
        for i in table:
            last=len(i)-1
            for j in range(len(i)-1):#verif\n
                fi.write(i[j])
                fi.write(sep)
            fi.write(str(i[last])+'\n')
        fi.close()


###fonctions geom

    def valeursabs(self,x):
        if x<0:
            return -x
        if x>=0:
            return x 



#un point par quart d'unite(distance de d unites)
#utile pour intersect
#arrondi qui aligne sur une grille en .5
    def aplatissementfin(self,xy):
        "renvoie une liste de tuple des points interpolés entre X(a,b) et Y(d,e)"
        d=self.distancepoint(xy)
        if d<=0.25:
            return xy
        elif self.valeursabs(xy[1][0]-xy[0][0])!=0:
            xi=xy[0][0]
            dx=(1/4)*(1/d)*self.valeursabs(xy[1][0]-xy[0][0])
            boolean=(xy[1][0]-xy[0][0])/self.valeursabs(xy[1][0]-xy[0][0])
            result=[0]*(2+(int(4*d)-1))##verifier les limites
            result[0]=[self.arrondi(xy[0][0]),self.arrondi(xy[0][1])]
            for i in range(1,len(result)-1):
                xi+=boolean*dx
                yi=xy[0][1]*(xy[1][0]-xi)/(xy[1][0]-xy[0][0])+xy[1][1]*(xi-xy[0][0])/(xy[1][0]-xy[0][0])
                result[i]=(self.arrondi(xi),self.arrondi(yi))
            result[len(result)-1]=[self.arrondi(xy[1][0]),self.arrondi(xy[1][1])]
        else:
            yi=xy[0][1]
            dy=(1/4)*(1/d)*self.valeursabs(xy[1][1]-xy[0][1])
            boolean=(xy[1][1]-xy[0][1])/self.valeursabs(xy[1][1]-xy[0][1])
            result=[0]*(2+(int(4*d)-1))##verifier les limites
            result[0]=[self.arrondi(xy[0][0]),self.arrondi(xy[0][1])]
            for i in range(1,len(result)-1):
                yi+=boolean*dy
                xi=xy[0][0]+((yi-xy[0][1])/(xy[1][1]-xy[0][1]))*(xy[1][0]-xy[0][0])
                result[i]=(self.arrondi(xi),self.arrondi(yi))
            result[len(result)-1]=[self.arrondi(xy[1][0]),self.arrondi(xy[1][1])]
        return result

#un point par unite de distance, utile pr distancelignepoint
    def aplatissement(self,xy):
        "renvoie une liste de tuple des points interpolés entre X(a,b) et Y(d,e)"
        d=self.distancepoint(xy)
        if d<=1:
            return xy
        elif self.valeursabs(xy[1][0]-xy[0][0])!=0:
            xi=xy[0][0]
            dx=(1/d)*self.valeursabs(xy[1][0]-xy[0][0])
            boolean=(xy[1][0]-xy[0][0])/self.valeursabs(xy[1][0]-xy[0][0])
            result=[0]*(2+(int(d)-1))##verifier les limites
            result[0]=xy[0]
            for i in range(1,len(result)-1):
                xi+=boolean*dx
                yi=xy[0][1]*(xy[1][0]-xi)/(xy[1][0]-xy[0][0])+xy[1][1]*(xi-xy[0][0])/(xy[1][0]-xy[0][0])
                result[i]=(xi,yi)
            result[len(result)-1]=xy[1]
        else:
            yi=xy[0][1]
            dy=(1/d)*self.valeursabs(xy[1][1]-xy[0][1])
            boolean=(xy[1][1]-xy[0][1])/self.valeursabs(xy[1][1]-xy[0][1])
            result=[0]*(2+(int(d)-1))##verifier les limites
            result[0]=xy[0]
            for i in range(1,len(result)-1):
                yi+=boolean*dy
                xi=xy[0][0]+((yi-xy[0][1])/(xy[1][1]-xy[0][1]))*(xy[1][0]-xy[0][0])
                result[i]=(xi,yi)
            result[len(result)-1]=xy[1]
        return result


    def ligneinterpolee(self,geomligneastext):
        listpointslignes=self.deccoord(geomligneastext)
        result=[]
        for i in range(len(listpointslignes)-1):
            result+=self.aplatissement([listpointslignes[i],listpointslignes[i+1]])
        return result

    def ligneinterpoleefine(self,geomligneastext):
        listpointslignes=self.deccoord(geomligneastext)
        result=[]
        for i in range(len(listpointslignes)-1):
            result+=self.aplatissementfin([listpointslignes[i],listpointslignes[i+1]])
        return result

    def llgne(self,geomligneastext):
    #longueurligne
        listpointslignes=self.deccoord(geomligneastext)
        distancetot=0
        for i in range(len(listpointslignes)-1):
            d=self.distancepoint([listpointslignes[i],listpointslignes[i+1]])
            distancetot+=d      
        return distancetot

    def distancepoint(self,xy):
        "distance euclidienne,le systeme de coordonnees nest pas precise"
        if self.valeursabs(xy[0][0]-xy[1][0])<100000 and self.valeursabs(xy[0][1]-xy[1][1])<100000:##verifier limit 
            d=((xy[1][1]-xy[0][1])**2+(xy[1][0]-xy[0][0])**2)**(1/2)
        else:
            d=666666
        return d

    def distpolygonline(self,geompolygontext,geomlignetext):
        geomxy=self.deccoord(geompolygontext)
        xypoly=self.centroide(geomxy)
        listexyinterpolligne=self.ligneinterpolee(geomlignetext)
        result=[]
        for i in listexyinterpolligne:
            d=self.distancepoint([xypoly,i])
            result.append(d)
        mind=min(result)
        return mind

    def distpointline(self,geompointtext,geomlignetext):
#prend point ou polygone en geompointtext
        geomxy=self.deccoord(geompointtext)
        if len(geomxy)>1:
            geomxy=self.centroide(geomxy)
        else:
            geomxy=geomxy[0]
        listexyinterpolligne=self.deccoord(geomlignetext)
#version interpolee ligneinterpolee(geomlignetext)
        result=[]
        for i in listexyinterpolligne:
            d=self.distancepoint([geomxy,i])
            result.append(d)
        mind=min(result)
        return mind

    def arrondi(self,nb):
#arrondi à .0 ou .5 le chiffre décimal
        if round(nb,1)-round(nb)==0:
            nombre=round(nb,1)   
        elif (round(nb,1)-round(nb))<0.25 and (round(nb,1)-round(nb))>-0.25:
            nombre=round(nb)
        elif (round(nb,1)-round(nb))<=-0.25:
            nombre=round(nb)-0.5
        elif (round(nb,1)-round(nb))>=0.25: 
            nombre=round(nb)+0.5
        return nombre

#n'a de sens que si les interpolations sont calibrées correctement
    def intersect(self,geomlignetext1,geomlignetext2):
        listexyinterpolligne1=self.ligneinterpoleefine(geomlignetext1)
        listexyinterpolligne2=self.ligneinterpoleefine(geomlignetext2)
        for i in listexyinterpolligne1:
            #print(i)
            for j in listexyinterpolligne2:
                #print(j)
                if self.distancepoint([i,j])<0.001:###i==j moyen en float aparemment
                    return True
        return False

    def intst(self,geomligne1,geomligne2):
#ligne1[[x,y],[x1,y1],...] ligne2idem
#idem fonction au dessus mais avec ligne deja interpolees
        for i in geomligne1:
            for j in geomligne2:
                #print(self.distancepoint([i,j]))
                if self.distancepoint([i,j])<0.001:
                    return True
        return False

    def centroide(self,geompolygone):
#geompolygone est une liste
#coordpoints=deccoord(geompolygone)
        sigmaX=0
        sigmaY=0
        for i in geompolygone:
            sigmaX+=i[0]
            sigmaY+=i[1]
        X=sigmaX/(len(geompolygone))
        Y=sigmaY/(len(geompolygone))
        return [X,Y]

    def dist_building_item(self,csv1,geoname,csv2):
#csv1building,csv2item
#geom=last_col_ds_csv2
#geoname=nomgeom_col_ds_csv1
        buildinglist=self.import_(csv1,'#')
        buildinglist[0].append('dist_'+str(csv2))
        itemlist=self.import_(csv2,'#')

        for k in range(len(buildinglist[0])):
            if buildinglist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        m=0
        fin=len(buildinglist)-1
        for i in range(1,len(buildinglist)-1):#-1 car psql
            temp=[]
            bgeomtext=buildinglist[i][bgeom]
            coordbpoints=self.deccoord(bgeomtext)
            XYbgeomtext=self.centroide(coordbpoints)
            if len(itemlist)<=2:
                d=666666
            else:
                for j in range(1,len(itemlist)-1):#-1 car psql
                    itgeom=len(itemlist[j])-1
                    itgeomtext=itemlist[j][itgeom]
                    XYPt=self.deccoord(itgeomtext)
                    if len(XYPt)<=1:#prend les geometries pt et polygones en csv2
                        XYPt=XYPt[0]
                    else:
                        XYPt=self.centroide(XYPt)##voir pour les infras
                    dist=self.distancepoint([XYbgeomtext,XYPt])
                    temp.append(dist)
                    d=min(temp)
            buildinglist[i].append(d)
            m+=1
            print('avancement : '+str(csv2)+' '+str(m)+'/ '+str(fin))
        self.export_(buildinglist,csv1,'#')

    def dist_building_infra0(self,csv1,geoname,csv2):
#csv1building,csv2item
#geom=last_col_ds_csv2
#geoname=nomgeom_col_ds_csv1
        buildinglist=self.import_(csv1,'#')
        buildinglist[0].append('dist_'+str(csv2))
        itemlist=self.import_(csv2,'#')

        for k in range(len(buildinglist[0])):
            if buildinglist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        m=0
        fin=len(buildinglist)-1
        for i in range(1,len(buildinglist)-1):#-1 car psql
            d=17
            buildinglist[i].append(d)
            m+=1
            print('avancement : '+str(csv2)+' '+str(m)+'/ '+str(fin))
        self.export_(buildinglist,csv1,'#')


    def dist_building_infra(self,csv1,geoname,csv2):
#csv1building,csv2item
#geom=last_col_ds_csv2
#geoname=nomgeom_col_ds_csv1
        buildinglist=self.import_(csv1,'#')
        buildinglist[0].append('dist_'+str(csv2))
        itemlist=self.import_(csv2,'#')

        for k in range(len(buildinglist[0])):
            if buildinglist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        m=0
        fin=len(buildinglist)-1
        for i in range(1,len(buildinglist)-1):#-1 car psql
            temp=[]
            bgeomtext=buildinglist[i][bgeom]
            if len(itemlist)<=2:
                d=666666
            else:
                for j in range(1,len(itemlist)-1):#-1 car psql
                    itgeom=len(itemlist[j])-1
                    itgeomtext=itemlist[j][itgeom]
                    dist=self.distpointline(bgeomtext,itgeomtext)
                    temp.append(dist)
                    d=min(temp)
            buildinglist[i].append(d)
            m+=1
            print('avancement : '+str(csv2)+' '+str(m)+'/ '+str(fin))
        self.export_(buildinglist,csv1,'#')


    def km1pt5_nb_building_tc(self,csv1,geoname,csv2):
        'ajoute un item compteur du nb de tc à moins de 1,5 km de csv2'
#csv1building,csv2item
#geom=last_col_ds_csv2
#geoname=nomgeom_col_ds_csv1
        buildinglist=self.import_(csv1,'#')
        buildinglist[0].append('nb_tcpt')
        itemlist=self.import_(csv2,'#')

        for k in range(len(buildinglist[0])):
            if buildinglist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        m=0
        fin=len(buildinglist)-1
        for i in range(1,len(buildinglist)-1):#-1 car psql
            temp=[]
            bgeomtext=buildinglist[i][bgeom]
            coordbpoints=self.deccoord(bgeomtext)
            XYbgeomtext=self.centroide(coordbpoints)
            compteur=0
            if len(itemlist)<=2:
                compteur=0
            else:
                for j in range(1,len(itemlist)-1):#-1 car psql
                    itgeom=len(itemlist[j])-1
                    itgeomtext=itemlist[j][itgeom]
                    XYPt=self.deccoord(itgeomtext)
                    if len(XYPt)<=1:#prend les geometries pt et polygones en csv2
                        XYPt=XYPt[0]
                    else:
                        XYPt=self.centroide(XYPt)##voir pour les infras
                    dist=self.distancepoint([XYbgeomtext,XYPt])
                    if dist<1500:
                        compteur+=1
            buildinglist[i].append(compteur)
            m+=1
            print('avancement : '+str(m)+'/ '+str(fin))
        self.export_(buildinglist,csv1,'#')


    def contain(self,chaine1,chaine2):
        'true si chaine1 inclut ds chaine2'
        chaine1=chaine1.lower()
        chaine2=chaine2.lower()
        L=len(chaine1)
        if len(chaine2)<len(chaine1):
            return False
        for i in range(len(chaine2)-L+1):
            slicee=''
            for j in range (L):
                slicee+=chaine2[i+j]
            if slicee==chaine1:
                return True
        return False

#calibration construction
##trop grand=risque de clusters disjoints, 
#trop petits=risque de 0,1,2,3 non rep de la surf tot
#1/V2r1 car si un carré contient un item, tout le carré (et les 
#3 autours) sont positifs
###l'hypothèse que les négatifs sont disjoints est pas 
#systématique, mais le rayon de courbure de r2 fait que 
#la surface des deux ensembles disjoints tient dans une bande 
#fine. non démontré mathématiquement, mais empiriquement au compas la surface 
#des deux ensembles negatifs disjoints sera faiblement supérieure 
#à celle d'un seul ensemble compte tenu de la surface des positifs 
#et du rayon de courbure choisi
    def cali(self,fichierbuildings,bbox,col,r1):#algo \surface des positifs
#col int index colonne distanceàitem
#bbox=[xmin,xmax,ymin,ymax]
        
#(m)r1 rayon max à building
        lpt=[]#liste des points ok (<=r1)
        lpt0=[]#liste des points negatifs
        pos=0
        poum=0
        sdelta=0
        s=0
        s2=0
        liste=self.import_(fichierbuildings,'#')
        for k in range(len(liste[0])):
                if self.contain('geom',liste[0][k]):#on considere que la colonne geometrie contient geom
#par defaut psql n'ajoute pas des " aux noms de colonnes
                    geom=k
        for i in range(1,len(liste)-1):#-1 car psql dans un premier temps
        #print(k)
        #print(liste[i])
            geomxy=self.deccoord(liste[i][geom])
        #print(geomxy)
            if float(liste[i][col])<=r1:#dist du building à service de la ligne i <r1?
                lpt.append(geomxy)#liste de polygones [[[x1,y1],[x2,y2],..],[[xa1,ya1],[xa2,ya2],...]       
            else:
                lpt0.append(geomxy)
        di=(1/(2**(1/2)))*r1###un item couvre 4 carrés + 12% de surface (0,55*surface dun carré)
        quadri=self.subbox(bbox,di)
        fin=len(quadri)
        k=0
        for i in quadri:##optimisation:ici on reparcours toute la liste pleins de fois,gist index spatial permet d'éviter ca
            resulti=self.selection(lpt,i)#lpt liste de polygones,resulti liste de points
            result0=self.selection(lpt0,i)###ces deux lignes sont selection() i fois alors qu'on pourrait parcourir une seule fois
            if (len(resulti)==0 or len(resulti)==1 or len(resulti)==2) and len(result0)>2:#eventuellement ajuster avec distance des points et % arbitraire
                positif=0
                positifprct=0
                negatif=self.fonction(result0)#surf triangulée negative
                negatifprct=negatif/(positif+negatif)
            elif (len(result0)==0 or len(result0)==1 or len(result0)==2) and len(resulti)>2:
                negatif=0
                negatifprct=0
                positif=self.fonction(resulti)#surf triangulée positive
                positifprct=positif/(positif+negatif)
            elif (len(resulti)==0 or len(resulti)==1 or len(resulti)==2) and (len(result0)==0 or len(result0)==1 or len(result0)==2):
                negatif=0
                negatifprct=0
                positif=0
                positifprct=0
            else: 
                positif=self.fonction(resulti)#surf triangulée positive
                negatif=self.fonction(result0)#surf triangulée negative
                positifprct=positif/(positif+negatif)
                negatifprct=negatif/(positif+negatif)
            s2+=(i[1]-i[0])*(i[3]-i[2])
            s=(i[1]-i[0])*(i[3]-i[2])#=di**2 le plus souvent
            sdelta+=s-positif-negatif#surface non couverte par le calcul triangulaire
            pos+=positifprct*s#extrapolation surf dans r1
            poum+=negatifprct*s#extrapolation surf hors r1
            k+=1
            print('avancement : '+str(k)+'/ '+str(fin))
        stot=(bbox[1]-bbox[0])*(bbox[3]-bbox[2])#surf si tout est <=r1
        nbitemfull=stot/(4.55*di*di)#4,55*di*di pr etre exact mais des sommes de cercles ne couvrent pas tout l'espace
        nbitemestimee=pos/(4.55*di*di)#
        nbitemaconstruire=poum/(4.55*di*di)#
        if s-stot>1:
            return (print('il y a eu un soucis de subbox'))
        else:
            return [nbitemaconstruire,100*(sdelta/stot),nbitemfull,nbitemestimee,stot,s2,sdelta,pos,poum]
#result sur bikeinfo sur mayotte:
#[49.90924478134292, 99.42315453717555, 309.94992216836744, 12.068777196678223, 1586556164.0993307, 1586556164.0993345, 1577404186.851562, 61777053.27549664, 255472946.72449902]
#note: nbitemfull prend tout le carré de la bbox meme sil ny a pas de batiments
#dou un chiffre nettement plus elevés que nbitemestimee et aconstruire fondés
#sur pos et poum

    def subbox(self,bbox,a):
#bbox=[xmin,xmax,ymin,ymax]
#renvoie une liste de bboxes de cote a a l'interieur de bbox
        bboxx=bbox[1]-bbox[0]
        bboxy=bbox[3]-bbox[2]
        nbx=int(bboxx/a)
        nby=int(bboxy/a)
        restex=bboxx%a
        #print(restex)
        restey=bboxy%a
        #print(restey)
        result=[]
        for i in range(nbx):
            for j in range (nby):
                if j<nby-1 and i==nbx-1:
                    sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+j*a,bbox[2]+(j+1)*a]
                    result.append(sub)
                    if restex>0:
                        sub=[bbox[0]+(i+1)*a,bbox[0]+(i+1)*a+restex,bbox[2]+j*a,bbox[2]+(j+1)*a]
                        result.append(sub)
                elif i<nbx-1 and j==nby-1:
                    sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+j*a,bbox[2]+(j+1)*a]
                    result.append(sub)
                    if restey>0:
                        sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+(j+1)*a,bbox[2]+(j+1)*a+restey]
                        result.append(sub)
                elif i==nbx-1 and j==nby-1:
                    sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+j*a,bbox[2]+(j+1)*a]
                    result.append(sub)
                    if restex>0 and restey>0:
                        sub=[bbox[0]+(i+1)*a,bbox[0]+(i+1)*a+restex,bbox[2]+(j)*a,bbox[2]+(j+1)*a]
                        result.append(sub)
                        sub=[bbox[0]+(i)*a,bbox[0]+(i+1)*a,bbox[2]+(j+1)*a,bbox[2]+(j+1)*a+restey]
                        result.append(sub)
                        sub=[bbox[0]+(i+1)*a,bbox[0]+(i+1)*a+restex,bbox[2]+(j+1)*a,bbox[2]+(j+1)*a+restey]
                        result.append(sub)
                    if restex>0 and restey==0:
                        sub=[bbox[0]+(i+1)*a,bbox[0]+(i+1)*a+restex,bbox[2]+j*a,bbox[2]+(j+1)*a]
                        result.append(sub)                
                    if restex==0 and restey>0:
                        sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+(j+1)*a,bbox[2]+(j+1)*a+restey]
                        result.append(sub)                    
                else:
                    sub=[bbox[0]+i*a,bbox[0]+(i+1)*a,bbox[2]+j*a,bbox[2]+(j+1)*a]
                    result.append(sub)
            #print(result)
        return result

    def selection(self,lpt,bbox):
#renvoie les points de lpt dans bbox
#bbox=[xmin,xmax,ymin,ymax]
#lpt=[[[x1,y1],[x2,y2]...],[[xa1,ya1],[xa2,ya2]]..]
        result=[]
        for i in lpt:#i est une liste de points
            XYbgeomtext=self.centroide(i)
            if (XYbgeomtext[0]>=bbox[0] and XYbgeomtext[0]<=bbox[1] and XYbgeomtext[1]>=bbox[2] and XYbgeomtext[1]<=bbox[3]):
                result.append(XYbgeomtext)
        return result
      

###decouper bbox en tranches dx,dy de longueur r2
###r2=r1/V2
###pour chaque tranche:
###selectionner tous les points negatifs dans la tranche
###hypothese:1 seul ensemble dans la tranche dxdy
###calculer la surface que cela respresente
###definir un % par rapport à la surf du carré dx,dy
###sommer les % de tous les carrés dx,dy
###un item couvre 4 carrés + 12% de surface (0,55*surface dun carré)
###S=1/4 de la sum
###S/pi*r1² = le nb d'item à ajouter pr tout couvrir à 12% pres 
###(on peut approximer mieuxx en divisant mieux)




    def surfbh(self,triangle):
        "methode non utilisee:surf triangle par base*hauteur/2"
#liste [[x1,y1],[x2,y2],[x3,y3]]
        if len(triangle)>3:
            return false
        else:
            b=self.distancepoint([triangle[0],triangle[1]])
            ligne=self.aplatissement([triangle[0],triangle[1]])
#prolonger ligne à partir du xminxmaxyminymax des deux premiers points
#pr etre sur d'avoir l'orthogonalité
#pas utile en termes de temps machine pr calcul surface mais peut etre utile 
#pr d'autres usages
            result=[]
            for i in ligne:
                d=self.distancepoint([triangle[2],i])
                result.append(d)
            h=min(result)
            s=b*h/2
        return s


    def surf(self,triangle):
        "methode surf triangle par determinant matriciel"
#liste [[x1,y1],[x2,y2],[x3,y3]]
#det:1/2[x1(y2–y3)+x2(y3–y1)+x3(y1–y2)]
        s=(1/2)*(triangle[0][0]*(triangle[1][1]-triangle[2][1])+triangle[1][0]*(triangle[2][1]-triangle[0][1])+triangle[2][0]*(triangle[0][1]-triangle[1][1]))
        return self.valeursabs(s)

    def dedans(self,pt,listetriangle):
#1 si pt dans 1 triangle de listetriangle
        for i in listetriangle:
            s=self.surf(i)
            s1=self.surf([pt,i[0],i[1]])
            s2=self.surf([pt,i[0],i[2]])
            s3=self.surf([pt,i[1],i[2]])
            if s1+s2+s3-s<0.001:
                return 1

    def indexdist(self,pt,lpt):
#ordonne la liste des points lpt selon la distance à pt
        result0=[]
        result=[]
        for i in lpt:
            result0.append([i,self.distancepoint([i,pt])]) 
        result0.sort(key=lambda x: x[1])
        for k in result0:
            result.append(k[0])
        return result


    def fonction(self,l):
#optimisation:voirtheoriedesgraphs
#l=[pt1,pt2,...,ptn]
        s=0
        lt=[]#listedetriangles
        lpt=[]#listedepoints
        xxyy=[]#[[xmin,xmax],[ymin,ymax]]
        t=[]#triangle
        for i in range(len(l)):
            if i<3:
                t.append(l[i])#i<3
                lpt.append(l[i])
                if i==2:
                    lt.append(t)
                    xxyy=self.minmaxt(lt)
        #condition sur i>=3
            else:
            #verifier l[i] hors des polygones
                boolean=1
                if (l[i][0]-xxyy[0][0]>=0 and l[i][0]-xxyy[0][1]<=0 and l[i][1]-xxyy[1][0]>=0 and l[i][1]-xxyy[1][1]<=0):
#hors xminxmaxyminymax:out dans xminxmax hors yminymax:out hors xminxmax dans yminymax:out 
#dans xminxmax dans yminymax surf sommedestriangles=surftriangleprecedent->in surf sommedestriangles>surftriangleprecedent->out
                    if self.dedans(l[i],lt):#dedans(pt,listetriangle) renvoie 1 si à l'interieur d'un des triangle
                        boolean=0
                else:
                    pass

                if boolean!=0:
                #creertriangle_pt1
                    t=[l[i]]
                    pt1=self.indexdist(l[i],lpt)#liste des points triés selon distance à l[i]
###ici le index gist en postgis accelererait probablement
                    t.append(pt1[0])

                #creertriangle_pt2
                    for k in range(1,len(pt1)):
                        #lignei=aplatissementfin([l[i],pt1[k]])
                        #listlignes=toile(pt1,k)#liste des lignes entre pt1[0] et les pts de pt1 sauf le kieme
                        if self.secantlll([l[i],pt1[k]],pt1,k):#si la ligne l[i]pt1[k] interesecte une des lignes pt1[0]autresptsdept1
                            pass
                        else:
                            t.append(pt1[k])  
                            #print(t)
                            lt.append(t)
                            xxyy=self.minmaxt(lt)
                            lpt.append(l[i])
                            break
    #bouclesurf
    #print(lt)
        for j in lt:
            s+=self.surf(j)
        return s

    def minmaxt(self,lt):
#renvoie les xxyy=[]#[[xmin,xmax],[ymin,ymax]] de la liste de triangle
        X=[]
        Y=[]
        for i in lt:
            X.append(i[0][0])
            X.append(i[1][0])             
            X.append(i[2][0])
            Y.append(i[0][1])
            Y.append(i[1][1])
            Y.append(i[2][1])
        xmin=min(X)
        xmax=max(X)
        ymin=min(Y)
        ymax=max(Y)
        return [[xmin,xmax],[ymin,ymax]]


    def intersectlll(self,ligne,listligne):
#renvoie 1 si ligne interesecte une autre ligne de listligne
        for i in listligne:
            if self.intst(ligne,i):
                return 1

    def secantlll(self,xy1,lpt,k):
#renvoie 1 si xy1 et les lignes de lpt sont secantes sauf la kieme
        for i in range(1,len(lpt)):
            if i==k:
                pass
            if self.secant(xy1,[lpt[0],lpt[i]]):
                return 1

    def secant(self,xy1,xy2):
#return 1 si xy1 et xy2 sont secants par determinant matriciel
#xy[[x1,y1],[x'1,y'1]]
#si colinéaire det=0
        x1min=min(xy1[0][0],xy1[1][0])
        x1max=max(xy1[0][0],xy1[1][0])
        y1min=min(xy1[0][1],xy1[1][1])
        y1max=max(xy1[0][1],xy1[1][1])
        x2min=min(xy2[0][0],xy2[1][0])
        x2max=max(xy2[0][0],xy2[1][0])
        y2min=min(xy2[0][1],xy2[1][1])
        y2max=max(xy2[0][1],xy2[1][1])
        xmax=max(x1max,x2max)
        xmin=min(x1min,x2min)
        #print(xy1)
        #print(xy2)
        #print(self.valeursabs((xy1[1][0]-xy1[0][0])*(xy2[1][1]-xy2[0][1])-(xy1[1][1]-xy1[0][1])*(xy2[1][0]-xy2[0][0])))
        if self.valeursabs((xy1[1][0]-xy1[0][0])*(xy2[1][1]-xy2[0][1])-(xy1[1][1]-xy1[0][1])*(xy2[1][0]-xy2[0][0]))==0:##etonnant, pas ==0 sur un cas a1=a2
            return 0
        if ((x1max<x2min) or (x1min>x2max)) and ((y1max<y2min) or (y1min>y2max)):#condition sur disjoints
            return 0
        else:#equations ax+b
            if (xy1[1][0]-xy1[0][0])==0:
                a2=(xy2[1][1]-xy2[0][1])/(xy2[1][0]-xy2[0][0])
                b2=xy2[1][1]-a2*(xy2[1][0])    
                y2=a2*xy1[1][0]+b2
                if y2<=y1max and y2>=y1min:
                    return 1
                else: 
                    return 0
            elif (xy2[1][0]-xy2[0][0])==0:
                a1=(xy1[1][1]-xy1[0][1])/(xy1[1][0]-xy1[0][0])
                b1=xy1[1][1]-a1*(xy1[1][0])
                y1=a1*xy2[1][0]+b1
                if y1<=y2max and y1>=y2min:
                    return 1
                else:
                    return 0
            else:
                a1=(xy1[1][1]-xy1[0][1])/(xy1[1][0]-xy1[0][0])
                b1=xy1[1][1]-a1*(xy1[1][0])
                a2=(xy2[1][1]-xy2[0][1])/(xy2[1][0]-xy2[0][0])
                b2=xy2[1][1]-a2*(xy2[1][0])
                if ((b2-b1)/(a1-a2))<=xmax and ((b2-b1)/(a1-a2))>=xmin:
                    return 1

    def toile(self,lpt,k):
#renvoie la liste des lignes interpolées entre lpt[0] et les pts de lpt sauf le kieme
        result=[]
        for i in range(1,len(lpt)):
            if i==k:
                pass
            else:
                lignei=self.aplatissementfin([lpt[0],lpt[i]])
                result.append(lignei)
        return result


###calcul des ranking service mobilité
#faire apparaitre hypothèses:
#service mobilite accessible considéré comme tel si :
#temps acces inferieur ou egal à 15min
#<-> 12.5km en voiture
#<-> 5km en velo
#<-> 1.5km a pied
###pour amelioration:densite d'infra et pas un seul point
###pour amelioration: 

    def rank(self,csv):
#scoring sur distances à differents services auto,velo et tc
#attention les numeros dependent de l'ordre dans lequel les
#calculs précédents ont été faits
        buildinglist=self.import_('building','#')
        buildinglist[0].append('rank_auto')
        for i in range(1,len(buildinglist)-1):#-1 car psql
            rrankauto=0
            for j in range(6,12):#ranking sur 6 items
                if float(buildinglist[i][j])<12500:
                    rrankauto+=1
            buildinglist[i].append(str(rrankauto))
        buildinglist[0].append('rank_velo')
        for i in range(1,len(buildinglist)-1):#-1 car psql
            rrankvelo=0
            for j in range(12,17):#ranking sur 5 items
                if float(buildinglist[i][j])<5000:
                    rrankvelo+=1
            buildinglist[i].append(str(rrankvelo))
        buildinglist[0].append('rank_tc')
        for i in range(1,len(buildinglist)-1):#-1 car psql
            rranktc=0
            if float(buildinglist[i][18])<1500:
                rranktc+=1
            rranktc+=int(buildinglist[i][19])
            buildinglist[i].append(str(rranktc))
        self.export_(buildinglist,csv,'#')

    def geot(self,building,geoname,occsol):
#calcule les buildings potentiellement ok pour accueillir une
#installation geothermique individuelle
#geoname=nomgeom_col_ds_building_et_occsol
        buildinglist=self.import_(building,'#')
        buildinglist[0].append('geot')
        ocs=self.import_(occsol,'#')
        for a in range(len(ocs[0])):
            if ocs[0][a]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                ocsgeom=a
        for k in range(len(buildinglist[0])):
            if buildinglist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        for b in range(1,len(ocs)-1):#-1 car psql 
            ocsgeom=len(ocs[b])-1
            ocsgeomtext=ocs[b][ocsgeom]
            XYPtj=self.deccoord(ocsgeomtext)
            ocs[b].append(XYPtj)#ocs[j][-2]
            surfj=self.fonction(XYPtj)
            ocs[b].append(surfj)#ocs[j][-1]
            #print(str(b))
        m=0
        fin=len(buildinglist)-1
        for i in range(1,len(buildinglist)-1):#-1 car psql
            buildinglist[i].append('non')#buildinglist[i][-1]
            bgeomtext=buildinglist[i][bgeom]
            coordbpoints=self.deccoord(bgeomtext)
            surfi=self.fonction(coordbpoints)
            XYbi=self.centroide(coordbpoints)
            valeurseuil=2*(surfi/3.141592653589793)**(1/2)#r à definir
            for j in range(1,len(ocs)-1):#-1 car psql 
                XYPtj=ocs[j][-2]
                surfj=ocs[j][-1]
                compteur=0
                temp=[]
                for l in XYPtj:
                    dist=self.distancepoint([XYbi,l])
                    temp.append(dist)
                for n in temp:
                    if n<valeurseuil:#mettre en boucle sur dist list
                        compteur+=1#compteur sur ocs
                if compteur>=2 and surfj>=surfi:
                    ocs[j][-1]-=surfi#on enleve du potentiel à hypothese 1:1
                    buildinglist[i][-1]='oui'
            m+=1
            print('avancement : '+str(occsol)+' '+str(m)+'/ '+str(fin))
        self.export_(buildinglist,building,'#')

    def p2g(self,stationscsv,geoname,occsol):
#calcule les buildings potentiellement ok pour accueillir une
#installation geothermique individuelle
#geoname=nomgeom_col_ds_building_et_occsol
        stationlist=self.import_(carfuel.csv,'#')
        stationlist[0].append('p2g')
        ocs=self.import_(occsol,'#')
        for a in range(len(ocs[0])):
            if ocs[0][a]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                ocsgeom=a
        for k in range(len(stationlist[0])):
            if stationlist[0][k]==geoname:#par defaut psql n'ajoute pas des " aux noms de colonnes
                bgeom=k
        for b in range(1,len(ocs)-1):#-1 car psql 
            ocsgeom=len(ocs[b])-1
            ocsgeomtext=ocs[b][ocsgeom]
            XYPtj=self.deccoord(ocsgeomtext)
            ocs[b].append(XYPtj)#ocs[j][-2]
            surfj=self.fonction(XYPtj)
            ocs[b].append(surfj)#ocs[j][-1]
            #print(str(b))
        m=0
        fin=len(stationlist)-1
        for i in range(1,len(stationlist)-1):#-1 car psql
            stationlist[i].append('non')#buildinglist[i][-1]
            bgeomtext=stationlist[i][bgeom]
            coordbpoints=self.deccoord(bgeomtext)
            XYbi=self.centroide(coordbpoints)
            valeurseuil=2*(surfi/3.141592653589793)**(1/2)#r à definir
            for j in range(1,len(ocs)-1):#-1 car psql 
                XYPtj=ocs[j][-2]
                surfj=ocs[j][-1]
                compteur=0
                temp=[]
                XYPtjc=self.centroide(XYPtj)
                dist=self.distancepoint([XYbi,XYPtjc])
                if surfj>=12000 and dist<=1000:
                    stationlist[i][-1]='oui'
            m+=1
            print('avancement : '+str(occsol)+' '+str(m)+'/ '+str(fin))
        self.export_(stationlist,stationcsv,'#')

        
#a=Planning()
#b=a.cali('building',[495457.9997,534226.8225,8562211.9086,8603135.4149],4,1500)
#print(b)


#]]decomposing(self,chaine,car)
#decomposingpropre(self,chaine,car)
#matri(self,phrase,car)
#subchain(self,chaine,car1,car2)
#find(self,chaine,caractère,debut)
#slicationg(self,chaine,car)
#slicationd(self,chaine,car)
#estUnCarPoints(self,arg)
#chiffreEstUn(self,arg)
#deccoord(self,chaine)
#coord(self,chaine)
#import_(self,csv,sep)
#export_(self,table,csv,sep)
#valeursabs(self,x)
#aplatissementfin(self,xy)#interpolation sur grille 0.5 d'un doublet [[x1,y1],[x2,y2]]
#aplatissement(self,xy)##interpolation d'un doublet [[x1,y1],[x2,y2]]
#ligneinterpolee(self,geomligneastext)
#ligneinterpoleefine(self,geomligneastext)#sur grille en 0.5
#llgne(self,geomligneastext)
#distancepoint(self,xy)
#distpolygonline(self,geompolygontext,geomlignetext)
#distpointline(self,geompointtext,geomlignetext)
#arrondi(self,nb)
#intersect(self,geomlignetext1,geomlignetext2
#intst(self,geomligne1,geomligne2):#ligne1[[x,y],[x1,y1],...] ligne2idem
#centroide(self,geompolygone):#geompolygone est une liste
#dist_building_item(self,csv1,geoname,csv2)
#dist_building_infra0(self,csv1,geoname,csv2)
#dist_building_infra(self,csv1,geoname,csv2)
#km1pt5_nb_building_tc(self,csv1,geoname,csv2)
#contain(self,chaine1,chaine2)
#subbox(self,bbox,a)
#selection(self,lpt,bbox)
#surfbh(self,triangle)
#surf(self,triangle)
#dedans(self,pt,listetriangle)
#indexdist(self,pt,lpt)
#fonction(self,l)#l=[pt1,pt2,...,ptn]
#minmaxt(self,lt)#renvoie les xxyy=[]#[[xmin,xmax],[ymin,ymax]] de la liste de triangle
#intersectlll(self,ligne,listligne)
#secantlll(self,xy1,lpt,k)
#secant(self,xy1,xy2)
#toile(self,lpt,k)
#cali(self,fichierbuildings,bbox,col,r1)
#geot(self,building,geoname,occsol)

#rank(self,csv)#à tester avec appli un peu avancee
#p2g(self,stationscsv,geoname,occsol)

###listing automation calculs
import os
def listFileskw(folder,kw=''):
    'la fonction listFiles renvoie une liste des fichiers du répertoire courant contenant kw'                                           
    os.chdir(folder)    
    files=[]
    a=os.listdir()
    b=[]
    for i in a:
        if os.path.isfile(i) and contain(kw,i):
            files.append(i)
        else:
            b.append(i)
    return files 

###preciser l'automation des calculs, et verifier temps machine
#listcsv=listFileskw('/home/user','.csv')
#for i in listcsv:
#    dist_building_item(buildingfile,geoname,i):
#print(listcsv)


###Controller
class Appli(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        #variable du frame qui contiendra les vues
        self.container = tk.Frame(self)
        self.container.pack(side="top", fill="both", expand = True)
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)
        #variables intermédiaires pour la création d'un "évenement"
        
        #self.frames = {StartPage, CatOne, CatTwo,CatFact,DefineDetails,Resume,AfficheEvent,Imprimer}#,Bilan,ResultatBilan):}#dictionnaire de fonction
        #initialisation de la vue au démarrage de l'application
        self.page=StartPage0
        self.occsol=0
        self.show_frame(self.page)

    def show_frame(self, F):
        self.page=F
        frame = F(self.container, controller=self)
        frame.grid(row=0, column=0, sticky="nsew")#instanciation de la frame
        frame.tkraise()

    def defuser(self,user):
        self.user=user

    def create_occur(self,date=' ',what=' ',where=' ',who=' ',how=' ',people=' '):
        self.date=date
        self.occur=Occur(date,what,where,who,how,people)
        self.occurs[self.occur.name]=self.occur
    def lecture_occur(self,name):
        self.occurs[name].affichevariables()
    def create_bilan(self,bilank,chaines01):
        self.bilank=Bilan(bilank,chaines01)
        self.bilans[bilank]=self.bilank
    def lecture_bilan(self,name):
        return self.bilans[name].affichevariables()
    def create_contexte(self,contextname,chaines01,chainedates,chainecomments):
        self.context=Contexte(contextname,chaines01,chainedates,chainecomments)
        self.contexte[contextname]=self.context
    def lecture_contexte(self,name):
        return self.contexte[name].affichevariables()
    def changek1(self,arg):
        self.k1=str(arg)
    def changek2(self,arg):
        self.k2=str(arg)
    def changek(self,arg):
        self.k=str(arg)
    def changelistk2(self,arg):
        self.listk2=metier.selectk2(arg)
    def changelistk1k2(self,k1,k2):
        self.k1k2list=metier.select(k1,k2)
    def quit(self):
        self.destroy()

###View        
class StartPage0(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="accueil_page0", font=LARGE_FONT)
        label.grid(row=0,pady=10,padx=10)
        self.controller=controller
        self.entree=tk.Entry(self, width=40)
        self.entree.bind("<Key-Return>", self.Def)
        self.entree.grid(row=1)
        self.usertext=tk.Label(self,text="entrer un nom d'utilisateur")
        self.usertext.grid(row=2)
        button = tk.Button(self, text="OK",
                            command=lambda:self.DefineUser())
        button.grid(row=3)
    def Def(self,event):
        self.DefineUser()
    def DefineUser(self):
        self.user=self.entree.get()
        if self.user=='cnnr':
            self.controller.defuser(self.user)
            self.controller.show_frame(StartPage)        
        else:
            pass

###v1:à discuter si bbox ou relations ou autres pr preciser l'espace infra pbf
###v1:integrer un catche error?
class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="choix_espace", font=LARGE_FONT)
        label.grid(row=0,column=0)
        self.controller=controller
        label2 = tk.Label(self, text="SELECTIONNEZ LES DONNEES A CHARGER \n DANS LA BASE DE DONNEES. Les données téléchargées correspondent \n à osm au 01.09.20 et ocs_ge de l'ign \n (Martinique,Corse,MiPy,LanguedocRoussillon,PaysdeLaLoire)", font=LARGE_FONT)
        label2.grid(row=0,column=0)
        button1 = tk.Button(self, text="Guadeloupe",#5490
                            command=lambda: self.Guadeloupe())
        button1.grid(row=1,column=0)

        button2 = tk.Button(self, text="Martinique",#5490
                            command=lambda: self.Martinique())
        button2.grid(row=2,column=0)

        button3 = tk.Button(self, text="Guyane",#2972
                            command=lambda: self.Guyane())
        button3.grid(row=3,column=0)

        
        button4 = tk.Button(self, text="La Reunion",#2975
                            command=lambda: self.Lareunion())
        button4.grid(row=4,column=0)

        button5 = tk.Button(self, text="Mayotte",#4471
                            command=lambda: self.Mayotte())
        button5.grid(row=5,column=0)

        button6 = tk.Button(self, text="Alsace",#27572
                            command=lambda: self.Alsace())
        button6.grid(row=6,column=0)

        button7 = tk.Button(self, text="Aquitaine",#27572
                            command=lambda: self.Aquitaine())
        button7.grid(row=7,column=0)
        button8 = tk.Button(self, text="Auvergne",#27572
                            command=lambda: self.Auvergne())
        button8.grid(row=8,column=0)

        button9 = tk.Button(self, text="Basse-Normandie",#27572
                            command=lambda: self.Bassenormandie())
        button9.grid(row=9,column=0)

        button10 = tk.Button(self, text="Bourgogne",#27572
                            command=lambda: self.Bourgogne())
        button10.grid(row=10,column=0)
        button11 = tk.Button(self, text="Bretagne",#27572
                            command=lambda: self.Bretagne())
        button11.grid(row=11,column=0)

        button12 = tk.Button(self, text="Centre",#27572
                            command=lambda: self.Centre())
        button12.grid(row=12,column=0)
        button13 = tk.Button(self, text="Champagne-Ardenne",#27572
                            command=lambda: self.Champagneardenne())
        button13.grid(row=13,column=0)
        button14 = tk.Button(self, text="Corse",#27572
                            command=lambda: self.Corse())
        button14.grid(row=14,column=0)
        button15 = tk.Button(self, text="Franche-Comte",#27572
                            command=lambda: self.Franchecompte())
        button15.grid(row=1,column=1)
        button16 = tk.Button(self, text="Haute-Normandie",#27572
                            command=lambda: self.Hautenormandie())
        button16.grid(row=2,column=1)
        button17 = tk.Button(self, text="Ile-de-France",#27572
                            command=lambda: self.Iledefrance())
        button17.grid(row=3,column=1)
        button18 = tk.Button(self, text="Languedoc-Roussillon",#27572
                            command=lambda: self.Languedocroussillon())
        button18.grid(row=4,column=1)
        button19 = tk.Button(self, text="Limousin",#27572
                            command=lambda: self.Limousin())
        button19.grid(row=5,column=1)
        button20 = tk.Button(self, text="Lorraine",#27572
                            command=lambda: self.Lorraine())
        button20.grid(row=6,column=1)
        button21 = tk.Button(self, text="Midi-pyrenees",#27572
                            command=lambda: self.Midipyrenees())
        button21.grid(row=7,column=1)
        button23 = tk.Button(self, text="Nord-Pas-De-Calais",#27572
                            command=lambda: self.Nordpasdecalais())
        button23.grid(row=8,column=1)
        button24 = tk.Button(self, text="Pays-de-la-loire",#27572
                            command=lambda: self.Paysdelaloire())
        button24.grid(row=9,column=1)
        button25 = tk.Button(self, text="Picardie",#27572
                            command=lambda: self.Picardie())
        button25.grid(row=10,column=1)
        button26 = tk.Button(self, text="Poitou-Charentes",#27572
                            command=lambda: self.Poitoucharentes())
        button26.grid(row=11,column=1)
        button27 = tk.Button(self, text="PACA",#27572
                            command=lambda: self.PACA())
        button27.grid(row=12,column=1)
        button28 = tk.Button(self, text="Rhone-Alpes",#27572
                            command=lambda: self.Rhonealpes())
        button28.grid(row=13,column=1)
        button0 = tk.Button(self, text="Quitter",
                            command=lambda: self.controller.quit())
        button0.grid(row=14,column=1)
    def Guadeloupe(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/guadeloupe-latest.osm.pbf -p osm -E 5490 --slim")
    def Martinique(self):
        self.controller.occsol=1
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/martinique-latest.osm.pbf -p osm -E 5490 --slim")
        os.system("""ogr2ogr -nlt POLYGON -overwrite -t_srs "EPSG:5490" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/OCS_GE_1-1_2017_SHP_RGAF09UTM20_D972_2019-07-24/OCS_GE/1_DONNEES_LIVRAISON_2019-07-00355/OCSGE_1-1_SHP_RGAF09UTM20_D972_2017/OCCUPATION_SOL.shp""")
    def Guyane(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/guyane-latest.osm.pbf -p osm -E 2972 --slim")
    def Lareunion(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/reunion-latest.osm.pbf -p osm -E 2975 --slim")
    def Mayotte(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/mayotte-latest.osm.pbf -p osm -E 4471 --slim")
    def Alsace(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/alsace-latest.osm.pbf -p osm -E 27572 --slim")
    def Aquitaine(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/aquitaine-latest.osm.pbf -p osm -E 27572 --slim")
    def Auvergne(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/auvergne-latest.osm.pbf -p osm -E 27572 --slim")
    def Bassenormandie(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/basse-normandie-latest.osm.pbf -p osm -E 27572 --slim")
    def Bourgogne(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/bourgogne-latest.osm.pbf -p osm -E 27572 --slim")
    def Bretagne(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/bretagne-latest.osm.pbf -p osm -E 27572 --slim")
    def Centre(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/centre-latest.osm.pbf -p osm -E 27572 --slim")
    def Champagneardenne(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/champagne-ardenne-latest.osm.pbf -p osm -E 27572 --slim")
    def Corse(self):
        self.controller.occsol=1
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/corse-latest.osm.pbf -p osm -E 27572 --slim")
        os.system("""ogr2ogr -nlt POLYGON -overwrite -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Corse/OCS_GE_1-1_2016_SHP_LAMB93_D02B_2019-04-16/OCS_GE/1_DONNEES_LIVRAISON_2019-04-00288/OCSGE_1-1_SHP_LAMB93_D2B_2016/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Corse/OCS_GE_1-1_2016_SHP_LAMB93_D02A_2019-04-16/OCS_GE/1_DONNEES_LIVRAISON_2019-04-00288/OCSGE_1-1_SHP_LAMB93_D2A_2016/OCCUPATION_SOL.shp""")
    def Franchecomte(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/franche-comte-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Hautenormandie(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/haute-normandie-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Iledefrance(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/ile-de-france-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Languedocroussillon(self):
        self.controller.occsol=1
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/languedoc-roussillon-latest.osm.pbf -p osm -E 27572 --slim")
        os.system("""ogr2ogr -nlt POLYGON -overwrite -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/LR/OCS_GE_1-1_2015_SHP_LAMB93_D030_2019-09-06/OCS_GE/1_DONNEES_LIVRAISON_2019-09-00096/OCSGE_1-1_SHP_LAMB93_D30_2015/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/LR/OCS_GE_1-1_2015_SHP_LAMB93_D034_2019-09-06/OCS_GE/1_DONNEES_LIVRAISON_2019-09-00096/OCSGE_1-1_SHP_LAMB93_D34_2015/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/LR/OCS_GE_1-1_2015_SHP_LAMB93_D048_2019-09-06/OCS_GE/1_DONNEES_LIVRAISON_2019-09-00096/OCSGE_1-1_SHP_LAMB93_D48_2015/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/LR/OCS_GE_1-1_2015_SHP_LAMB93_D066_2019-09-06/OCS_GE/1_DONNEES_LIVRAISON_2019-09-00096/OCSGE_1-1_SHP_LAMB93_D66_2015/OCCUPATION_SOL.shp""")
    def Limousin(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/limousin-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Lorraine(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/lorraine-latest.osm.pbf -p osm -E 27572 --slim")                        
    def Midipyrenees(self):
        self.controller.occsol=1
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/midi-pyrenees-latest.osm.pbf -p osm -E 27572 --slim")
        os.system("""ogr2ogr -nlt POLYGON -overwrite -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D082_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D82-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D009_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D09-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D012_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D12-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D031_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D31-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D032_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D32-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D046_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D46-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D065_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D65-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2013_SHP_LAMB93_D081_2018-03-12/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00296/OCSGE_1-1_SHP_LAMB93_D81-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Mipy/OCS_GE_1-1_2015_SHP_LAMB93_D011_2019-09-06/OCS_GE/1_DONNEES_LIVRAISON_2019-09-00096/OCSGE_1-1_SHP_LAMB93_D11_2015/OCCUPATION_SOL.shp""")
    def Nordpasdecalais(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/nord-pas-de-calais-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Paysdelaloire(self):
        self.controller.occsol=1
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/pays-de-la-loire-latest.osm.pbf -p osm -E 27572 --slim")
        os.system("""ogr2ogr -nlt POLYGON -overwrite -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Pays/OCS_GE_1-1_2013_SHP_LAMB93_D044_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D44-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Pays/OCS_GE_1-1_2013_SHP_LAMB93_D049_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D49-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Pays/OCS_GE_1-1_2013_SHP_LAMB93_D053_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D53-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Pays/OCS_GE_1-1_2013_SHP_LAMB93_D072_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D72-2013/OCCUPATION_SOL.shp""")
        os.system("""ogr2ogr -nlt POLYGON -append -t_srs "EPSG:27572" -f PostgreSQL PG:"dbname=gis host=localhost user=cnnr password=cnnr" ~/KIT/Pays/OCS_GE_1-1_2013_SHP_LAMB93_D085_2018-03-13/OCS_GE/1_DONNEES_LIVRAISON_2018-03-00297/OCSGE_1-1_SHP_LAMB93_D85-2013/OCCUPATION_SOL.shp""")                             
    def Picardie(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/picardie-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Poitoucharentes(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/poitou-charentes-latest.osm.pbf -p osm -E 27572 --slim")                  
    def PACA(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/provence-alpes-cote-d-azur-latest.osm.pbf -p osm -E 27572 --slim")                  
    def Rhonealpes(self):
        self.controller.show_frame(Page2)
        os.system("osm2pgsql -d gis -C 53 --create --drop ~/KIT/rhone-alpes-latest.osm.pbf -p osm -E 27572 --slim")

class Page2(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="extractiondata", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        self.controller=controller
        label2 = tk.Label(self, text="ATTENTION les calculs peuvent prendre plusieurs dizaines de jours pour un espace d'un département selon votre materiel. En cliquant sur le bouton ci-dessous vous préparez les données pour les calculs", font=LARGE_FONT)
        label2.pack(pady=10,padx=10)
        label3 = tk.Label(self, text="Choisissez le score des services de mobilité, les stations enr, ou la geothermie individuelle,ou la calibration de services", font=LARGE_FONT)
        label2.pack(pady=10,padx=10)
        button1 = tk.Button(self, text="extraire_donnees pour calcul score services",
                            command=lambda: self.Scoring())
        button1.pack()
        button2 = tk.Button(self, text="extraire_donnees pour calcul calibration oeuvres d'arts seulement",
                            command=lambda: self.Calibration())
        button2.pack()
        if self.controller.occsol==1:          
            button3 = tk.Button(self, text="extraire_donnees pour calcul geothermie individuelle",command=lambda: self.Geot())
            button3.pack()
            button4 = tk.Button(self, text="extraire_donnees pour calcul stations enr",command=lambda: self.Station())
            button4.pack()                        
        button0 = tk.Button(self, text="Quitter",
                            command=lambda: self.controller.quit())
        button0.pack()
    def Scoring(self):
        os.system("""psql -A --command "SELECT osm_id, name, building as type, st_astext(way) as geometry FROM osm_polygon WHERE building IS NOT NULL" --dbname gis --username cnnr --output ~/building --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('arts_center') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('arts_center') UNION ALL SELECT osm_id, name, tourism  as type, st_astext(way) as geometry FROM osm_point WHERE tourism in ('artwork') UNION ALL SELECT osm_id, name, tourism  as type, st_astext(way) as geometry FROM osm_polygon WHERE tourism in ('artwork')" --dbname gis --username cnnr --output ~/art.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('hospital','clinic') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('hospital','clinic')" --dbname gis --username cnnr --output ~/soins.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('parking','parking_space') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('parking','parking_space')" --dbname gis --username cnnr --output ~/carparking.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('fuel','charging_station') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('fuel','charging_station') UNION ALL SELECT osm_id, name, shop  as type,st_astext(way) as geometry FROM osm_point WHERE shop in ('fuel') UNION ALL SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_polygon WHERE shop in ('fuel')" --dbname gis --username cnnr --output ~/carfuel.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('car_rental') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('car_rental')" --dbname gis --username cnnr --output ~/carrent.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('car_sharing') UNION ALL SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('car_sharing')" --dbname gis --username cnnr --output ~/carshare.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_point WHERE shop in ('car_parts','car_repair','car') UNION ALL SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_polygon WHERE shop in ('car_parts','car_repair','car')" --dbname gis --username cnnr --output ~/carshop.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, highway as type,CASE WHEN tunnel='yes' THEN 1 ELSE 0 END tunnel, CASE WHEN bridge='yes' THEN 1 ELSE 0 END bridge,CASE WHEN oneway='yes' THEN 1 ELSE 0 END oneway, CASE WHEN bicycle='designated' THEN 1 ELSE 0 END piste, CASE WHEN bicycle='yes' THEN 1 ELSE 0 END bande, CASE WHEN bicycle in ('1cykel_fortovet','2cykel_fortovet','3cykel_fortovet','4cykel_fortovet') THEN 1 ELSE 0 END cykel_fortovet,z_order,st_astext(way) as geometry FROM osm_line WHERE highway IN ('primary','primary_link','secondary','secondary_link','tertiary','road','path','track','service','footway','bridleway','steps','living_street','unclassified','residential')" --dbname gis --username cnnr --output ~/mainminorroads.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('bicycle_parking') UNION ALL SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('bicycle_parking')" --dbname gis --username cnnr --output ~/bikeparking.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('bicycle_repair_station') UNION ALL SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('bicycle_repair_station') UNION ALL SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_point WHERE shop in ('bicycle') UNION ALL SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_polygon WHERE shop in ('bicycle')" --dbname gis --username cnnr --output ~/bikeshop.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('bicycle_rental') UNION ALL SELECT osm_id, name, amenity as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('bicycle_rental')" --dbname gis --username cnnr --output ~/bikerent.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, tourism as type, st_astext(way) as geometry FROM osm_point WHERE tourism in ('information') UNION ALL SELECT osm_id, name, tourism as type, st_astext(way) as geometry FROM osm_polygon WHERE tourism in ('information')" --dbname gis --username cnnr --output ~/bikeinfo.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, type, tunnel, bridge, oneway, piste, bande,cykel_fortovet,geometry from(SELECT osm_id, name, highway as type, CASE WHEN tunnel='yes' THEN 1 ELSE 0 END tunnel, CASE WHEN bridge='yes' THEN 1 ELSE 0 END bridge, CASE WHEN oneway='yes' THEN 1 ELSE 0 END oneway, CASE WHEN bicycle='designated' THEN 1 ELSE 0 END piste,CASE WHEN bicycle='yes' THEN 1 ELSE 0 END bande,CASE WHEN bicycle in ('1cykel_fortovet','2cykel_fortovet','3cykel_fortovet','4cykel_fortovet') THEN 1 ELSE 0 END cykel_fortovet,z_order,st_astext(way) as geometry FROM osm_line WHERE highway IN ('cycleway','primary','primary_link','secondary','secondary_link','tertiary','road','path','track','service','footway','bridleway','steps','living_street','unclassified','residential')) AS TEMP01 WHERE type in ('cycleway') UNION ALL SELECT osm_id, name, type,tunnel,bridge,oneway,piste,bande,cykel_fortovet,geometry from (SELECT osm_id,name,highway as type,CASE WHEN tunnel='yes' THEN 1 ELSE 0 END tunnel,CASE WHEN bridge='yes' THEN 1 ELSE 0 END bridge, CASE WHEN oneway='yes' THEN 1 ELSE 0 END oneway, CASE WHEN bicycle='designated' THEN 1 ELSE 0 END piste, CASE WHEN bicycle='yes' THEN 1 ELSE 0 END bande, CASE WHEN bicycle in ('1cykel_fortovet','2cykel_fortovet','3cykel_fortovet','4cykel_fortovet') THEN 1 ELSE 0 END cykel_fortovet,z_order,st_astext(way) as geometry FROM osm_line WHERE highway IN ('cycleway','primary','primary_link','secondary','secondary_link','tertiary','road','path','track','service','footway','bridleway','steps','living_street','unclassified','residential')) AS TEMP02 where piste in (1) OR bande in (1) OR cykel_fortovet in (1)" --dbname gis --username cnnr --output ~/mainminorcycleroads.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, railway as type, st_astext(way) as geometry FROM osm_point WHERE railway in ('tram_stop','subway_entrance','station','halt') UNION ALL SELECT osm_id, name, highway as type, st_astext(way) as geometry FROM osm_point WHERE highway in ('bus_stop') UNION ALL SELECT osm_id, name, amenity as type,st_astext(way) as geometry FROM osm_point WHERE amenity in ('bus_station') UNION ALL SELECT osm_id, name, public_transport as type, st_astext(way) as geometry FROM osm_point WHERE public_transport in ('station','stop_area','stop_position','platform')" --dbname gis --username cnnr --output ~/tc_stop.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, tourism as type, st_astext(way) as geometry FROM osm_point WHERE tourism in ('information') UNION ALL SELECT osm_id, name, tourism as type, st_astext(way) as geometry FROM osm_polygon WHERE tourism in ('information')" --dbname gis --username cnnr --output ~/tcinfo.csv --field-separator '#'""")
        self.controller.show_frame(PageRanking)
        
    def Calibration(self):
        os.system("""psql -A --command "SELECT osm_id, name, building as type, st_astext(way) as geometry FROM osm_polygon WHERE building IS NOT NULL" --dbname gis --username cnnr --output ~/building --field-separator '#'""")
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('arts_center') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('arts_center') UNION ALL SELECT osm_id, name, tourism  as type, st_astext(way) as geometry FROM osm_point WHERE tourism in ('artwork') UNION ALL SELECT osm_id, name, tourism  as type, st_astext(way) as geometry FROM osm_polygon WHERE tourism in ('artwork')" --dbname gis --username cnnr --output ~/art.csv --field-separator '#'""")
        self.controller.show_frame(PageCali)

    def Geot(self):
        os.system("""psql -A --command "SELECT osm_id, name, building as type, st_astext(way) as geometry FROM osm_polygon WHERE building IS NOT NULL" --dbname gis --username cnnr --output ~/building --field-separator '#'""")
        os.system("""psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, st_astext(wkb_geometry) as geometry FROM occupation_sol WHERE code_cs in ('CS2.2.1')" --dbname gis --username cnnr --output ~/occsol --field-separator '#'""")
        self.controller.show_frame(PageOccsol)

    def Station(self):
        os.system("""psql -A --command "SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_point WHERE amenity in ('fuel','charging_station') UNION ALL SELECT osm_id, name, amenity  as type, st_astext(way) as geometry FROM osm_polygon WHERE amenity in ('fuel','charging_station') UNION ALL SELECT osm_id, name, shop  as type,st_astext(way) as geometry FROM osm_point WHERE shop in ('fuel') UNION ALL SELECT osm_id, name, shop as type, st_astext(way) as geometry FROM osm_polygon WHERE shop in ('fuel')" --dbname gis --username cnnr --output ~/carfuel.csv --field-separator '#'""")
        os.system("""psql -A --command "SELECT ogc_fid,id,code_cs,millesime,source, st_astext(wkb_geometry) as geometry FROM occupation_sol WHERE code_cs in ('CS2.2.1')" --dbname gis --username cnnr --output ~/occsol --field-separator '#'""")
        self.controller.show_frame(PageStations)

class PageRanking(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="calculer", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        self.controller=controller
        ###v1 verif ranking deja fait?
        ###texte d'explications calculs distances, page suivante cali
        button = tk.Button(self, text="lancer calcul de score",
                            command=lambda: self.Rank())
        button.pack()

        button2 = tk.Button(self, text="Retour",
                            command=lambda: self.controller.show_frame(Page2))
        button2.pack()
    def Rank(self):
        plan.dist_building_item('building','geometry','art.csv')
        plan.dist_building_item('building','geometry','soins.csv')
        plan.dist_building_item('building','geometry','carparking.csv')
        plan.dist_building_item('building','geometry','carfuel.csv')
        plan.dist_building_item('building','geometry','carrent.csv')
        plan.dist_building_item('building','geometry','carshare.csv')
        plan.dist_building_item('building','geometry','carshop.csv')
        plan.dist_building_infra('building','geometry','mainminorroads.csv')
        plan.dist_building_item('building','geometry','bikeparking.csv')
        plan.dist_building_item('building','geometry','bikeshop.csv')
        plan.dist_building_item('building','geometry','bikerent.csv')
        plan.dist_building_item('building','geometry','bikeinfo.csv')
        plan.dist_building_infra('building','geometry','mainminorcycleroads.csv')
        plan.dist_building_item('building','geometry','tc_stop.csv')
        plan.dist_building_item('building','geometry','tcinfo.csv')
        plan.km1pt5_nb_building_tc('building','geometry','tc_stop.csv')
        plan.rank('building')

class PageCali(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="calculer", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        self.controller=controller
        ###texte d'explications calculs cali
        ###radio button 1 "service infrastructurel" choisi
        ###mise en page resultats
        button = tk.Button(self, text="Lancer calcul calibration oeuvre d'art à construire",
                            command=lambda: self.Cali())
        button.pack()

        button2 = tk.Button(self, text="Retour",
                            command=lambda: self.controller.show_frame(Page2))
        button2.pack()
    def Cali(self):
        plan.dist_building_item('building','geometry','art.csv')
        plan.cali('building',[bbox],4,5000)
                

class PageOccsol(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="calculer", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        self.controller=controller
        ###texte d'explications calculs geot
        ###mise en page resultats
        button = tk.Button(self, text="calculer le potentiel de geothermie individuelle",
                            command=lambda: self.Geot())
        button.pack()

        button2 = tk.Button(self, text="Retour",
                            command=lambda: self.controller.show_frame(Page2))
        button2.pack()
    def Geot(self):
        plan.geot('building','geometry','occsol')

class PageStations(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        label = tk.Label(self, text="calculer", font=LARGE_FONT)
        label.pack(pady=10,padx=10)
        self.controller=controller
        ###texte d'explications calculs p2g
        ###mise en page resultats
        button = tk.Button(self, text="faire les calculs pr les stations enr",
                            command=lambda: self.Stations())
        button.pack()

        button2 = tk.Button(self, text="Retour",
                            command=lambda: self.controller.show_frame(Page2))
        button2.pack()
    def Stations(self):
        plan.p2g('carfuel.csv','geometry','occsol')

plan=Planning()
A=Appli().mainloop()
 
