Django==4.2.4
django-cors-headers==4.2.0
django-test-migrations==1.3.0
django-migration-linter==5.0.0
djangorestframework==3.14.0
gunicorn==21.2.0
psycopg2-binary==2.9.7
pytest==7.4.0
pytest-django==4.5.2
pytest-dotenv==0.5.2
python-dotenv==1.0.0
whitenoise==6.5.0
numpy==1.26.4
pandas==2.0.0

