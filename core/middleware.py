import logging
from datetime import datetime
from core import settings
from datetime import datetime
from map.models import Metrics

class IPLoggingMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Default to English if no cookie is set
        x_forwarded_for = request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip = x_forwarded_for.split(",")[0]
        else:
            ip = request.META.get("REMOTE_ADDR")
        logging.getLogger("django").info(
            f'[{datetime.now().strftime("%d/%b/%Y %H:%M:%S")}] - IP: ' + ip
        )
        response = self.get_response(request)
        return response


class CustomXFrameMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # Define the list of allowed domains
        allowed_domains = getattr(settings, 'ALLOWED_XFRAME_DOMAINS', [])

        # Check if the request domain is in the list of allowed domains
        if 'HTTP_REFERER' in request.META:
            referrer = request.META['HTTP_REFERER']
            for domain in allowed_domains:
                if domain in referrer:
                    response['X-Frame-Options'] = f'ALLOW-FROM {referrer}'
                    break

        return response

class MetricsMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self,request):
        #increment view counts
        self.metrics(request)
        response=self.get_response(request)
        return response

    def metrics(self,request):
        x_forwarded_for=request.META.get("HTTP_X_FORWARDED_FOR")
        if x_forwarded_for:
            ip=x_forwarded_for.split(",")[0]
        else:
            ip=request.META.get("REMOTE_ADDR")
        today=datetime.now().date()
        Metrics.objects.create(ip=ip,date=today,path=request.path)
