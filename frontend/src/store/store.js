import { create } from 'zustand'

export const useStore = create((set) => ({
  user: {},
  setUsers: (data) => set((state) => ({ user: data })),
}))

