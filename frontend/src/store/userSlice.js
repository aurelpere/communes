import { createSlice } from "@reduxjs/toolkit";

export const userSlice = createSlice({
  name: "user",
  initialState: {},
  reducers: {
    setUser: (state, action) => {
      return (state = { ...action.payload });
    },
    setUserEmail: (state, action) => {
      state.email = action.payload;
    },
    setUserUsername: (state, action) => {
      state.username = action.payload;
    },
    setAuthToken: (state, action) => {
      state.authToken = action.payload;
    },
    removeAuthToken: (state, action) => {
      state.authToken = null;
    },
  },
});

export const {
  setUser,
  setUserEmail,
  setUserUsername,
  setAuthToken,
  removeAuthToken,
} = userSlice.actions;

export default userSlice.reducer;
