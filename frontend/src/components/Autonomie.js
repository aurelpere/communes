import React from "react";
import {Link} from "react-router-dom";

const Autonomie = () => {



    const content = <div><h2>Photovoltaïque autonome:</h2><br/>
Tutos lowtechlab:<br/>
<Link to="https://wiki.lowtechlab.org/wiki/Dimensionner_une_installation_photovolta%C3%AFque_autonome">1.Dimensionner une installation photovoltaïque autonome</Link><br/>
<Link to="https://wiki.lowtechlab.org/wiki/Mesure_de_l%27ensoleillement-luminosit%C3%A9_avec_un_ordinateur_monocarte_(raspberry-orangepi)">2.Mesure de l'ensoleillement-luminosité avec un ordinateur monocarte (raspberry-orangepi)</Link><br/>
<Link to="https://wiki.lowtechlab.org/wiki/Serveur_orangepi-raspberry_nextcloud_en_photovolta%C3%AFque_autonome">3.Serveur orangepi-raspberry nextcloud en photovoltaïque autonome</Link><br/>
<br/>
<Link to="/sun">Utilitaire de dimensionnement</Link><br/>
<br/><br/>
<h2>Gestion de l'eau autonome:</h2><br/>
Tutos lowtechlab:<br/>
<Link to="https://wiki.lowtechlab.org/wiki/Dimensionner_la_r%C3%A9cup%C3%A9ration_d%27eau_de_pluie_pour_%C3%AAtre_autonome">
Dimensionner la récupération d'eau de pluie pour être autonome</Link>
<br/><br/>
<Link to="/water">Utilitaire de dimensionnement</Link><br/>

<br/>


        <br/>
<br/><br/>
<h2>Construction autonome:</h2><br/>
      <Link to="https://wiki.lowtechlab.org/wiki/Construire_une_cabane_en_mode_autonome">
      Connaissances sur les proécédés de constructions avec un minimu de motorisation (cabanes)</Link>


      <br/><br/> </div>
    return (
        <>{content}</>
    )
};
export default Autonomie;
