import {useEffect, useRef, useState} from "react";
import React from 'react';
import MapWrapper from "./MapWrapper";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import {Alert, Input, Spinner} from "reactstrap";
import {getTokenDetail} from "../functions/getTokenDetail";
import postmap from "../functions/postmap";
import Content from "../common/Content";
import postid from "../functions/postid";

const Feature = (props) => {


    return (
        <>
            date mutation: {props.datemut}<br/>
            annee mutation: {props.anneemut}<br/>
            surface batie(m2): {props.sbati}<br/>
            surface de terrain(m2): {props.sterr}<br/>
            valeur fonciere(€): {props.valeurfonc}<br/>
            valeur fonciere/surface bati (€/m2): {props.sbatieuro_par_m2}<br/>
            valeur fonciere/surface terrain(€/ha): {props.sterr_euro_par_ha}<br/>
            valeur fonciere/surface terrain(€/m2): {props.sterr_euro_par_m2}<br/>
            vefa:{props.vefa}<br/><br/>
            nblocmut: {props.nblocmut}<br/>
            nbvolmut: {props.nbvolmut}<br/>
            libnatmut: {props.libnatmut}<br/>
            codtypbien: {props.codtypbien}<br/>
            idmutinvar: {props.idmutinvar}<br/>
            idopendata: {props.idopendata}<br/>
            lcodinsee: {props.lcodinsee}<br/>
            lidlocmut: {props.lidlocmut}<br/>
            lidparmut: {props.lidparmut}<br/>
            libtypbien: {props.libtypbien}<br/>
            coddep: {props.coddep}<br/>
            nbcomm: {props.nbcomm}<br/>
            lidpar: {props.lidpar}<br/>
            nbpar: {props.nbpar}<br/>
            id: {props.id}<br/>
            lat: {props.lat}<br/>
            lon: {props.lon}<br/>
        </>
    );
};
export default Feature;
