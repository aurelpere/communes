import React from "react";
import Content from "../common/Content"
import MapWrapper from "./MapWrapper";
import MapComponent from "./MapComponent";
import ErrorComponent from "./ErrorComponent";

import {Spinner} from "reactstrap";

const Accueil = () => {


    // const render = (status) => {
    //     if (status === Status.FAILURE) return <ErrorComponent/>;
    //     else if (status === Status.LOADING) return <Spinner/>;
    //     else if (status === Status.SUCCESS) return <MapComponent mapOptions={mapOptions}/>;
    // };

    const content = <div style={{width: "800px", height: "800px"}}>
        <MapComponent/>

    </div>
    return (
        <>{content}</>
    )
};
export default Accueil;

