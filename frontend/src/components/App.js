import {createBrowserRouter, Route, Routes, Switch, redirect, RouterProvider, BrowserRouter} from "react-router-dom";
import Accueil from "./Accueil";
import Communes from "./Communes";
import Works from "./Works";
import Water from "./Water";
import Sun from "./Sun";
import Autonomie from "./Autonomie";
function App() {

    //useEffect(() => {
    //    console.log(user); // Log the updated user state whenever it changes
    //}, [user]);
    const routes =
        [

            {
                path: "/alive",
                element: <Accueil/>
            },
            {
                path: "/works",
                element: <Works/>
            },
            {
                path: "/autonomie",
                element: <Autonomie/>
            },
            {
                path: "/communes",
                element: <Communes/>
            },
            {
                path: "/water",
                element: <Water/>
            },
            {
                path: "/sun",
                element: <Sun/>
            },
        ];
    const router = createBrowserRouter(routes);

    return (
        <BrowserRouter>
            {/* <TopNav/>Render the TopNav component */}
            <div className="content-container">
                <Routes>
                    {/* Render the route components */}
                    {routes.map((route, index) => (
                        <Route
                            key={index}
                            path={route.path}
                            element={route.element}
                        />
                    ))}
                </Routes>
            </div>
            {/* <Footer/>  Render the Footer component */}
        </BrowserRouter>
    );

}

export default App;
