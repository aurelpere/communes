import {useEffect, useRef, useState} from "react";
import React from 'react';
import {Alert, Button, Form, FormFeedback, FormGroup, FormText, Input, Spinner} from "reactstrap";
import sunpost from "../functions/sunpost";
import {Link} from "react-router-dom";
import axios from "axios";

const Sun = () => {
    const ref = useRef(null);
    const [lat, setLat] = useState(null);
    const [lon, setLon] = useState(null);
    const [consojour, setConsojour] = useState(null);
    const [njnoelec, setNjnoelec] = useState(0);
    const [typebatterie, setTypebatterie] = useState("lithium");
    const [anneedebut, setAnneedebut] = useState(2005);
    const [anneefin, setAnneefin] = useState(2020);
    const [angle,setAngle]=useState(45);
    const [aspect,setAspect]=useState(0);
    const [d,setD]=useState(0);
    const [noticelon, setNoticelon] = useState(false);
    const [noticelat, setNoticelat] = useState(false);
    const [noticeconsojour, setNoticeconsojour] = useState(false);
    const [noticenjnoelec, setNoticenjnoelec] = useState(false);

    const [noticetypebatterie, setNoticetypebatterie] = useState(false);
    const [noticeanneedebut, setNoticeanneedebut] = useState(false);
    const [noticeanneefin, setNoticeanneefin] = useState(false);

    const [data, setData] = useState([[],[],[],[],[]]);

    const [loadingdata, setLoadingdata] = useState(false);
    const [dataerror, setDataerror] = useState(false);
    const [dataok, setDataok] = useState(false);
    const [ip,setIp]=useState("999.999.999.999");

 useEffect(() => {
  axios.get('https://api.ipify.org')
    .then(response => {
      setIp(response.data);
      console.log(response.data); // Use response.data, not ipAddressResponse.data
      // Use ipAddress for further processing
    })
    .catch(error => {
      console.log("there was an error with retrieving your ip adress through ipify. A default ip adress will be used and should not prevent the website to work properly")
      console.log(error);
      setIp("999.999.999.999");
    });
}, []);
 useEffect(() => {
  console.log(data);
}, [data]);


    const post = () => {
        setDataerror(false)
        setLoadingdata(true)
        setDataok(false)
        sunpost(d,angle,aspect,ip,lat,lon,consojour,typebatterie,njnoelec,anneedebut,anneefin).then((res) => {
            setData(res)
            setLoadingdata(false)
            setDataok(true)
            //console.log(`data:${res}`)
        }).catch((err) => {
            setLoadingdata(false)
            setDataerror(true)
        })
    };
    const contentalert = <div className="flex align-items-center justify-content-center">
        <Alert color="danger">
            <h4 className="alert-heading">
                Un probleme est survenue lors de la récupération des résultats.<br/>
                Ressayez en recliquant sur le bouton et en vérifiant les données d'entrée<br/>
            </h4>
        </Alert>
    </div>
    const contentok = <div className="flex align-items-center justify-content-center">
        <Alert>
            <h4 className="alert-heading">
                Les résultats ont été téléchargées correctement<br/>
            </h4>
        </Alert>
    </div>

    const handleLat = (e) => {
        setNoticelat(false)
        if (typeof e.target.value != "string") return setLat(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticelat("Entrer un nombre")
        }
        setLat(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleLon = (e) => {
        setNoticelon(false)
        if (typeof e.target.value != "string") return setLon(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticelon("Entrer un nombre")
        }
        setLon(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleConsojour = (e) => {
        setNoticeconsojour(false)
        if (typeof e.target.value != "string") return setConsojour(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticeconsojour()("Entrer un nombre")
        }
        setConsojour(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleAngle = (e) => {
        if (typeof e.target.value != "string") return setAngle(0)
        let value = e.target.value;
        setAngle(value)
    }
    const handleD = (e) => {
        if (typeof e.target.value != "string") return setD(0)
        let value = e.target.value;
        setD(value)
    }

    const handleNjnoelec = (e) => {
        setNoticenjnoelec(false)
        if (typeof e.target.value != "string") return setNjnoelec(0)
        let value = e.target.value;
        setNjnoelec(value)
    }
    const handleTypebatterie = (e) => {
        setNoticetypebatterie(false)
        if (typeof e.target.value != "string") return setTypebatterie("lithium")
        let value = e.target.value;
        setTypebatterie(value)
    }
    const handleAspect = (e) => {
        if (typeof e.target.value != "string") return setAspect("sud")
        let value = e.target.value;
        setAspect(value)
    }
    const handleAnneedebut = (e) => {
        setNoticeanneedebut(false)
        if (typeof e.target.value != "string") return setAnneedebut(2005)
        let value = e.target.value;
        setAnneedebut(value)
    }
    const handleAnneefin = (e) => {
        setNoticeanneefin(false)
        if (typeof e.target.value != "string") return setAnneefin(2020)
        let value = e.target.value;
        setAnneefin(value)
    }


    return (
        <>
            <div className="container-fluid">
                <br/>
                <div className="row h-100 d-flex flex-row">
                    <div className="col-8">
                        <Form>
                            <div className="row">
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticelat)}
                                            id="lat"
                                            name="lat"
                                            placeholder="Latitude"
                                            onChange={(e) => handleLat(e)}
                                        />
                                        <FormText>Latitude WGS 84. Exemple: 44.2 pour Agen, Lot-et-Garone</FormText>
                                        <FormFeedback>{noticelat}</FormFeedback>
                                    </FormGroup>
                                </div>
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticelon)}
                                            id="lon"
                                            name="lon"
                                            placeholder="Longitude"
                                            onChange={(e) => handleLon(e)}
                                        />
                                        <FormText>Longitude WGS 84. Exemple: 0.6 pour Agen, Lot-et-Garone</FormText>
                                        <FormFeedback>{noticelon}</FormFeedback>
                                    </FormGroup>
                                </div>

                                <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="consojour"
                                            name="consojour"
                                            placeholder="Consommation journalière constante(kWh)"
                                            onChange={(e) => handleConsojour(e)}
                                        />
                                        <FormText>Consommation journaliere constante (kWh)</FormText>
                                        <FormFeedback>{noticeconsojour}</FormFeedback>
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="njnoelec"
                                            name="njnoelec"
                                            value={njnoelec}
                                            placeholder="Durée des épisodes sans élec admissibles (j)"
                                            onChange={(e) => handleNjnoelec(e)}
                                        />
                                        <FormText>Durée des épisodes sans élec admissibles (j) - 0j par défaut - </FormText>
                                        <FormFeedback>{noticenjnoelec}</FormFeedback>
                                    </FormGroup>
                                </div>
 <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="angle"
                                            name="angle"
                                            value={angle}
                                            onChange={(e) => handleAngle(e)}
                                       />
                                            <FormText>Angle des modules</FormText>
                                    </FormGroup>
                                </div>
 <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="d"
                                            name="d"
                                            value={d}
                                            onChange={(e) => handleD(e)}
                                        />
                                            <FormText>% de decharge maximum (20% pour ne pas descendre en dessous de 20% de charge)</FormText>
                                    </FormGroup>
                                </div>
 <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="aspect"
                                            name="aspect"
                                            type="select"
                                            onChange={(e) => handleAspect(e)}
                                        >
                                            <option>
                                                sud
                                            </option>
                                            <option>
                                                nord
                                            </option>
                                            <option>
                                                ouest
                                            </option>
                                            <option>
                                                est
                                            </option>
                                            </Input>
                                            <FormText>Orientation des modules</FormText>
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="typebatterie"
                                            name="typebatterie"
                                            type="select"
                                            onChange={(e) => handleTypebatterie(e)}
                                        >
                                            <option>
                                                lithium
                                            </option>
                                            <option>
                                                plomb
                                            </option>
                                            </Input>
                                            <FormText>Type de batterie (plomb ou lithium) - lithium par
                                                défaut</FormText>
                                    </FormGroup>
                                </div>

                                <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="anneedebut"
                                            name="anneedebut"
                                            value={anneedebut}
                                            placeholder="Année de début"
                                            onChange={(e) => handleAnneedebut(e)}
                                        />
                                        <FormText>Première année de la periode à prendre en compte pour les données
                                            (2005 par défaut, données disponibles jusqu'en 2005)</FormText>
                                    </FormGroup>
                                </div>
                                <div className="row">
                                    <FormGroup>
                                        <Input
                                            id="anneefin"
                                            name="anneefin"
                                            value={anneefin}
                                            placeholder="Année de fin"
                                            onChange={(e) => handleAnneefin(e)}
                                        />
                                        <FormText>Derniere année de la periode à prendre en compte pour les données
                                            (2020 par défaut, données disponibles jusqu'en 2020)</FormText>
                                    </FormGroup>
                                </div>
                            </div>
                        </Form>

                        <div className="row">

                            <div className="col-4">
                                <Button onClick={post} color="primary">Effectuer le calcul de dimensionnement</Button>
                            </div>
                            <div className="col-4">
                            </div>

                            <div className="col-4">
                                {(loadingdata) ? (<Spinner/>) : (<div></div>)}
                            </div>


                        </div>

                    </div>
                    <div className="col-4">
                        <div>
                            {(dataerror) ? (<div key="alert">{contentalert}</div>) : (
                                <div></div>)}
                            {(dataok) ? (<div key="ok">{contentok}</div>) : (
                                <div></div>)}
                        </div>
                        <div>
                        {(loadingdata) ? (<Spinner/>) : ((dataok) ? (
                            <>
                                <b>Résultats optimisés</b>
                                <br/>
                                <b>Sans coupures:</b>
                                <div>
                                    {data[3].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>
                                <b>Avec coupures selon nb de jours admissibles:</b>
                                <div>
                                    {data[4].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>
                                <br/>

                                <b>Stats production 1kWc avec données entrées</b>
                                <div>
                                    {data[0].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>
                                <b>Détail dimensionnement Itérations sans coupures</b>

                                <div>
                                    {data[1].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>
                                <b>Détail dimensionnement Itérations avec coupures</b>
                                <div>
                                    {data[2].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>


                            </>
                        ) : (<div></div>)
                        )
                        }


                        </div>

                        <br/>
                    </div>
                </div>
            </div>

        </>
    );
};
export default Sun;
