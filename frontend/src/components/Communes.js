import React from "react";
import {Link} from "react-router-dom";

const Communes = () => {


    
    const content = <div>
1.water rain synop+water quality sise-eaux<br/>
2.pesticides<br/>
2b.<a href="https://www.telepac.agriculture.gouv.fr/telepac/tbp/feader/afficher.action">annuaire pac</a><br/>
3.geothermie<br/>
4.foncier: <a href="https://www.gpso.xyz">cartographie des transactions foncieres et immobilieres sur les tracés lgv gpso et a69</a>(septembre-octobre 2023)<br/><br/>
<a href="https://www.lesprix.immo">cartographie des transactions foncieres et immobilieres ailleurs</a>(nouvelle aquitaine, midi pyrénées, rhone alpes en avril 2024)<br/><br/>
5.annuaire élus<br/>
6.enr à la commune et cartographie de potentiel geothermique individuel<br/>
7.linéaire voiries obs territoires+parlons velo<br/>
8.budgets obs territoires<br/>
9.dpe <br/>
Si vous voulez participer, n'hesitez pas à <Link href="mailto:aurel.pere@matangi.dev">me contacter (aurel.pere@matangi.dev)</Link><br/>
Tout ca est fait à partir des données publiques de <Link to="http://data.cquest.org/">data.cquest.org</Link>
</div>
    return (
        <>{content}</>
    )
};
export default Communes;
