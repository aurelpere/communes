import {useEffect, useRef, useState} from "react";
import React from 'react';
import MapWrapper from "./MapWrapper";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import {Alert, Button, Form, FormFeedback, FormGroup, FormText, Input, Spinner} from "reactstrap";
import {getTokenDetail} from "../functions/getTokenDetail";
import postmap from "../functions/postmap";
import Content from "../common/Content";
import postid from "../functions/postid";
import postmapnontiba from "../functions/postmapnontiba";
import postmaptiba from "../functions/postmaptiba";
import {Link} from "react-router-dom";
import Feature from "./Feature"
import axios from "axios";

const MapComponent = () => {
    const ref = useRef(null);
    const [map, setMap] = useState(null);
    const [mapLayerGroup, setMapLayerGroup] = useState(null);
    const [mapLayerGroup0, setMapLayerGroup0] = useState(null);
    const [sup, setSup] = useState(0);
    const [inf, setInf] = useState(100000000000000000000);
    const [commune, setCommune] = useState("");

    const [zoom, setZoom] = useState([]);
    const [noticeinf, setNoticeinf] = useState(false);
    const [noticesup, setNoticesup] = useState(false);

    const [pos, setPos] = useState(true);
    const [poum, setPoum] = useState(true);

    const [boundingbox, setBoundingbox] = useState(false);
    const [data, setData] = useState(false);
    const [data_, setData_] = useState(false);
    const [data__, setData__] = useState(false);

    const [id, setId] = useState(null);
    const [lat, setLat] = useState(null);
    const [lon, setLon] = useState(null);

    const [datemut, setDatemut] = useState(null);
    const [anneemut, setAnneemut] = useState(null);
    const [sbati, setSbati] = useState(null);
    const [sterr, setSterr] = useState(null);
    const [valeurfonc, setValeurfonc] = useState(null);
    const [sbatieurom2, setSbatieurom2] = useState(null);
    const [sterreuroha, setSterreuroha] = useState(null);
    const [sterreurom2, setSterreurom2] = useState(null);

    const [nblocmut, setNblocmut] = useState(null);
    const [nbparmut, setNbparmut] = useState(null);
    const [nbvolmut, setNbvolmut] = useState(null);
    const [libnatmut, setLibnatmut] = useState(null);
    const [codtypbien, setCodtypbien] = useState(null);
    const [idmutinvar, setIdmutinvar] = useState(null);
    const [idopendata, setIdopendata] = useState(null);
    const [lcodinsee, setLcodinsee] = useState(null);
    const [lidlocmut, setLidlocmut] = useState(null);
    const [lidparmut, setLidparmut] = useState(null);
    const [libtypbien, setLibtypbien] = useState(null);
    const [coddep, setCoddep] = useState(null);
    const [nbcomm, setNbcomm] = useState(null);
    const [lidpar, setLidpar] = useState(null);
    const [vefa, setVefa] = useState(null);
    const [nbpar, setNbpar] = useState(null);
    const [loadingdata, setLoadingdata] = useState(false);
    const [dataerror, setDataerror] = useState(false);
    const [loadingdata_, setLoadingdata_] = useState(false);
    const [data_error, setData_error] = useState(false);
    const [loadingdata__, setLoadingdata__] = useState(false);
    const [data__error, setData__error] = useState(false);
    const [data__ok, setData__ok] = useState(false);
    const [data_ok, setData_ok] = useState(false);
    const [dataok, setDataok] = useState(false);
    const [loadingid, setLoadingid] = useState(false);
    const [ip,setIp]=useState("999.999.999.999");

 useEffect(() => {
  axios.get('https://api.ipify.org')
    .then(response => {
      setIp(response.data);
      console.log(response.data); // Use response.data, not ipAddressResponse.data
      // Use ipAddress for further processing
    })
    .catch(error => {
      console.log(error);
      setIp("999.999.999.999");
    });
}, []);

    const markers = [{
        'localisant': ' Bruch 47130',
        'lat': 44.206990149999996,
        'lon': 0.4051931
    }, {
        'localisant': ' Pompiey 47230',
        'lat': 44.199470399999996,
        'lon': 0.2042391
    }, {
        'localisant': ' Montgaillard-en-Albret 47230',
        'lat': 44.209751749999995,
        'lon': 0.29816675000000004
    }, {
        'localisant': ' Buzet-sur-Baïse 47160',
        'lat': 44.25137805,
        'lon': 0.30408250000000003
    }, {
        'localisant': ' Lavardac 47230',
        'lat': 44.18215685,
        'lon': 0.30061329999999997
    }, {'localisant': ' La Réunion 47700', 'lat': 44.2842275, 'lon': 0.1164675}, {
        'localisant': ' Barbaste 47230',
        'lat': 44.1623073,
        'lon': 0.23823945000000002
    }, {
        'localisant': ' Fargues-sur-Ourbise 47700',
        'lat': 44.2274585,
        'lon': 0.16381355
    }, {'localisant': ' Durance 47420', 'lat': 44.163878, 'lon': 0.16974515}, {
        'localisant': ' Ambrus 47160',
        'lat': 44.23194755,
        'lon': 0.2455563
    }, {'localisant': ' Xaintrailles 47230', 'lat': 44.2077879, 'lon': 0.2544115}, {
        'localisant': ' Casteljaloux 47700',
        'lat': 44.310425499999994,
        'lon': 0.08936799999999999
    }, {'localisant': ' Pompogne 47420', 'lat': 44.242577, 'lon': 0.0628795}, {
        'localisant': ' Houeillès 47420',
        'lat': 44.188771450000004,
        'lon': 0.0452712
    }, {'localisant': ' Vianne 47230', 'lat': 44.21490955, 'lon': 0.3204128}, {
        'localisant': ' Feugarolles 47230',
        'lat': 44.21333475,
        'lon': 0.3622885
    }, {'localisant': ' Espiens 47600', 'lat': 44.17094815, 'lon': 0.38222715}, {
        'localisant': ' Moirax 47310',
        'lat': 44.13566965,
        'lon': 0.60793995
    }, {
        'localisant': ' Clermont-Dessous 47130',
        'lat': 44.250134,
        'lon': 0.45352950000000003
    }, {
        'localisant': ' Montesquieu 47130',
        'lat': 44.2093344,
        'lon': 0.45274735
    }, {
        'localisant': ' Montagnac-sur-Auvignon 47600',
        'lat': 44.16460685,
        'lon': 0.4524896
    }, {
        'localisant': 'Colayrac-Saint-Cirq 47450',
        'lat': 44.23514325,
        'lon': 0.5586669
    }, {'localisant': ' Aubiac 47310', 'lat': 44.13786605, 'lon': 0.5686913}, {
        'localisant': ' Estillac 47310',
        'lat': 44.1708468,
        'lon': 0.5764705999999999
    }, {'localisant': ' Le Passage 47520', 'lat': 44.187869250000006, 'lon': 0.58317445}, {
        'localisant': ' Brax 47310',
        'lat': 44.19931415,
        'lon': 0.5586181
    }, {
        'localisant': ' Sérignac-sur-Garonne 47310',
        'lat': 44.213806399999996,
        'lon': 0.4972923
    }, {
        'localisant': ' Roquefort 47310',
        'lat': 44.17403349999999,
        'lon': 0.5615139
    }, {
        'localisant': ' Sainte-Colombe-en-Bruilhois 47310',
        'lat': 44.1874042,
        'lon': 0.51620425
    }, {'localisant': ' Foulayronnes 47510', 'lat': 44.2561813, 'lon': 0.6425914500000001}, {
        'localisant': ' Boé 47550',
        'lat': 44.1694773,
        'lon': 0.64950735
    }, {'localisant': ' Agen 47000', 'lat': 44.20103375, 'lon': 0.6301791999999999}
        , {'localisant': ' Cuq 47220', 'lat': 44.08445695, 'lon': 0.7180567}, {
        'localisant': ' Saint-Jean-de-Thurac 47270',
        'lat': 44.155162000000004,
        'lon': 0.7405279499999999
    }, {
        'localisant': ' Saint-Nicolas-de-la-Balerme 47220',
        'lat': 44.132359,
        'lon': 0.7586515
    }, {
        'localisant': ' Saint-Sixte 47220',
        'lat': 44.126935700000004,
        'lon': 0.780829
    }, {
        'localisant': ' Sauveterre-Saint-Denis 47220',
        'lat': 44.148875700000005,
        'lon': 0.7020753
    }, {
        'localisant': ' Caudecoste 47220',
        'lat': 44.122671499999996,
        'lon': 0.744917
    }, {
        'localisant': ' Pont-du-Casse 47480',
        'lat': 44.239420949999996,
        'lon': 0.6815049
    }, {'localisant': ' Lafox 47240', 'lat': 44.16550735, 'lon': 0.69838395}, {
        'localisant': ' Bon-Encontre 47240',
        'lat': 44.209048949999996,
        'lon': 0.6905549
    }, {'localisant': ' Fals 47220', 'lat': 44.10537275, 'lon': 0.6917955}, {
        'localisant': ' Layrac 47390',
        'lat': 44.123425499999996,
        'lon': 0.6596232500000001
    }, {
        'localisant': ' Sauméjan 47420',
        'lat': 44.227042,
        'lon': -0.022797650000000003
    }, {
        'localisant': ' Pindères 47700',
        'lat': 44.2720965,
        'lon': 0.0004820999999999992
    }, {
        'localisant': ' Saint-Alban 31140',
        'lat': 43.695539999999994,
        'lon': 1.41334485
    }, {'localisant': ' Bruguières 31150', 'lat': 43.72600785, 'lon': 1.4104983}, {
        'localisant': ' Launaguet 31140',
        'lat': 43.666797200000005,
        'lon': 1.4579575500000002
    }, {'localisant': ' Aucamville 31140', 'lat': 43.6714289, 'lon': 1.42433925}, {
        'localisant': ' Fonbeauzard 31140',
        'lat': 43.67922715,
        'lon': 1.4367873
    }, {
        'localisant': ' Castelginest 31780',
        'lat': 43.69807265,
        'lon': 1.4347872
    }, {
        'localisant': ' Villeneuve-lès-Bouloc 31620',
        'lat': 43.7814978,
        'lon': 1.4197340999999999
    }, {'localisant': ' Gratentour 31150', 'lat': 43.72059765, 'lon': 1.43320735}, {
        'localisant': ' Toulouse 31400',
        'lat': 43.600681,
        'lon': 1.43288755
    }, {'localisant': ' Toulouse 31500', 'lat': 43.600681, 'lon': 1.43288755}, {
        'localisant': ' Toulouse 31000',
        'lat': 43.600681,
        'lon': 1.43288755
    }, {'localisant': ' Toulouse 31200', 'lat': 43.600681, 'lon': 1.43288755}, {
        'localisant': ' Toulouse 31100',
        'lat': 43.600681,
        'lon': 1.43288755
    }, {'localisant': ' Toulouse 31300', 'lat': 43.600681, 'lon': 1.43288755}, {
        'localisant': ' Blagnac 31700',
        'lat': 43.64095135,
        'lon': 1.37710785
    }, {
        'localisant': ' Saint-Rustice 31620',
        'lat': 43.80251495,
        'lon': 1.32807835
    }, {
        'localisant': " Castelnau-d'Estrétefonds 31620",
        'lat': 43.795617899999996,
        'lon': 1.35162045
    }, {
        'localisant': ' Ondes 31330',
        'lat': 43.7858115,
        'lon': 1.3048378999999999
    }, {
        'localisant': ' Saint-Sauveur 31790',
        'lat': 43.7489968,
        'lon': 1.3943834499999999
    }, {
        'localisant': ' Lespinasse 31150',
        'lat': 43.717436449999994,
        'lon': 1.3820689499999999
    }, {'localisant': ' Fenouillet 31150', 'lat': 43.6864516, 'lon': 1.38954015}, {
        'localisant': ' Saint-Jory 31790',
        'lat': 43.7398082,
        'lon': 1.35395215
    }, {
        'localisant': ' Beauzelle 31700',
        'lat': 43.66799185,
        'lon': 1.3753686
    }, {
        'localisant': ' Gagnac-sur-Garonne 31150',
        'lat': 43.70786555,
        'lon': 1.3636829
    }, {'localisant': ' Grenade 31330', 'lat': 43.77037695, 'lon': 1.2790802000000001}, {
        'localisant': ' Illats 33720',
        'lat': 44.6087017,
        'lon': -0.37967090000000003
    }, {'localisant': ' Origne 33113', 'lat': 44.5024009, 'lon': -0.5089796}, {
        'localisant': ' Budos 33720',
        'lat': 44.53703755,
        'lon': -0.4056905
    }, {'localisant': ' Léogeats 33210', 'lat': 44.505758, 'lon': -0.36479705}, {
        'localisant': ' Balizac 33730',
        'lat': 44.4877479,
        'lon': -0.44994080000000003
    }, {
        'localisant': ' Noaillan 33730',
        'lat': 44.481232500000004,
        'lon': -0.3697415
    }, {
        'localisant': ' Saint-Symphorien 33113',
        'lat': 44.4261843,
        'lon': -0.5503838999999999
    }, {
        'localisant': ' Saint-Léger-de-Balson 33113',
        'lat': 44.42999745,
        'lon': -0.4414819
    }, {
        'localisant': ' Villandraut 33730',
        'lat': 44.4499632,
        'lon': -0.38103790000000004
    }, {'localisant': ' Préchac 33730', 'lat': 44.38587995, 'lon': -0.37151715}, {
        'localisant': ' Bourideys 33113',
        'lat': 44.36137155,
        'lon': -0.47706775
    }, {'localisant': ' Cazalis 33113', 'lat': 44.3304888, 'lon': -0.3873723}, {
        'localisant': ' Lucmau 33840',
        'lat': 44.295479650000004,
        'lon': -0.33626409999999995
    }, {'localisant': ' Captieux 33840', 'lat': 44.26213455, 'lon': -0.29803055}, {
        'localisant': ' Marions 33690',
        'lat': 44.373046450000004,
        'lon': -0.08800464999999999
    }, {
        'localisant': ' Sillas 33690',
        'lat': 44.37113175,
        'lon': -0.05854465
    }, {
        'localisant': ' Saint-Michel-de-Castelnau 33840',
        'lat': 44.2882525,
        'lon': -0.0938053
    }, {'localisant': ' Cudos 33430', 'lat': 44.3786367, 'lon': -0.18417485}, {
        'localisant': ' Sauviac 33430',
        'lat': 44.40010975,
        'lon': -0.18305525
    }, {'localisant': ' Bernos-Beaulac 33430', 'lat': 44.3641243, 'lon': -0.25856625}, {
        'localisant': ' Lavazan 33690',
        'lat': 44.3928935,
        'lon': -0.11326625000000001
    }, {'localisant': ' Escaudes 33840', 'lat': 44.3175266, 'lon': -0.204238}, {
        'localisant': ' Goualade 33840',
        'lat': 44.3181532,
        'lon': -0.12502375
    }, {
        'localisant': ' Giscos 33840',
        'lat': 44.26248925,
        'lon': -0.17476730000000001
    }, {
        'localisant': ' Lerm-et-Musset 33840',
        'lat': 44.335422949999995,
        'lon': -0.15792195
    }, {'localisant': ' Landiras 33720', 'lat': 44.5694862, 'lon': -0.4334122}, {
        'localisant': ' Guillos 33720',
        'lat': 44.55075865,
        'lon': -0.5235073
    }, {
        'localisant': ' Cabanac-et-Villagrains 33650',
        'lat': 44.589140099999995,
        'lon': -0.54099105
    }, {
        'localisant': ' Eysines 33320',
        'lat': 44.877859900000004,
        'lon': -0.64581695
    }, {
        'localisant': ' Martillac 33650',
        'lat': 44.7176058,
        'lon': -0.55811005
    }, {
        'localisant': " Villenave-d'Ornon 33140",
        'lat': 44.77367475,
        'lon': -0.55199775
    }, {
        'localisant': " Villenave-d'Ornon 33140",
        'lat': 44.77367475,
        'lon': -0.55199775
    }, {'localisant': ' Bègles 33130', 'lat': 44.8030335, 'lon': -0.5484173}, {
        'localisant': ' Saint-Selve 33650',
        'lat': 44.657469500000005,
        'lon': -0.47242154999999997
    }, {
        'localisant': ' Bassens 33530',
        'lat': 44.90690895,
        'lon': -0.5255777500000001
    }, {
        'localisant': ' Lormont 33310',
        'lat': 44.874862699999994,
        'lon': -0.51767945
    }, {
        'localisant': ' Ayguemorte-les-Graves 33640',
        'lat': 44.7026548,
        'lon': -0.48545745
    }, {'localisant': ' Quinsac 33360', 'lat': 44.7510197, 'lon': -0.4876486}, {
        'localisant': ' Latresne 33360',
        'lat': 44.789650550000005,
        'lon': -0.49845419999999996
    }, {'localisant': ' Cenon 33150', 'lat': 44.85527235, 'lon': -0.52233005}, {
        'localisant': ' Léognan 33850',
        'lat': 44.7234123,
        'lon': -0.6172263499999999
    }, {
        'localisant': ' Bruges 33520',
        'lat': 44.889384899999996,
        'lon': -0.60312145
    }, {
        'localisant': ' Gradignan 33170',
        'lat': 44.768133649999996,
        'lon': -0.61629955
    }, {'localisant': ' Talence 33400', 'lat': 44.80603535, 'lon': -0.591917}, {
        'localisant': ' Saint-Morillon 33650',
        'lat': 44.63876075,
        'lon': -0.52308845
    }, {
        'localisant': " Saint-Médard-d'Eyrans 33650",
        'lat': 44.71397815,
        'lon': -0.512509
    }, {'localisant': ' Floirac 33270', 'lat': 44.832193950000004, 'lon': -0.52161265}, {
        'localisant': ' Bouliac 33270',
        'lat': 44.81664925,
        'lon': -0.50249515
    }, {
        'localisant': ' Cadaujac 33140',
        'lat': 44.746767399999996,
        'lon': -0.53162235
    }, {'localisant': ' La Brède 33650', 'lat': 44.6774577, 'lon': -0.53958405}, {
        'localisant': ' Mérignac 33700',
        'lat': 44.8313112,
        'lon': -0.68195875
    }, {
        'localisant': ' Pessac 33600',
        'lat': 44.7860623,
        'lon': -0.68086875
    }, {
        'localisant': ' Isle-Saint-Georges 33640',
        'lat': 44.7276904,
        'lon': -0.47578905
    }, {'localisant': ' Cambes 33880', 'lat': 44.74084335, 'lon': -0.46455115}, {
        'localisant': ' Bordeaux 33100',
        'lat': 44.8634816,
        'lon': -0.5861912499999999
    }, {
        'localisant': ' Bordeaux 33200',
        'lat': 44.8634816,
        'lon': -0.5861912499999999
    }, {
        'localisant': ' Bordeaux 33300',
        'lat': 44.8634816,
        'lon': -0.5861912499999999
    }, {
        'localisant': ' Bordeaux 33000',
        'lat': 44.8634816,
        'lon': -0.5861912499999999
    }, {
        'localisant': ' Bordeaux 33800',
        'lat': 44.8634816,
        'lon': -0.5861912499999999
    }, {'localisant': ' Arbanats 33640', 'lat': 44.6678498, 'lon': -0.4023382}, {
        'localisant': ' Virelade 33720',
        'lat': 44.6562407,
        'lon': -0.3979589
    }, {
        'localisant': ' Saint-Michel-de-Rieufret 33720',
        'lat': 44.6272071,
        'lon': -0.44549615
    }, {'localisant': ' Portets 33640', 'lat': 44.67829675, 'lon': -0.4261395}, {
        'localisant': ' Beautiran 33640',
        'lat': 44.703719899999996,
        'lon': -0.4603302
    }, {
        'localisant': ' Castres-Gironde 33640',
        'lat': 44.686604200000005,
        'lon': -0.45652970000000004
    }, {
        'localisant': ' Camblanes-et-Meynac 33360',
        'lat': 44.7643346,
        'lon': -0.47394585
    }, {'localisant': ' Baurech 33880', 'lat': 44.7251401, 'lon': -0.4305083}, {
        'localisant': ' Ondres 40440',
        'lat': 43.5624386,
        'lon': -1.45792375
    }, {
        'localisant': ' Ousse-Suzan 40110',
        'lat': 43.94930975,
        'lon': -0.752132
    }, {
        'localisant': ' Ygos-Saint-Saturnin 40110',
        'lat': 43.993683399999995,
        'lon': -0.72959745
    }, {
        'localisant': ' Saint-Yaguen 40400',
        'lat': 43.9020227,
        'lon': -0.7411265499999999
    }, {
        'localisant': ' Beylongue 40370',
        'lat': 43.92007925,
        'lon': -0.82026905
    }, {'localisant': ' Carcen-Ponson 40400', 'lat': 43.86694085, 'lon': -0.8181159}, {
        'localisant': ' Tartas 40400',
        'lat': 43.815517549999996,
        'lon': -0.7790242
    }, {'localisant': ' Lesgor 40400', 'lat': 43.859205700000004, 'lon': -0.908701}, {
        'localisant': ' Bégaar 40400',
        'lat': 43.8305204,
        'lon': -0.86894895
    }, {
        'localisant': " Pontonx-sur-l'Adour 40465",
        'lat': 43.792849399999994,
        'lon': -0.93669165
    }, {'localisant': ' Herm 40990', 'lat': 43.80126475, 'lon': -1.1332009}, {
        'localisant': ' Gourbera 40990',
        'lat': 43.8064806,
        'lon': -1.0421977
    }, {
        'localisant': ' Laluque 40465',
        'lat': 43.85329935,
        'lon': -0.9815524
    }, {
        'localisant': ' Saint-Vincent-de-Paul 40990',
        'lat': 43.772563399999996,
        'lon': -0.99639915
    }, {
        'localisant': ' Saint-Paul-lès-Dax 40990',
        'lat': 43.7484815,
        'lon': -1.09412695
    }, {
        'localisant': ' Magescq 40140',
        'lat': 43.775147700000005,
        'lon': -1.184246
    }, {'localisant': ' Canenx-et-Réaut 40090', 'lat': 44.00205475, 'lon': -0.4600659}, {
        'localisant': ' Cère 40090',
        'lat': 43.9933372,
        'lon': -0.5468775
    }, {'localisant': ' Retjons 40120', 'lat': 44.1398806, 'lon': -0.29283309999999996}, {
        'localisant': ' Arue 40120',
        'lat': 44.05162985,
        'lon': -0.36188889999999996
    }, {
        'localisant': ' Lucbardez-et-Bargues 40090',
        'lat': 43.974810500000004,
        'lon': -0.4142075
    }, {'localisant': ' Pouydesseaux 40120', 'lat': 43.98801065, 'lon': -0.35175245}, {
        'localisant': ' Roquefort 40120',
        'lat': 44.03772175,
        'lon': -0.3287772
    }, {'localisant': ' Sarbazan 40120', 'lat': 44.0125691, 'lon': -0.32031825}, {
        'localisant': ' Bostens 40090',
        'lat': 43.97034635,
        'lon': -0.3674607
    }, {'localisant': ' Saint-Pandelon 40180', 'lat': 43.66752085, 'lon': -1.0415167}, {
        'localisant': ' Geloux 40090',
        'lat': 43.978501449999996,
        'lon': -0.6373658
    }, {
        'localisant': ' Bougue 40090',
        'lat': 43.9017901,
        'lon': -0.4033209
    }, {
        'localisant': ' Saint-Pierre-du-Mont 40280',
        'lat': 43.8725394,
        'lon': -0.51973105
    }, {
        'localisant': ' Mont-de-Marsan 40000',
        'lat': 43.8928796,
        'lon': -0.5010851000000001
    }, {
        'localisant': ' Saint-Perdon 40090',
        'lat': 43.873424549999996,
        'lon': -0.6038761500000001
    }, {
        'localisant': ' Campet-et-Lamolère 40090',
        'lat': 43.9164145,
        'lon': -0.5820516499999999
    }, {
        'localisant': ' Saint-Avit 40090',
        'lat': 43.943896949999996,
        'lon': -0.45262305
    }, {
        'localisant': ' Uchacq-et-Parentis 40090',
        'lat': 43.94495795,
        'lon': -0.54764495
    }, {
        'localisant': ' Mazerolles 40090',
        'lat': 43.8846917,
        'lon': -0.43339035000000004
    }, {
        'localisant': " Saint-Martin-d'Oney 40090",
        'lat': 43.929310099999995,
        'lon': -0.6497048000000001
    }, {'localisant': ' Saint-Gor 40120', 'lat': 44.0752965, 'lon': -0.2367363}, {
        'localisant': ' Maillas 40120',
        'lat': 44.21212335,
        'lon': -0.1787089
    }, {
        'localisant': ' Bourriot-Bergonce 40120',
        'lat': 44.15301755,
        'lon': -0.20912624999999999
    }, {
        'localisant': ' Rivière-Saas-et-Gourby 40180',
        'lat': 43.696408649999995,
        'lon': -1.1753646999999998
    }, {'localisant': ' Oeyreluy 40180', 'lat': 43.6746908, 'lon': -1.07906635}, {
        'localisant': ' Yzosse 40180',
        'lat': 43.71902405,
        'lon': -1.0149419
    }, {
        'localisant': ' Narrosse 40180',
        'lat': 43.692416050000006,
        'lon': -1.00506055
    }, {
        'localisant': ' Seyresse 40180',
        'lat': 43.680998349999996,
        'lon': -1.06112235
    }, {'localisant': ' Angoumé 40990', 'lat': 43.708114800000004, 'lon': -1.1391972}, {
        'localisant': ' Mées 40990',
        'lat': 43.71072585,
        'lon': -1.1291437
    }, {
        'localisant': ' Dax 40100',
        'lat': 43.70260145,
        'lon': -1.0635646
    }, {
        'localisant': ' Saint-Jean-de-Marsacq 40230',
        'lat': 43.6161113,
        'lon': -1.25442645
    }, {
        'localisant': ' Saint-Geours-de-Maremne 40230',
        'lat': 43.685532699999996,
        'lon': -1.23483425
    }, {
        'localisant': ' Saint-Vincent-de-Tyrosse 40230',
        'lat': 43.6630102,
        'lon': -1.3012331499999998
    }, {
        'localisant': ' Bénesse-Maremne 40230',
        'lat': 43.6342652,
        'lon': -1.3679168499999999
    }, {'localisant': ' Josse 40230', 'lat': 43.641903299999996, 'lon': -1.2373273}, {
        'localisant': ' Saubrigues 40230',
        'lat': 43.60906685,
        'lon': -1.324648
    }, {
        'localisant': ' Saint-André-de-Seignanx 40390',
        'lat': 43.56219795,
        'lon': -1.3618805
    }, {'localisant': ' Labenne 40530', 'lat': 43.598155399999996, 'lon': -1.4370645}, {
        'localisant': ' Orx 40230',
        'lat': 43.60468005,
        'lon': -1.3682182
    }, {'localisant': ' Capbreton 40130', 'lat': 43.63113935, 'lon': -1.4290093}, {
        'localisant': ' Angresse 40150',
        'lat': 43.659190300000006,
        'lon': -1.36234505
    }, {'localisant': ' Saubion 40230', 'lat': 43.6730544, 'lon': -1.33246625}, {
        'localisant': ' Tosse 40230',
        'lat': 43.69514985,
        'lon': -1.31582965
    }, {
        'localisant': ' Saint-Martin-de-Seignanx 40390',
        'lat': 43.5377319,
        'lon': -1.38286205
    }, {'localisant': ' Tarnos 40220', 'lat': 43.53376305, 'lon': -1.4718111999999999}, {
        'localisant': ' Urcuit 64990',
        'lat': 43.4828641,
        'lon': -1.34868945
    }, {
        'localisant': ' Villefranque 64990',
        'lat': 43.44518585,
        'lon': -1.4462613000000002
    }, {
        'localisant': ' Mouguerre 64990',
        'lat': 43.45883345,
        'lon': -1.4057602999999999
    },{
        'localisant': ' Ste Eutrope 47210',
        'lat': 44.575566,
        'lon': 0.705764
    },  {'localisant': ' Lahonce 64990', 'lat': 43.48160305, 'lon': -1.40189495}, {
        'localisant': ' Briscous 64240',
        'lat': 43.4600674,
        'lon': -1.3273586499999999
    }, {'localisant': ' Halsou 64480', 'lat': 43.38429595, 'lon': -1.4089939}, {
        'localisant': ' Jatxou 64480',
        'lat': 43.40587895,
        'lon': -1.41078655
    }, {
        'localisant': " Saint-Pierre-d'Irube 64990",
        'lat': 43.462832250000005,
        'lon': -1.44467745
    }, {
        'localisant': ' Boucau 64340',
        'lat': 43.52582155,
        'lon': -1.4813176499999998
    }, {
        'localisant': ' Larressore 64480',
        'lat': 43.368891399999995,
        'lon': -1.4445712
    }, {'localisant': ' Espelette 64250', 'lat': 43.3225875, 'lon': -1.45194985}, {
        'localisant': ' Itxassou 64250',
        'lat': 43.3096471,
        'lon': -1.4047483
    }, {
        'localisant': ' Hasparren 64240',
        'lat': 43.39497325,
        'lon': -1.31947005
    }, {
        'localisant': ' Cambo-les-Bains 64250',
        'lat': 43.36222875,
        'lon': -1.3888733
    }, {
        'localisant': ' Ustaritz 64480',
        'lat': 43.40194595,
        'lon': -1.470517
    }, {
        'localisant': ' Saint-Jean-de-Luz 64500',
        'lat': 43.3937029,
        'lon': -1.63350075
    }, {'localisant': ' Anglet 64600', 'lat': 43.48990165, 'lon': -1.5193052}, {
        'localisant': ' Arbonne 64210',
        'lat': 43.42012375,
        'lon': -1.55350575
    }, {'localisant': ' Arcangues 64200', 'lat': 43.4259383, 'lon': -1.5078628}, {
        'localisant': ' Bassussarry 64200',
        'lat': 43.4444604,
        'lon': -1.49744685
    }, {'localisant': ' Bidart 64210', 'lat': 43.4384541, 'lon': -1.57549955}, {
        'localisant': ' Biarritz 64200',
        'lat': 43.47105395,
        'lon': -1.55589105
    }, {
        'localisant': ' Bayonne 64100',
        'lat': 43.48437895,
        'lon': -1.4610524
    }, {
        'localisant': ' Saint-Pée-sur-Nivelle 64310',
        'lat': 43.34692985,
        'lon': -1.56046315
    }, {'localisant': ' Souraïde 64250', 'lat': 43.34316305, 'lon': -1.4889873}, {
        'localisant': ' Ciboure 64500',
        'lat': 43.376476499999995,
        'lon': -1.6660735999999998
    }, {'localisant': ' Biriatou 64700', 'lat': 43.32270855, 'lon': -1.73222995}, {
        'localisant': ' Ainhoa 64250',
        'lat': 43.2968038,
        'lon': -1.482808
    }, {'localisant': ' Ascain 64310', 'lat': 43.338283950000005, 'lon': -1.62816485}, {
        'localisant': ' Urrugne 64122',
        'lat': 43.3508758,
        'lon': -1.7017883500000002
    }, {'localisant': ' Urrugne 64700', 'lat': 43.3508758, 'lon': -1.7017883500000002}, {
        'localisant': ' Hendaye 64700',
        'lat': 43.3635623,
        'lon': -1.76199155
    }, {
        'localisant': ' Sare 64310',
        'lat': 43.29674315,
        'lon': -1.58235005
    }, {
        'localisant': ' Saint-Nicolas-de-la-Grave 82210',
        'lat': 44.06314145,
        'lon': 1.02613625
    }, {
        'localisant': ' Saint-Michel 82340',
        'lat': 44.0479007,
        'lon': 0.9376456500000001
    }, {'localisant': ' Merles 82210', 'lat': 44.066878, 'lon': 0.9645283}, {
        'localisant': ' Le Pin 82340',
        'lat': 44.036833200000004,
        'lon': 0.97601525
    }, {'localisant': ' Espalais 82400', 'lat': 44.08010765, 'lon': 0.9158066}, {
        'localisant': ' Caumont 82210',
        'lat': 44.02345085,
        'lon': 0.99654295
    }, {
        'localisant': ' Castelmayran 82210',
        'lat': 44.029428100000004,
        'lon': 1.0527729
    }, {
        'localisant': ' Asques 82120',
        'lat': 44.00848395,
        'lon': 0.9509048499999999
    }, {
        'localisant': ' Angeville 82210',
        'lat': 43.98862575,
        'lon': 1.03410445
    }, {
        'localisant': ' Saint-Porquier 82700',
        'lat': 44.00805484999999,
        'lon': 1.1793467
    }, {
        'localisant': ' Montech 82700',
        'lat': 43.962183100000004,
        'lon': 1.2365244999999998
    }, {
        'localisant': ' Montbeton 82290',
        'lat': 44.0139371,
        'lon': 1.2660704
    }, {
        'localisant': ' La Ville-Dieu-du-Temple 82290',
        'lat': 44.041715499999995,
        'lon': 1.2210079999999999
    }, {
        'localisant': ' Lacourt-Saint-Pierre 82290',
        'lat': 43.98784275,
        'lon': 1.27289715
    }, {
        'localisant': ' Escatalens 82700',
        'lat': 43.985127899999995,
        'lon': 1.1922340999999999
    }, {'localisant': ' Castelsarrasin 82100', 'lat': 44.0534187, 'lon': 1.12105245}, {
        'localisant': ' Auvillar 82340',
        'lat': 44.0659549,
        'lon': 0.8864398
    }, {'localisant': ' Sistels 82340', 'lat': 44.05083415, 'lon': 0.7839446}, {
        'localisant': ' Saint-Loup 82340',
        'lat': 44.08145025,
        'lon': 0.85497145
    }, {'localisant': ' Saint-Cirice 82340', 'lat': 44.060888750000004, 'lon': 0.837974}, {
        'localisant': ' Dunes 82340',
        'lat': 44.08657384999999,
        'lon': 0.7759751
    }, {
        'localisant': ' Donzac 82340',
        'lat': 44.09932555,
        'lon': 0.8167747000000001
    }, {'localisant': ' Saint-Aignan 82100', 'lat': 44.0214707, 'lon': 1.0735725}, {
        'localisant': ' Lafitte 82100',
        'lat': 43.975904299999996,
        'lon': 1.10391115
    }, {
        'localisant': ' Garganvillar 82100',
        'lat': 43.972099549999996,
        'lon': 1.0717352
    }, {
        'localisant': ' Cordes-Tolosannes 82700',
        'lat': 43.9744789,
        'lon': 1.13605445
    }, {'localisant': ' Castelferrus 82100', 'lat': 44.0088127, 'lon': 1.0905442}, {
        'localisant': ' Montauban 82000',
        'lat': 44.021735050000004,
        'lon': 1.36466665
    }, {
        'localisant': ' Fabas 82170',
        'lat': 43.862178799999995,
        'lon': 1.3407605500000002
    }, {
        'localisant': ' Labastide-Saint-Pierre 82370',
        'lat': 43.923321349999995,
        'lon': 1.34404215
    }, {
        'localisant': ' Dieupentale 82170',
        'lat': 43.86861285,
        'lon': 1.2816158500000001
    }, {
        'localisant': ' Pompignan 82170',
        'lat': 43.819277400000004,
        'lon': 1.3289725
    }, {'localisant': ' Grisolles 82170', 'lat': 43.8240502, 'lon': 1.29213815}, {
        'localisant': ' Campsas 82370',
        'lat': 43.889005600000004,
        'lon': 1.3289843000000001
    }, {'localisant': ' Bressols 82710', 'lat': 43.95234375, 'lon': 1.3170351}, {
        'localisant': ' Canals 82170',
        'lat': 43.86061665,
        'lon': 1.2976802
    }, {'localisant': ' Montbartier 82700', 'lat': 43.9162093, 'lon': 1.28658085}, {
        'localisant': ' Bessens 82170',
        'lat': 43.8858403,
        'lon': 1.2684902
    }, {'localisant': ' montrabé 31850', 'lat': 43.643632600000004, 'lon': 1.5333092}, {
        'localisant': ' beaupuy 31850',
        'lat': 43.651665300000005,
        'lon': 1.55619495
    }, {
        'localisant': ' saint-marcel-paulel 31590',
        'lat': 43.65651335,
        'lon': 1.61486705
    }, {
        'localisant': ' Gragnague 31380',
        'lat': 43.685191950000004,
        'lon': 1.58276965
    }, {
        'localisant': ' Bonrepos-Riquet 31590',
        'lat': 43.675837200000004,
        'lon': 1.62405415
    }, {'localisant': ' verfeil 31590', 'lat': 43.65787695, 'lon': 1.6791488}, {
        'localisant': ' Teulat 81500',
        'lat': 43.63669525,
        'lon': 1.7103619
    }, {'localisant': ' Bannières 81500', 'lat': 43.6195295, 'lon': 1.7596322}, {
        'localisant': ' Francarville 31460',
        'lat': 43.5872348,
        'lon': 1.74798995
    }, {
        'localisant': ' Vendine 31460',
        'lat': 43.59043525,
        'lon': 1.7650932
    }, {
        'localisant': ' Villeneuve lès Lavaur 81500',
        'lat': 43.60021915,
        'lon': 1.78232175
    }, {
        'localisant': ' Maurens-Scopont 81470',
        'lat': 43.595150950000004,
        'lon': 1.8147074
    }, {
        'localisant': ' Cambon-lès-Lavaur 81470',
        'lat': 43.591885,
        'lon': 1.8451891
    }, {'localisant': ' Cuq Toulza 81470', 'lat': 43.5642735, 'lon': 1.88839875}, {
        'localisant': ' Lacroisille 81470',
        'lat': 43.58156735,
        'lon': 1.92533175
    }, {'localisant': ' Algans 81470', 'lat': 43.60343395, 'lon': 1.8914670500000001}, {
        'localisant': ' Appelle 81700',
        'lat': 43.58130565,
        'lon': 1.9562015000000001
    }, {
        'localisant': ' Puylaurens 81700',
        'lat': 43.579446700000005,
        'lon': 2.00880125
    }, {'localisant': ' Vielmur-sur-Agout 81570', 'lat': 43.62658275, 'lon': 2.0964727}, {
        'localisant': ' Soual 81580',
        'lat': 43.5492778,
        'lon': 2.1258709
    }, {
        'localisant': ' Saint-Germain-des-Prés 81700',
        'lat': 43.56253555,
        'lon': 2.0845522
    }, {
        'localisant': ' Cambounet-sur-le-Sor 81580',
        'lat': 43.577719900000005,
        'lon': 2.1170616
    }, {
        'localisant': ' Fréjeville 81570',
        'lat': 43.60879895,
        'lon': 2.1484392999999997
    }, {
        'localisant': ' Viviers-lès-Montagnes 81290',
        'lat': 43.54916235,
        'lon': 2.1798581500000003
    }, {'localisant': ' Saix 81710', 'lat': 43.586219650000004, 'lon': 2.17280615}, {
        'localisant': ' Castres 81100',
        'lat': 43.6131355,
        'lon': 2.2448758
    },  ]

    var greenIcon = L.icon({
        iconUrl: './../../static/frontend/marker.png',
        iconSize: [46, 56], // size of the icon
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    function trigger(map) {
        let time = new Date().valueOf();
        // if (time % 13 === 0) {
        setBoundingbox(map.getBounds())
        // }
    }


    const post = () => {
        setDataerror(false)
        setData_error(false)
        setData__error(false)
        setLoadingdata(true)
        setDataok(false)
        setData_ok(false)
        setData__ok(false)
        postmap(boundingbox, sup, inf,ip).then((res) => {
            setData(res)
            setLoadingdata(false)
            setDataok(true)
            //console.log(`data:${res}`)
        }).catch((err) => {
            setLoadingdata(false)
            setDataerror(true)
        })
        setLoadingdata__(true)
        postmaptiba(boundingbox, sup, inf,ip).then((res) => {
            setData__(res)
            setLoadingdata__(false)
            setData__ok(true)
            //console.log(`data__:${res}`)
        }).catch((err) => {
            setLoadingdata__(false)
            setData__error(true)
        })
        setLoadingdata_(true)
        postmapnontiba(boundingbox, sup, inf,ip).then((res) => {
            setData_(res)
            setLoadingdata_(false)
            setData_ok(true)
        }).catch((err) => {
            setLoadingdata_(false)
            setData_error(true)
        })
    };
    const contentalert = <div className="flex align-items-center justify-content-center">
        <Alert color="danger">
            <h4 className="alert-heading">
                Un probleme est survenue lors de la récupération des données.<br/>
                Ressayez en recliquant sur le bouton, et/ou en ajustant le zoom et/ou sur une autre commune<br/>
            </h4>
        </Alert>
    </div>
    const contentok = <div className="flex align-items-center justify-content-center">
        <Alert>
            <h4 className="alert-heading">
                Les données ont été téléchargées correctement<br/>
            </h4>
        </Alert>
    </div>
    useEffect(() => {
        const nonbati = document.querySelector("#non_bati");
        //const ingredients = document.querySelectorAll("ul input");
        if (nonbati !== null) {
            nonbati.addEventListener("click", (e) => {
                setPos(e.target.checked);
                map.setView([map._lastCenter.lat, map._lastCenter.lng - 0.0001]);

            })
        }
        const bati = document.querySelector("#bati");
        //const ingredients = document.querySelectorAll("ul input");
        if (bati !== null) {
            bati.addEventListener("click", (e) => {
                setPoum(e.target.checked);
                map.setView([map._lastCenter.lat, map._lastCenter.lng - 0.0001]);

            })
        }

    }, [map]);

    useEffect(() => {
        let mapInstance = map;
        if (mapInstance === null) {
            // create the map instance
            var item = markers[(Math.random() * markers.length) | 0];

            mapInstance = L.map(ref.current, {
                center: [item["lat"], item["lon"]],
                //center: [44.533211, 0.662735],

                zoom: 16
            });

            // add the tile layer
            let openstreetmap = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
                maxZoom: 17,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });
            let googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                maxZoom: 17,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            });
            let googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}', {
                maxZoom: 17,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            });
            googleHybrid.addTo(mapInstance);

            var baseLayers = {
                "Openstreetmap": openstreetmap,
            };
            var overlayLayers = {
                "Google Satellite": googleHybrid
            };
            // mapInstance.on('overlayadd', function (eventLayer) {
            //     if (eventLayer.name === 'Google Satellite') {
            //         map.addControl(popuLegend);
            //     } else {
            //         map.removeControl(popuLegend);
            //     }
            // });
            // Add any other layers or features to the map here
            // For example, adding a marker:

            for (let i = 0; i < markers.length; i++) {
                L.marker([markers[i]["lat"], markers[i]["lon"]], {
                    title: markers[i]["localisant"],
                    icon: greenIcon
                }).bindTooltip(markers[i]["localisant"], {permanent: true}).openTooltip([markers[i]["lat"], markers[i]["lon"]]).addTo(mapInstance);
                //var tooltip = L.tooltip({permanent:true},latlng, {content: 'Hello world!<br />This is a nice tooltip.'}).addTo(map);
            }
            // mapInstance.addEventListener("mousemove", (e) => {
            //     trigger(mapInstance);
            // })
            mapInstance.addEventListener("mouseover", (e) => {
                trigger(mapInstance);
            })
            mapInstance.addEventListener("load", (e) => {
                trigger(mapInstance);
            })
            mapInstance.addEventListener("zoomend", (e) => {
                trigger(mapInstance);
            })
            mapInstance.addEventListener("moveend", (e) => {
                trigger(mapInstance);
            })
            //Create a LayerGroup and add it to the map


            //var polygon = L.polygon(l1, {color: 'red'}).addTo(mapInstance);
            const layerGroup = L.layerGroup().addTo(mapInstance);
            const layerGroup0 = L.layerGroup().addTo(mapInstance);
            var legend = L.control({position: 'bottomleft'});
            legend.onAdd = function (mapInstance) {
                var div = L.DomUtil.create('div', 'info legend');
                let labels = ['<div style="background-color: white;"><input id="non_bati" type="checkbox" name="checkbox" checked />']
                labels.push('<label for="non_bati">Non bati</label></div><br>')
                labels.push('<div style="background-color: white;"><input id="bati" type="checkbox" name="checkbox" checked />')
                labels.push('<label for="bati">Bati</label></div><br>')
                // labels.push(['<strong>Categories</strong><br>'])
                // let categories = ['Road Surface', 'Signage', 'Line Markings', 'Roadside Hazards', 'Other'];
                // for (var i = 0; i < categories.length; i++) {
                //     div.innerHTML +=
                //         labels.push(
                //             '<i class="circle" style="background:' + getColor(categories[i]) + '"></i> <br>' +
                //             (categories[i] ? categories[i] : '+'));
                //
                // }

                div.innerHTML = labels.join('');

                return div;

            };
            legend.addTo(mapInstance);
            setMapLayerGroup(layerGroup);
            setMapLayerGroup0(layerGroup0);

            setMap((map) => mapInstance);
        }

    }, [map]);

    function displayMutinfos(id) {
        setLoadingid(true)
        postid(id,ip).then((res) => {
            console.log(res)
            setId(res[0][0])
            setDatemut(res[0][12])
            setAnneemut(res[0][14])
            setSbati(res[0][8])
            setSterr(res[0][9])
            setValeurfonc(res[0][26])
            setSbatieurom2((sbatieurom2) => res[0][27])
            setSterreuroha((sterreuroha) => res[0][28])
            setSterreurom2((sterreurom2) => res[0][29])
            setVefa(res[0][6])
            setNblocmut(res[0][15])
            setNbparmut(res[0][16])
            setNbvolmut(res[0][17])
            setLibnatmut(res[0][18])
            setCodtypbien(res[0][19])
            setIdmutinvar(res[0][20])
            setIdopendata(res[0][21])
            setLcodinsee(res[0][22])
            setLidlocmut(res[0][23])
            setLidparmut(res[0][24])
            setLibtypbien(res[0][25])
            setCoddep(res[0][26])
            setNbcomm(res[0][27])
            setLidpar(res[0][28])
            setNbpar(res[0][29])
            setLat(JSON.parse(res[0][1])[0][0][0][0])
            setLon(JSON.parse(res[0][1])[0][0][0][1])
            setLoadingid(false)


        })
    }

    let textpolygonarray = []
    let polygonarray = []


    function displaytiba(ml) {
        //console.log("displaylayer")
        if (Boolean(data__) && poum !== false) {
            //console.log("displaylayer true")
            textpolygonarray = []
            polygonarray = []
            ml.clearLayers()
            ml.remove()
            for (let i = 0; i < data__.length; i++) {
                textpolygonarray.push({"polygon": JSON.parse(data__[i][1]), "id": data__[i][0]});
            }
            //console.log(textpolygonarray)
            for (let j = 0; j < textpolygonarray.length; j++) {
                polygonarray.push({
                    "lpolygon": L.polygon(textpolygonarray[j]["polygon"][0][0], {color: 'red'}),
                    "id": textpolygonarray[j]["id"]
                });

            }
            //console.log(polygonarray)
            let id = null
            for (let k = 0; k < polygonarray.length; k++) {
                id = polygonarray[k]["id"]
                polygonarray[k]["lpolygon"]
                polygonarray[k]["lpolygon"].bindPopup(function (id) {
                    displayMutinfos(polygonarray[k]["id"])
                }).addTo(ml);
            }
            ml.addTo(map)

        }
    }


    const handleSup = (e) => {
        setNoticesup(false)
        if (typeof e.target.value != "string" || !e.target.value) return setSup(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticesup("Entrer un nombre")
        }
        setSup(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleInf = (e) => {
        setNoticeinf(false)
        if (typeof e.target.value != "string" || !e.target.value) return setInf(100000000000000000000)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticeinf("Entrer un nombre")
        }
        setInf(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    // useEffect(() => {
    //     console.log(`inf:${inf}`)
    //     console.log(`sup:${sup}`)
    //
    // }, [sup, inf]);
    const zoomPan = (event, itemlat, itemlon) => {
        event.preventDefault();
        setZoom([])
        const zoompan = new L.latLng([itemlat, itemlon]);
        mapLayerGroup.clearLayers();
        //map._animateZoom(zoompan, 16 );
        map.setView(zoompan);
        setCommune("")
    }
    const handleKeyDown = (event) => {
        if (event.key === 'Enter') {
            //console.log('do validate')
        }
    }

    const handleOnChange = (e, field) => {
        if (!e.target.value) {
            setCommune("");
            return setZoom([])
        }
        let value = e.target.value;
        setCommune(e.target.value);
        const resultArray = markers.filter(item => {
            const searchterm = value.toLowerCase().replaceAll("-", " ").replace("sainte", "ste").replace("saint", "st").replaceAll("é", "e").replaceAll("è", "e").replaceAll("ê", "e").replace("ï", "i").replace("î", "i").replace("ô", "o").replaceAll("'", " ");
            const itemlower = item.localisant.toLowerCase().replaceAll("-", " ").replace("sainte", "ste").replace("saint", "st").replaceAll("é", "e").replaceAll("è", "e").replaceAll("ê", "e").replace("ï", "i").replace("î", "i").replace("ô", "o").replaceAll("'", " ");
            return searchterm && itemlower.includes(searchterm)
        });
        setZoom(resultArray);
    }

    function displaynontiba(ml) {
        //console.log("displaylayer")
        if (Boolean(data_) && pos !== false) {
            //console.log("displaylayer true")
            textpolygonarray = []
            polygonarray = []
            ml.clearLayers()
            ml.remove()
            for (let i = 0; i < data_.length; i++) {
                textpolygonarray.push({"polygon": JSON.parse(data_[i][1]), "id": data_[i][0]});
            }
            //console.log(textpolygonarray)
            for (let j = 0; j < textpolygonarray.length; j++) {
                polygonarray.push({
                    "lpolygon": L.polygon(textpolygonarray[j]["polygon"][0][0], {color: 'purple'}),
                    "id": textpolygonarray[j]["id"]
                });

            }
            //console.log(polygonarray)
            let id = null
            for (let k = 0; k < polygonarray.length; k++) {
                id = polygonarray[k]["id"]
                polygonarray[k]["lpolygon"]
                polygonarray[k]["lpolygon"].bindPopup(function (id) {
                    displayMutinfos(polygonarray[k]["id"])
                }).addTo(ml);
            }
            ml.addTo(map)

        }
    }

    useEffect(() => {
        if (mapLayerGroup !== null) {
            //console.log(`maplayer not null, poum:${poum}`)
            //console.log(poum)
            if (poum) {
                //console.log("checked")
                //console.log(mapLayerGroup)
                displaytiba(mapLayerGroup)
            } else {
                //console.log(mapLayerGroup)
                //console.log('not checked')
                mapLayerGroup.clearLayers()
            }
        }
    }, [data__]);

    useEffect(() => {
        //console.log(`pos:${pos}`)
        if (mapLayerGroup0 !== null) {
            //console.log(`maplayer not null pos:${pos}`)
            //console.log(pos)
            if (pos) {
                //console.log("checked")
                //console.log(mapLayerGroup0)
                displaynontiba(mapLayerGroup0)
            } else {
                //console.log(mapLayerGroup0)
                //console.log('not checked')
                mapLayerGroup0.clearLayers()
            }
        }
    }, [data_]);

    const handleLinkparcelle = (event) => {
        if (map !== null) {
            event.preventDefault();
            const itemlat = 44.557062
            const itemlon = 0.64804
            mapLayerGroup.clearLayers();
            map._animateZoom([itemlat, itemlon], 17);
            map.setView([itemlat, itemlon - 0.0001]);
        }
    }
    useEffect(() => {
        console.log(map)
        if (map!==null) {
        trigger(map)
            }
    }, [map])
    //     useEffect(() => {
    //     displaynontiba(mapLayerGroup)
    // }, [nontiba]);
    // var l0 = data0["features"][0]["geometry"]["coordinates"][0][0]
    // var l1 = data0["features"][1]["geometry"]["coordinates"][0][0]
    // var polygon0 = L.polygon(l0, {color: 'red'})
    // var polygon1 = L.polygon(l1, {color: 'red'}).addTo(mapInstance);
    // function getColor(d) {
    //     return d === 'Road Surface' ? "#de2d26" :
    //         d === 'Signage' ? "#377eb8" :
    //             d === 'Line Markings' ? "#4daf4a" :
    //                 d === 'Roadside Hazards' ? "#984ea3" :
    //                     "#ff7f00";
    // }


// for (const ingredient of ingredients) {
//   ingredient.addEventListener("click", updateDisplay);
// }

    // function updateDisplay() {
    //     let checkedCount = 0;
    //     for (const ingredient of ingredients) {
    //         if (ingredient.checked) {
    //             checkedCount++;
    //         }
    //     }
    //
    //     if (checkedCount === 0) {
    //         overall.checked = false;
    //         overall.indeterminate = false;
    //     } else if (checkedCount === ingredients.length) {
    //         overall.checked = true;
    //         overall.indeterminate = false;
    //     } else {
    //         overall.checked = false;
    //         overall.indeterminate = true;
    //     }
    // }


    return (
        <>
            <div className="container-fluid">
                <br/>
                {/*<ReactHowler*/}
                {/*    src='http://apere.free.fr/shift_.mp3'*/}
                {/*    playing={true}*/}
                {/*    volume={0.5}*/}
                {/*/>*/}
                <div className="row h-100 d-flex flex-row">
                    <div className="col-8">
                        <Form>
                            <div className="row">
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticesup)}
                                            id="inf"
                                            name="inf"
                                            placeholder="Valeur €/ha supérieure à"
                                            onChange={(e) => handleSup(e)}
                                        />
                                        <FormText>1€/m2=10 000€/ha</FormText>
                                        <FormFeedback>{noticesup}</FormFeedback>
                                    </FormGroup>
                                </div>
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticeinf)}
                                            id="inf"
                                            name="inf"
                                            placeholder="Valeur €/ha inférieure à"
                                            onChange={(e) => handleInf(e)}
                                        />

                                        <FormFeedback>{noticeinf}</FormFeedback>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="zoom"
                                        name="zoom"
                                        value={commune}
                                        placeholder="Entrer le nom ou le code postal de la commune sur laquelle vous voulez zoomer"
                                        onChange={(e) => handleOnChange(e)}
                                    />
                                </FormGroup>
                            </div>

                        </Form>
                        <div className="itemsdropdown">
                            {zoom.map((item) => (
                                <div onClick={(event) => zoomPan(event, item.lat, item.lon)} key={item.localisant} id={item.localisant}
                                     className="itemdropdown">{item.localisant}</div>))}
                        </div>
                        <div className="row">

                            <div className="col-4">
                                <Button onClick={post} color="primary">Afficher les transactions</Button>
                            </div>
                            <div className="col-4">
                            </div>

                            <div className="col-4">
                                {(loadingdata || loadingdata_ || loadingdata__) ? (<Spinner/>) : (<div></div>)}
                            </div>


                        </div>
                        <br/>

                        <br/>

                        <div
                            ref={ref}
                            id="map"
                            className="h-100 w-100"
                            style={{height: "800px"}}
                        />
                </div>
                    <div className="col-4">
                        <div>
                            {(dataerror || data_error || data__error) ? (<div key="alert">{contentalert}</div>) : (
                                <div></div>)}
                            {(dataok && data_ok && data__ok) ? (<div key="ok">{contentok}</div>) : (
                                <div></div>)}
                        </div>
                        <div>
                        {loadingid ? (<Spinner/>

                        ) : (<Feature
                            datemut={datemut}
                            anneemut={anneemut}
                            sbati={sbati}
                            sterr={sterr}
                            valeurfonc={valeurfonc}
                            sbatieuro_par_m2={sbatieurom2}
                            sterr_euro_par_ha={sterreuroha}
                            sterr_euro_par_m2={sterreurom2}
                            vefa={vefa}
                            nblocmut={nblocmut}
                            nbvolmut={nbvolmut}
                            libnatmut={libnatmut}
                            codtypbien={codtypbien}
                            idmutinvar={idmutinvar}
                            idopendata={idopendata}
                            lcodinsee={lcodinsee}
                            lidlocmut={lidlocmut}
                            lidparmut={lidparmut}
                            libtypbien={libtypbien}
                            coddep={coddep}
                            nbcomm={nbcomm}
                            lidpar={lidpar}
                            nbpar={nbpar}
                            id={id}
                            lat={lat}
                            lon={lon}
                        />)}
                        </div>
                    </div>

                <br/>
            </div>
            <div>
            </div>
            <div className="col-md-8 col-12">
                {/*<Link onClick={handleLinkparcelle}>Test lien vers une parcelle dont le prix est*/}
                {/*    élevé</Link><br/><br/>*/}
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                
            </div>
            </div>
            {/*{postmapMut.isLoading ? (*/}
            {/*        <div className="align-items-center text-center flex flex-wrap justify-content-center">*/}
            {/*            <br/>*/}
            {/*            <br/>*/}
            {/*            <Spinner/>*/}
            {/*        </div>) :*/}
            {/*    postmapMut.isError ? (*/}
            {/*        < Content*/}
            {/*            content={notok}*/}
            {/*            offset={0}*/}
            {/*        />) : (*/}
            {/*        < Content*/}
            {/*            content={ok}*/}
            {/*            offset={0}*/}
            {/*        />)*/}

            {/*}*/}
        </>
    );
};
export default MapComponent;
