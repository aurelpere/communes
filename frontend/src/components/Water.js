import {useEffect, useRef, useState} from "react";
import React from 'react';
import {Alert, Button, Form, FormFeedback, FormGroup, FormText, Input, Spinner} from "reactstrap";
import waterpost from "../functions/waterpost";
import {Link} from "react-router-dom";
import axios from "axios";

const Water = () => {
    const ref = useRef(null);
    const [lat, setLat] = useState(null);
    const [lon, setLon] = useState(null);
    const [consohebdo, setConsohebdo] = useState(null);
    const [consohebdoestival, setConsohebdoestival] = useState(0);
    const [moisestivaldebut, setMoisestivaldebut] = useState(5);
    const [moisestivalfin, setMoisestivalfin] = useState(9);
    const [anneedebut, setAnneedebut] = useState(2010);
    const [anneefin, setAnneefin] = useState(2020);

    const [noticelon, setNoticelon] = useState(false);
    const [noticelat, setNoticelat] = useState(false);
    const [noticeconsohebdo, setNoticeconsohebdo] = useState(false);
    const [noticeconsohebdoestival, setNoticeconsohebdoestival] = useState(false);

    const [noticemoisestivaldebut, setNoticemoisestivaldebut] = useState(false);
    const [noticemoisestivalfin, setNoticemoisestivalfin] = useState(false);

    const [noticeanneedebut, setNoticeanneedebut] = useState(false);
    const [noticeanneefin, setNoticeanneefin] = useState(false);

    const [data, setData] = useState([[],[]]);

    const [loadingdata, setLoadingdata] = useState(false);
    const [dataerror, setDataerror] = useState(false);
    const [dataok, setDataok] = useState(false);
    const [ip,setIp]=useState("999.999.999.999");

 useEffect(() => {
  axios.get('https://api.ipify.org')
    .then(response => {
      setIp(response.data);
      console.log(response.data); // Use response.data, not ipAddressResponse.data
      // Use ipAddress for further processing
    })
    .catch(error => {
      console.log("there was an error retrieving your ip adress with ipify. A default ip adress will be used and should not prevent the website to work properly")
      console.log(error);
      setIp("999.999.999.999");
    });
}, []);
 useEffect(() => {
  console.log(data);
}, [data]);


    const post = () => {
        setDataerror(false)
        setLoadingdata(true)
        setDataok(false)
        waterpost(ip,lat,lon,consohebdo,consohebdoestival,moisestivaldebut,moisestivalfin,anneedebut,anneefin).then((res) => {
            setData(res)
            setLoadingdata(false)
            setDataok(true)
            //console.log(`data:${res}`)
        }).catch((err) => {
            setLoadingdata(false)
            setDataerror(true)
        })
    };
    const contentalert = <div className="flex align-items-center justify-content-center">
        <Alert color="danger">
            <h4 className="alert-heading">
                Un probleme est survenue lors de la récupération des résultats.<br/>
                Ressayez en recliquant sur le bouton et en vérifiant les données d'entrée<br/>
            </h4>
        </Alert>
    </div>
    const contentok = <div className="flex align-items-center justify-content-center">
        <Alert>
            <h4 className="alert-heading">
                Les résultats ont été téléchargées correctement<br/>
            </h4>
        </Alert>
    </div>

    const handleLat = (e) => {
        setNoticelat(false)
        if (typeof e.target.value != "string") return setLat(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticelat("Entrer un nombre")
        }
        setLat(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleLon = (e) => {
        setNoticelon(false)
        if (typeof e.target.value != "string") return setLon(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setNoticelon("Entrer un nombre")
        }
        setLon(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleConsohebdo = (e) => {
        setNoticeconsohebdo(false)
        if (typeof e.target.value != "string") return setConsohebdo(0)
        let value = e.target.value;
        value = value.replace(",", ".")
        if (isNaN(value) || isNaN(parseFloat(value))) {
            setConsohebdo("Entrer un nombre")
        }
        setConsohebdo(!isNaN(value) && !isNaN(parseFloat(value)) && parseFloat(value))
    }
    const handleConsohebdoestival = (e) => {
        setNoticeconsohebdoestival(false)
        if (typeof e.target.value != "string") return setConsohebdoestival(0)
        let value = e.target.value;
        setConsohebdoestival(value)
    }
    const handleMoisestivaldebut = (e) => {
        setNoticemoisestivaldebut(false)
        if (typeof e.target.value != "string") return setMoisestivaldebut(5)
        let value = e.target.value;
        setMoisestivaldebut(value)
    }
    const handleMoisestivalfin = (e) => {
        setNoticemoisestivalfin(false)
        if (typeof e.target.value != "string") return setMoisestivalfin(9)
        let value = e.target.value;
        setMoisestivalfin(value)
    }
    const handleAnneedebut = (e) => {
        setNoticeanneedebut(false)
        if (typeof e.target.value != "string") return setAnneedebut(2010)
        let value = e.target.value;
        setAnneedebut(value)
    }
    const handleAnneefin = (e) => {
        setNoticeanneefin(false)
        if (typeof e.target.value != "string") return setAnneefin(2020)
        let value = e.target.value;
        setAnneefin(value)
    }


    return (
        <>
            <div className="container-fluid">
                <br/>
                {/*<ReactHowler*/}
                {/*    src='http://apere.free.fr/shift_.mp3'*/}
                {/*    playing={true}*/}
                {/*    volume={0.5}*/}
                {/*/>*/}
                <div className="row h-100 d-flex flex-row">
                    <div className="col-8">
                        <Form>
                            <div className="row">
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticelat)}
                                            id="lat"
                                            name="lat"
                                            placeholder="Latitude"
                                            onChange={(e) => handleLat(e)}
                                        />
                                        <FormText>Latitude WGS 84. Exemple: 44.2 pour Agen, Lot-et-Garone</FormText>
                                        <FormFeedback>{noticelat}</FormFeedback>
                                    </FormGroup>
                                </div>
                                <div>
                                    <FormGroup>
                                        <Input
                                            invalid={Boolean(noticelon)}
                                            id="lon"
                                            name="lon"
                                            placeholder="Longitude"
                                            onChange={(e) => handleLon(e)}
                                        />
                                        <FormText>Longitude WGS 84. Exemple: 0.6 pour Agen, Lot-et-Garone</FormText>
                                        <FormFeedback>{noticelon}</FormFeedback>
                                    </FormGroup>
                                </div>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="consohebdo"
                                        name="consohebdo"
                                        placeholder="Consommation hebdomadaire constante(L)"
                                        onChange={(e) => handleConsohebdo(e)}
                                    />
                                    <FormText>Consommation hebdomadaire constante (L)</FormText>
                                    <FormFeedback>{noticeconsohebdo}</FormFeedback>
                                </FormGroup>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="consohebdoestival"
                                        name="consohebdoestival"
                                        value={consohebdoestival}
                                        placeholder="Consommation hebdomadaire estivale supplémentaire"
                                        onChange={(e) => handleConsohebdoestival(e)}
                                    />
                                    <FormText>Consommation hebdomadaire estivale supplémentaire (L) (0L par défaut)</FormText>
                                    <FormFeedback>{noticeconsohebdoestival}</FormFeedback>
                                </FormGroup>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="moisestivaldebut"
                                        name="moisestivaldebut"
                                        value={moisestivaldebut || ""}
                                        placeholder="Premier mois de la periode estivale"
                                        onChange={(e) => handleMoisestivaldebut(e)}
                                    />
                                    <FormText>Premier mois de la periode estivale (chiffre, 05 par défaut)</FormText>
                                </FormGroup>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="moisestivalfin"
                                        name="moisestivalfin"
                                        value={moisestivalfin}
                                        placeholder="Dernier mois de la periode estivale"
                                        onChange={(e) => handleMoisestivalfin(e)}
                                    />
                                    <FormText>Dernier mois de la periode estivale (chiffre, 09 par defaut)</FormText>
                                </FormGroup>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="anneedebut"
                                        name="anneedebut"
                                        value={anneedebut}
                                        placeholder="Année de début"
                                        onChange={(e) => handleAnneedebut(e)}
                                    />
                                    <FormText>Première année de la periode à prendre en compte pour les données (2010 par défaut, données disponibles jusqu'en 1996)</FormText>
                                </FormGroup>
                            </div>
                            <div className="row">
                                <FormGroup>
                                    <Input
                                        id="anneefin"
                                        name="anneefin"
                                        value={anneefin}
                                        placeholder="Année de fin"
                                        onChange={(e) => handleAnneefin(e)}
                                    />
                                    <FormText>Derniere année de la periode à prendre en compte pour les données (2020 par défaut, données disponibles jusqu'en 2023)</FormText>
                                </FormGroup>
                            </div>
                        </Form>

                        <div className="row">

                            <div className="col-4">
                                <Button onClick={post} color="primary">Effectuer le calcul de dimensionnement</Button>
                            </div>
                            <div className="col-4">
                            </div>

                            <div className="col-4">
                                {(loadingdata) ? (<Spinner/>) : (<div></div>)}
                            </div>


                        </div>

                    </div>
                    <div className="col-4">
                        <div>
                            {(dataerror) ? (<div key="alert">{contentalert}</div>) : (
                                <div></div>)}
                            {(dataok) ? (<div key="ok">{contentok}</div>) : (
                                <div></div>)}
                        </div>
                        <div>
                        {(loadingdata) ? (<Spinner/>) : (
                            <>
                                <b>Résultats optimisés:</b>
                                <div>
                                    {data[1].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>
                                <b>Statistiques précipitations</b>
                                <div>
                                    {data[0].map(
                                        (item) => (
                                            <div className="content" dangerouslySetInnerHTML={{__html: item}}></div>)
                                    )
                                    }
                                </div>
                                <br/>

                            </>
                        )
                        }


                        </div>

                        <br/>
                    </div>
                </div>
            </div>

        </>
    );
};
export default Water;
