import React, {useEffect, useState} from "react";
import {useRef} from "react";
import markerImg from "../../static/frontend/marker.png";
import getData from "../functions/getData";

const MapWrapper = ({
                        fullInputAdress,
                        latitude,
                        longitude,
                    }) => {
    const ref = useRef(null);
    const [data, setData] = useState({});
    const [bounds, setBounds] = useState({});

    let geocoder = new google.maps.Geocoder();
    let map;

    let mapOptions = {
        zoom: 11,
        center: new google.maps.LatLng(latitude, longitude),
        styles: [
            {
                featureType: "all",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "administrative",
                elementType: "labels.text.fill",
                stylers: [
                    {
                        color: "#444444",
                    },
                ],
            },
            {
                featureType: "administrative.province",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.locality",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "administrative.neighborhood",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.land_parcel",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "administrative.land_parcel",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "landscape",
                elementType: "all",
                stylers: [
                    {
                        color: "#f2f2f2",
                    },
                ],
            },
            {
                featureType: "landscape.man_made",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "poi",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                    {
                        color: "#cee9de",
                    },
                    {
                        saturation: "2",
                    },
                    {
                        weight: "0.80",
                    },
                ],
            },
            {
                featureType: "poi.attraction",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "poi.park",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road",
                elementType: "all",
                stylers: [
                    {
                        saturation: 0,
                    },
                    {
                        lightness: 45,
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        color: "#fffff",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.highway",
                elementType: "labels.icon",
                stylers: [
                    {
                        hue: "#ff0000",
                    },
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.highway.controlled_access",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "road.highway.controlled_access",
                elementType: "labels.icon",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#0064ff",
                    },
                    {
                        gamma: "1.44",
                    },
                    {
                        lightness: "-3",
                    },
                    {
                        weight: "1.69",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "road.local",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "road.local",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        weight: "0.31",
                    },
                    {
                        gamma: "1.43",
                    },
                    {
                        lightness: "-5",
                    },
                    {
                        saturation: "-22",
                    },
                ],
            },
            {
                featureType: "transit",
                elementType: "all",
                stylers: [
                    {
                        visibility: "off",
                    },
                ],
            },
            {
                featureType: "transit.line",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#ff0000",
                    },
                ],
            },
            {
                featureType: "transit.station.airport",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        hue: "#ff0045",
                    },
                ],
            },
            {
                featureType: "transit.station.bus",
                elementType: "all",
                stylers: [
                    {
                        visibility: "on",
                    },
                    {
                        hue: "#00d1ff",
                    },
                ],
            },
            {
                featureType: "transit.station.bus",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "transit.station.rail",
                elementType: "all",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                    {
                        hue: "#00cbff",
                    },
                ],
            },
            {
                featureType: "transit.station.rail",
                elementType: "labels.text",
                stylers: [
                    {
                        visibility: "simplified",
                    },
                ],
            },
            {
                featureType: "water",
                elementType: "all",
                stylers: [
                    {
                        color: "#46bcec",
                    },
                    {
                        visibility: "on",
                    },
                ],
            },
            {
                featureType: "water",
                elementType: "geometry.fill",
                stylers: [
                    {
                        weight: "1.61",
                    },
                    {
                        color: "#AECDCD",
                    },
                    {
                        visibility: "on",
                    },
                ],
            },
        ],
    };
    useEffect(() => {
        // Check if the Google Maps API is loaded
        if (window.google) {
            map = new window.google.maps.Map(ref.current, mapOptions);
            let marker = new google.maps.Marker({
                position: {lat: latitude, lng: longitude},
                title: "test",
                label: "test",
                map,
                icon: markerImg,
                clickable: true
            })
            marker.setMap(map);
        }
    }, []);


    return (
        <div
            ref={ref}
            id="map"
            key={1}
            className="h-100 rounded-top-2xl lg-rounded-top-0  rounded-start-2xl lg-rounded-bottom-2xl"
        />
    );
};

export default MapWrapper;

// function handleEvent(event) {
//       let lat=event.latLng.lat();
//       let lng=event.latLng.lng();
//       ////console.log("lat");
//              ////console.log(lat);
//       setProject({
//           ...project,
//           latitude: lat,
//           longitude: lng,
//         });
//         };

//   useEffect(() => {
//         const bounds = map.getBounds();
// getData(bounds).then((res)=>{
//   setData(res.data);
//       }, [bounds]);
//
//   function handleBoundsChanged() {
//  setBounds (map.getBounds());
// }

//map.addListener("bounds_changed", handleBoundsChanged);

// const initialize = () => {
//   geocoder = new google.maps.Geocoder();
//   let latlng = new google.maps.LatLng(latitude, longitude);
//
//   let marker = new google.maps.Marker({
//     position: { lat: latitude, lng: longitude },
//     map,
//     icon: markerImg,
//     draggable:true,
//   });
//
//   map = new google.maps.Map(ref.current, mapOptions);
//   map.setCenter(latlng);
//   marker.setMap(map);
//   marker.addListener("dragend", function() {
//       // fonction à exécuter lorsque le marqueur est déplacé
//       var lat=marker.getPosition().lat();
//       var lng=marker.getPosition().lng();
//       setProject({
//           ...project,
//           latitude: lat,
//           longitude: lng,
//         });
//       });
// };
//
//
//
// useEffect(() => {
//   // initialize map when data is not from user input ("onglet - informations et réglages")
//   if (latitude && longitude) initialize();
// }, [latitude, longitude]);
//
// useEffect(() => {
//   // Call function for generating map when full adress change in user input
//   codeAddress();
// }, [fullInputAdress]);
//