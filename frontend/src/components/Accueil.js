import React from "react";
import {Link} from "react-router-dom";


const Accueil = () => {


    const content = <div>
     <Link to="/works">Travaux et publications:</Link><br/>
     <a href="https://vpn.matangi.dev/lum">Mesures locale de la luminosité du serveur</a><br/>
     <Link to="/autonomie">Trucs sur le "lifestyle" autonome (out of stock for most)</Link><br/>
     <Link to="/communes">Travail débuté en février 2024 sur des trucs utiles pour les communes</Link><br/> 
     <a href="https://vpn.matangi.dev/app">Partage de quelques fichiers</a><br/> 

    </div>
    return (
        <>{content}</>
    )
};
export default Accueil;
