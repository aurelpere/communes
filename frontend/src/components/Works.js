import React from "react";
import {Link} from "react-router-dom";

const Works = () => {



    const content = <div>

Vous trouverez ci-dessous des documents publiés dont je suis l'auteur.<br/><br/>
2010 : <a href="/media/fichiers/2011/apere_cete_phtv_aqui_1.pdf" download>potentiel photovoltaique aquitaine</a><br/>
2010 : <a href="/media/fichiers/2011/apere_cete_phtv_limou_1.pdf" download>potentiel photovoltaique limousin</a><br/>
2011 : <a href="/media/fichiers/2011/FILOCOM_v3.pdf" download>Appréhension des typologies de pauvreté dans le Grand Sud-Ouest</a><br/>
2011 : <a href="/media/fichiers/2011/RapportSyntheseFilocomNovembre.pdf" download>Synthèse Appréhension des typologies de pauvreté dans le Grand Sud-Ouest</a><br/>
2011 : <a href="/media/fichiers/2011/Formation11.03.2011.pdf" download>Formation Analyse des revenus et de la pauvreté avec Filocom CVRH Toulouse</a><br/>
2011 : <a href="/media/fichiers/2011/Formation11.03_english.2011.pdf" download><b>(English)</b> Income and poverty analysis with filocom CVRH Toulouse</a><br/>
2011 : <a href="/media/fichiers/2011/apere_cete_phtv_aqui_2.pdf" download>prospective aquitaine photovoltaique 2020</a> (notez qu'on se trouve entre le scenario "median" et "de transition" fin 2019 avec environ 1800 MW installés cf stats <a href="https://www.statistiques.developpement-durable.gouv.fr/tableau-de-bord-solaire-photovoltaique-quatrieme-trimestre-2019" download>ici</a>)<br/>
2011 : <a href="/media/fichiers/2011/apere_cete_phtv_limou_2.pdf" download>prospective limousin photovoltaique 2020"</a> (notez qu'on est à environ 85% du scenario de transition fin 2019 avec environ 250MW installés cf stats <a href="https://www.statistiques.developpement-durable.gouv.fr/tableau-de-bord-solaire-photovoltaique-quatrieme-trimestre-2019" download>ici</a>)<br/>
2011 : <a href="/media/fichiers/2011/EC_Synthese_CUB.pdf" download>synthese des politique energie climat de 6 agglomerations</a> (exemple sur la CUB)<br/>
2012 : <a href="/media/fichiers/2012/cete_apere_loyers_aquitaine.pdf" download>etude des loyers prives en aquitaine</a><br/>
2012 : <a href="/media/fichiers/2012/apere_synthese_loyers_pc.pdf" download>etude des loyers prives en poitou charentes</a><br/>
2012 : <a href="/media/fichiers/2012/apere_synthese_loyers_17.pdf" download>synthese des loyers prives en charente maritime</a> (avec une data visualisation interessante)<br/>
2012 : <a href="/media/fichiers/2012/apere_synthese_loyers_17_english.pdf" download><b>(English)</b> rent prices synthesis in charente maritime</a> (with interesting data visualisation)<br/>
2013 : <a href="/media/fichiers/2013/CETE_cout_logement.pdf" download>cout du logement en gironde</a> (methode avec donnees locales sans pre-modele)<br/>
2013 : <a href="/media/fichiers/2013/diag_epci79_NordOuest_vfinale.pdf" download>diagnostic de tous les epci des deux-sevres</a> (exemple sur l'epci nord ouest)<br/>
2014 : publication quae psdr (envoyée fin 2014, publiée en 2015/2016) <a href="https://www.cairn.info/partenariats-pour-le-developpement-territorial--9782759224081-page-125.htm" download>Didier Labat, Aurélien Péré. "Méthode d’analyse des outils de politique forestière et de planification foncière.". Dans Partenariats pour le développement territorial, pages 125 à 148,2015</a><br/>
2015 : <a href="/media/fichiers/2015/apere_dtectv_fet.pdf" download>facture energetique territoriale</a><br/><br/>

A partir de 2015 la plupart de mes propositions n'ont pas été publiées, ou pas en mon nom. <br/><br/>

2016 : <a href="/media/fichiers/2016/script_habitat&activite_vacant.txt" download>script sql pour identifier les logements et batiments vacants dans le fichier majic</a> (je vous laisse deviner combien de lignes prend la commande sql permettant d'identifier tous les logements vides depuis plusieurs années au 1er janvier dans toute la France). Vous pouvez aussi calculer ceci en nombre de frappes clavier et clics de souris.
<br/>

2016 : <a href="/media/fichiers/2016/apere_dterso_zoneactivite_07.03.16.pdf" download>creation epfe aquitaine - contribution au diagnostic foncier des zones d'activites</a><br/>

2017 : <a href="/media/fichiers/2017/memoire_enr_APERE_31.08.17.pdf" download>memoire m2 sur les enr  : Quels critères pour une transition écologique « conviviale » ?</a><br/>

2019 : <a href="/media/fichiers/2019/APERE_ADEME_CCTP éolien_09.06.19.pdf" download>cahier des charges pour une etude sur l'impact de l'eolien sur les prix fonciers et immobiliers</a>(non retenu par le cedip)<br/>

2019 : <a href="/media/fichiers/2019/apere_inforsid_confiance_ntic.pdf" download>Proposition Inforsid transmise à M. Colletis : "Quelles sont les « nouvelles couches de contraintes » plus ou moins institutionnalisées que les
NTIC apportent et comment cela influence ou interfère avec les différents échelons de démocratie ?"</a> (non retenue par le cedip) <br/>

2019 : <a href="/media/fichiers/2019/apere_note_FOAD_velo_cedip.pdf" download>proposition de formation à distance velo pour le ministere de l'ecologie</a>(non retenue par le cedip)<br/>

2019 : <a href="/media/fichiers/2019/apere_veille_cedip.pdf" download>veille logicielle pour le cedip</a>(retenue par le cedip)<br/>

2019 : <a href="/media/fichiers/2019/apere_plu_mtp_noteverdanson.pdf" download>contribution citoyenne aux enjeux d'innondation du plu à montepellier - verdanson</a><br/><br/>

2020 : <a href="/media/fichiers/Harrassment_journal.pdf" download>contribution à un outil anti harcellement version papier (creative commons)</a>
<br/><br/>
</div>
    return (
        <>{content}</>
    )
};
export default Works;
