import axios from "axios";
import {getCookie} from "./getCookie";

export const postmap = async (payload, sup, inf,ipAddress,libnatmut,libtypbien) => {
    try {
        //const ipAddressResponse = await axios.get('https://api.ipify.org');
        //const ipAddress = ipAddressResponse.data;
        //const ipAddress = "999.999.999.999"
        const mapResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/tiba/`,
            {
                MOz7cZ5O5t9: payload._southWest.lat,
                SintHpC9GQ6: payload._southWest.lat - 0.00000000000001,
                BeKs25u6Gxj: payload._northEast.lng + 0.00000000000001,
                AAGxjnDfWLc: payload._southWest.lng - 0.00000000000001,
                ab66FO9Cofi: payload._northEast.lng,
                HOQ46BsSWQR: payload._northEast.lng - 0.00000000000001,
                PK7u16bdr6k: payload._northEast.lat,
                PiRs30p5S4e: payload._northEast.lat + 0.00000000000001,
                OsDGHH4QgKd: payload._southWest.lng + 0.00000000000001,
                haBJUOksrEk: payload._northEast.lat - 0.00000000000001,
                UxCMlDNLQyl: payload._southWest.lng + 0.00000000000001,
                CM3T01OtEMk: payload._southWest.lng,
                sup: sup,
                inf: inf,
                libnatmut: libnatmut,
                libtypbien: libtypbien
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return mapResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default postmap;

