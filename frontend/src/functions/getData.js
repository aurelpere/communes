import axios from "axios";
import {getCookie} from "./getCookie";

export const login = async (bounds) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const loginResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/data/`,
            {bounds: bounds},
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return loginResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default login;

