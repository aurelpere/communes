import axios from 'axios';
import {getCookie} from "./getCookie";
export const getTokenDetail = async (authToken) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const tokenDetailResponse = await axios.get(`${process.env.REACT_APP_BASE_URL}/token_detail/`, {
            headers: {
                'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                // Authorization: `Token ${authToken}`,
                'X-Forwarded-For': ipAddress,
            },
        });

        return tokenDetailResponse.data; // Return the actual data you want
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
};
