import axios from "axios";
import {getCookie} from "./getCookie";

export const signup = async (payload) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const subscribeResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/6lw7F9LY0BqOCcfx/`,
            {
                user: payload.user,
                mail: payload.mail,
                pw: payload.pw,
                cp: payload.cp,
                dn: payload.dn,
                tn: payload.tn,
                pro: payload.pro
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return subscribeResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default signup;

