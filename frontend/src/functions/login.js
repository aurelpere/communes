import axios from "axios";
import {getCookie} from "./getCookie";

export const login = async (payload) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const loginResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/IMoj7RuE8gKO8lZs/`,
            {mail: payload.mail, pw: payload.pw,},
            {
                headers: {
                    'contentType': 'application/json',
                    'stay-connected': payload.stay_connected.toString(),
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return loginResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default login;

