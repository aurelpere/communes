import axios from "axios";
import {getCookie} from "./getCookie";

export const postmap = async (payload, sup, inf,ipAddress) => {
    try {
        //const ipAddressResponse = await axios.get('https://api.ipify.org');
        //const ipAddress = ipAddressResponse.data;
        //const ipAddress = "999.999.999.999"
        const mapResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/map/`,
            {
                VHYJr6AeTGX: payload._southWest.lat,
                CBDhkxcNVXf: payload._southWest.lat + 0.00000000000001,
                zuKMq0gTu7w: payload._southWest.lng - 0.00000000000001,
                NBCqBLJzqTB: payload._northEast.lat + 0.00000000000001,
                p3Z69bFWNWU: payload._southWest.lng,
                hXdUoCq9dvq: payload._northEast.lng - 0.00000000000001,
                BuMriWljfiD: payload._northEast.lat,
                Irm1Qr76oSd: payload._northEast.lng + 0.00000000000001,
                PFrCyMQwbJa: payload._northEast.lng,
                K8U5BxhgjJO: payload._northEast.lat - 0.00000000000001,
                TvhjZMpNdRc: payload._southWest.lng + 0.00000000000001,
                sup: sup,
                inf: inf
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return mapResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default postmap;

