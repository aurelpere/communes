import axios from "axios";
import {getCookie} from "./getCookie";

export const sunpost = async (d,angle,aspect,ipAddress,lat, lon, consojour,typebatterie,njnoelec,anneedebut,anneefin) => {
    try {
        //const ipAddressResponse = await axios.get('https://api.ipify.org');
        //const ipAddress = ipAddressResponse.data;
        //const ipAddress = "999.999.999.999"
        const waterResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/sunapi/`,
            {
                d: d,
		angle: angle,
		aspect: aspect,
		lat: lat,
                lon: lon, 
                consojour: consojour,
                typebatterie: typebatterie,
                njnoelec: njnoelec,
                anneedebut:anneedebut,
                anneefin:anneefin
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return waterResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default sunpost;

