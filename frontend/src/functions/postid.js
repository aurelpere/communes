import axios from "axios";
import {getCookie} from "./getCookie";

export const postmap = async (id,ipAddress) => {
    try {
        //const ipAddressResponse = await axios.get('https://api.ipify.org');
        //const ipAddress = ipAddressResponse.data;
        //const ipAddress = "999.999.999.999"
        const mapResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/id/`,
            {
                pk: id,
            },
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return mapResponse.data;
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default postmap;

