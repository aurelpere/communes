import axios from "axios";
import {getCookie} from "./getCookie";

export const resetpw = async (payload) => {
    try {
        const ipAddressResponse = await axios.get('https://api.ipify.org');
        const ipAddress = ipAddressResponse.data;

        const resetpwResponse = await axios.post(`${process.env.REACT_APP_BASE_URL}/5w4nro4oOXVtgGBw/`,
            {mail: payload.mail, redirectTo: payload.redirectTo},
            {
                headers: {
                    'contentType': 'application/json',
                    'S3WE6MqjhGzVJooN': getCookie("csrftoken"),
                    'X-Forwarded-For': ipAddress,
                }
            },
        );

        return resetpwResponse.data; // Return the actual data you want
    } catch (error) {
        console.error('An error occurred:', error);
        throw error; // Rethrow the error to be handled by the caller
    }
}

export default resetpw;

