import React from "react";
import ReactDOM from "react-dom";
import {render} from "react-dom";
import App from "./components/App";
import '../static/frontend/css/style.css';

const container = document.getElementById("app");
render(
    <React.StrictMode>
            <App/>
    </React.StrictMode>,
    container
);
