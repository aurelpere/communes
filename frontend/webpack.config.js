const path = require("path");
const Dotenv = require("dotenv-webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");

module.exports = {
  mode: "development",
  entry: {
    app: "./src/index.js",
  },
  output: {
    path: path.resolve(__dirname, "static/frontend/js"),
    publicPath: "/static/frontend/js/",
    filename: "main.js",
    chunkFilename: "[name].chunk.js",
  },
  node: {
    fs: "empty",
  },
  plugins: [new CleanWebpackPlugin(), new Dotenv({ path: "./.env" })],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.svg$/,
        use: ["@svgr/webpack"],
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff(2)?|ttf|eot)$/i,
        use: [
          {
            loader: "file-loader",
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".css"], //An empty string is no longer required.
    modules: ["node_modules"],
  },
};
